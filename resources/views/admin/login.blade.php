@extends('admin.master')
@section('title', 'Login')



@section('content')


<!-- Page -->
  <div class="page vertical-align text-center">
    <div class="page-content vertical-align-middle">
      <div class="panel">
        <div class="panel-body">
          <div class="brand">
            <img class="brand-img" src="../assets/images/logo-blue.png" alt="...">
            <h2 class="brand-text font-size-18">Remark</h2>
          </div>
          {{ Form::open(['action' => 'Admin\login@loginSubmit']) }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label class="pull-left control-label">Email</label>
              <input type="email" class="form-control" name="email" />
              <small class="text-danger text-bold">{{$errors->first('email')}}</small>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label class="pull-left control-label">Password</label>
              <input type="password" class="form-control" name="password" />
              <small class="text-danger text-bold">{{$errors->first('password')}}</small>
            </div>
            <div class="form-group clearfix">
              <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg pull-left">
                <input type="checkbox" id="inputCheckbox" name="remember">
                <label for="inputCheckbox">Remember me</label>
              </div>
              <a class="pull-right" href="forgot-password.html">Forgot password?</a>
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-lg margin-top-40">Sign in</button>
          {{ Form::close() }}
          <p>Still no account? Please go to <a href="register-v3.html">Sign up</a></p>
        </div>
      </div>

      <footer class="page-copyright page-copyright-inverse">
        <p>WEBSITE BY amazingSurge</p>
        <p>© 2016. All RIGHT RESERVED.</p>
        <div class="social">
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
          <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-google-plus" aria-hidden="true"></i>
          </a>
        </div>
      </footer>
    </div>
  </div>
  <!-- End Page -->

  @endsection

  @push('styles')
      <link rel="stylesheet" href="{{asset('assets/assets/examples/css/pages/login-v3.css')}}" />
  @endpush