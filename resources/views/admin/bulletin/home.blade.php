@extends('admin.master')
@section('title', $page_title)



@section('content')

<div class="row">
<div class="col-md-8">

<div class="panel">
<div class="panel-body">
<div class="row">
<div class="col-xs-9 select-box">
<select name="post" id="post" class="form-control input-lg" onchange="get_post(this.value)">
<option value="">- Select Post -</option>
<?php echo $select_post ?>
</select>


</div>
<div class="col-xs-3">
<button class="btn btn-success waves-effect waves-classic btn-block" type="submit"
                       id="btnSave">Save</button>
</div>
</div>
</div>
</div>


<div class="panel">
<div class="panel-body post-data"></div>
</div>

</div>
</div>

@endsection




@push('scripts')
<script>
var current_post_id = '{{$current_post_id}}';
</script>
<script src="{{asset('assets/assets/js/App/Bulletin.js?time='.time())}}"></script>

@endpush