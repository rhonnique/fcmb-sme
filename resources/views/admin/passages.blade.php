@extends('admin.master')
@section('title', $page_title)



@section('content')


<div class="panel panel-add-new {{$errors->any()&&Input::get( 'mode' ) != "edit"?'panel-add-new-open':''}}">
<div class="panel-body">



<div class="row">
<div class="col-md-8">



<div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab"
                    href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab"
                    aria-selected="true">Single Upload</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab"
                    href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-selected="false">Bulk Upload
                    </a></li>

                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active padding-top-20" id="exampleTabsOne" role="tabpanel">



<!-- ***************** ADD SINGLE FORM ********************* -->


{{ Form::open(['action' => 'Admin\Passages@store', 'id' => 'form_add', 'autocomplete' => 'off']) }}
                      <div class="form-group clearfix {{ $errors->has('date') ? ' has-danger' : '' }}">
                      <label class="form-control-label">Date</label>
                      <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                          </span>
                                          <input type="text" placeholder="Date" class="form-control" name="date"
                                          data-plugin="datepicker" value="{{ old('date')?old('date'):date('m/d/Y')}}">

                      </div>

<small class="text-danger text-bold">{{$errors->first('date')}}</small>
                      </div>

                       <div class="form-group">
                       <label>Verse</label>
                       <input type="text" class="form-control" name="verse" placeholder="Verse" value="{{old('verse')}}">
                       </div>

                       <div class="form-group">
                       <label>Passage</label>
                       <textarea name="passage" cols="30" rows="8" class="form-control">{{old('passage')}}</textarea>
                       </div>


                        <div class="form-group">
                         <label>Note</label>
                         <textarea name="note" cols="30" rows="5" class="form-control">{{old('note')}}</textarea>
                       </div>

                       <div class="form-group">
                       <button type="button" class="btn btn-default waves-effect waves-classic
                       panel-hide-new">Close</button>
                       <button class="btn btn-success waves-effect waves-classic" type="submit"
                       id="btnSubmitAdd">Submit</button>
                       </div>

                       {{Form::close()}}







                    </div>
                    <div class="tab-pane padding-top-20" id="exampleTabsTwo" role="tabpanel">


{{ Form::open(['action' => 'Admin\Passages@upload', 'id' => 'form_upload', 'autocomplete' => 'off', 'enctype'=>'multipart/form-data']) }}
                      <div class="form-group">
                                             <label>Select file from computer</label>
                                             <input type="file" class="form-control" name="passage_upload">
                                             </div>

                      <div class="form-group">
                                             <button type="button" class="btn btn-default waves-effect waves-classic
                                             panel-hide-new">Close</button>
                                             <button class="btn btn-success waves-effect waves-classic" type="submit"
                                             id="btnSubmitUpload">Submit</button>
                                             </div>

                                             {{Form::close()}}

                    </div>
                  </div>
                </div>


</div>
</div>





<!-- ***************** ADD SINGLE FORM END ********************* -->






</div>
</div>


    <div class="panel panel-table-bordered">









@if($table->count() > 0)
<div class="table-responsive">

@if(Input::get( 'mode' ) != "edit")
{{ Form::open(['action' => 'Admin\Passages@delete', 'id' => 'form_table']) }}
@endif

    						<table class="table table-bordered font-size-14">
    							<thead>
    								<tr>
    									<th width="1%">


    									<span class="checkbox-custom checkbox-primary">
                                                                    <input class="selectable-all" type="checkbox">
                                                                    <label></label>
                                                                  </span>


    									</th>
    									<th width="20%">Date</th>
    									<th width="20%" >Verse</th>
    									<th width="50%">Passage</th>
    									<th width="1%" class="text-center">
    									<i class="icon wb-edit" aria-hidden="true"></i>
    									</th>
    								</tr>
    							</thead>
    							<tbody>
    							@foreach ($table as $row)
    							@if(Input::get( 'mode' ) == "edit" && Input::get( 'id' ) == $row->id)
    							<tr class="no-bg">
    							<td colspan="5" class="padding-50">

    							<div class="row">
                                <div class="col-md-6">

                                <h4>Edit Row</h4>
{{ Form::open(['method' => 'PATCH','action' => ['Admin\Passages@update', $row->id ]]) }}


<div class="form-group clearfix {{ $errors->has('date') ? ' has-danger' : '' }}">
                      <label class="form-control-label">Date</label>
                      <div class="input-group">
                                          <span class="input-group-addon">
                                            <i class="icon md-calendar" aria-hidden="true"></i>
                                          </span>
                                          <input type="text" placeholder="Date" class="form-control" name="date"
                                          data-plugin="datepicker" value="{{ old("date")?old("date"):date('m/d/Y',strtotime($row->date)) }}">

                      </div>

<small class="text-danger text-bold">{{$errors->first('date')}}</small>
                      </div>

                       <div class="form-group">
                       <label>Verse</label>
                       <input type="text" class="form-control" name="verse" placeholder="Verse"
                        value="{{ old("verse")?old("verse"):$row->verse }}">
                       </div>

                       <div class="form-group">
                       <label>Passage</label>
                       <textarea name="passage" cols="30" rows="8" class="form-control"
                       >{{ old("passage")?old("passage"):$row->passage }}</textarea>
                       </div>


                        <div class="form-group">
                         <label>Note</label>
                         <textarea name="note" cols="30" rows="5" class="form-control"
                         >{{ old("note")?old("note"):$row->note }}</textarea>
                       </div>



<!-- -->



                                	<button type="button" class="btn btn-default form-close"
                                	onclick="window.location='{{resetEdit("bible-passages")}}'">Close</button>
                                    <button type="submit" class="btn btn-primary" name="btnEdit">Submit</button>
@if(Input::get( 'mode' ) != "edit")
{{Form::close() }}
@endif



                                	</div></div>
    							</td>
    							</tr>
    							@else
    								<tr>
    									<td>
    									<span class="checkbox-custom checkbox-primary">
                                           <input class="selectable-item" type="checkbox" name="table_row_selected[]"
                                           value="{{$row->id}}">
                                               <label></label>
                                         </span>
    									</td>
    									<td>{{date('F d, Y',strtotime($row->date))}}</td>
    									<td >
    									{{$row->verse}}
    									</td>
    									<td>
    									{{$row->passage}}
    									</td>

    									<td class="text-center">

    									<button type="button" class="btn btn-pure btn-dark icon wb-edit no-effect"
    									onclick="window.location='{{url(getEditLink("bible-passages", $row->id))}}'"></button>

<!--

    									<div class="btn-group dropdown">
                                                            <button type="button" class="btn btn-info dropdown-toggle" id="exampleBulletDropdown2"
                                                              data-toggle="dropdown" aria-expanded="false">
                                                              <i class="icon md-more" aria-hidden="true"></i>
                                                            </button>
                                                            <div class="dropdown-menu bullet dropdown-menu-right" aria-labelledby="exampleBulletDropdown2"
                                                              role="menu">
                                                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Action</a>
                                                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Another action</a>
                                                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Something else here</a>
                                                              <div class="dropdown-divider"></div>
                                                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Separated link</a>
                                                            </div>
                                                          </div>
                                                          -->
    									</td>
    								</tr>
    								@endif
    								@endforeach
    							</tbody>
    						</table>

    						{{ Form::close() }}

    						</div>
    						@else
    						<div class="panel-body" style="padding-top: 15px">No result found!</div>
    						@endif




          </div>


          {{ $table->appends($select)->links() }}

                    </div>





















@endsection

  @push('styles')
        <link rel="stylesheet" href="{{asset('assets/global/vendor/formvalidation/formValidation.min.css?v4.0.1')}}">
        <link rel="stylesheet" href="{{asset('assets/assets/examples/css/forms/validation.min.css?v4.0.1')}}">

        <link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css?v4.0.1')}}">
        <link rel="stylesheet" href="{{asset('assets/assets/examples/css/forms/advanced.min.css?v4.0.1')}}">
  @endpush

  @push('scripts')

      <script src="{{asset('assets/global/vendor/formvalidation/formValidation.min.js?v4.0.1')}}"></script>
      <script src="{{asset('assets/global/vendor/formvalidation/framework/bootstrap4.min.js?v4.0.1')}}"></script>

<script src="{{asset('assets/global/js/Plugin/bootstrap-datepicker.min.js?v4.0.1')}}"></script>
      <script src="{{asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js?v4.0.1')}}"></script>


      <script src="{{asset('assets/global/js/Plugin/asselectable.min.js?v4.0.1')}}"></script>
      <script src="{{asset('assets/global/js/Plugin/selectable.min.js?v4.0.1')}}"></script>

      <script src="{{asset('assets/assets/js/App/Passages.js?time='.time())}}"></script>

      <script>
          function search_page(){
              var s_details = $("#s_details").val();
              var s_date_from = $("#s_date_from").val();
              var s_date_to = $("#s_date_to").val();

              if(!s_details && !s_date_from && !s_date_to){
                  alert("At least one field is required!");
                  $(".btn-search-wrapper input:first").focus();
                  return false;
              }

              s_details = s_details?'details='+encodeURIComponent(s_details):'';
              s_date_from = s_date_from?'&date_from='+encodeURIComponent(s_date_from):'';
              s_date_to = s_date_to?'&date_to='+encodeURIComponent(s_date_to):'';

              var string = s_details + s_date_from + s_date_to;
              //string = string.substring(0, string.length-1);
              window.location = '{{url("bible-passages/")}}?'+string;

          }
      </script>

  @endpush