

                <div class="nav-tabs-horizontal">
                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne"
                      role="tab">Home</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo"
                      role="tab">Components</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsThree" aria-controls="exampleTabsThree"
                      role="tab">Css</a></li>
                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsFour" aria-controls="exampleTabsFour"
                      role="tab">Javascript</a></li>
                  </ul>
                  <div class="tab-content padding-top-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neglegentur sabinum instructus
                      fingitur accusantibus harum neque consuetudine intereant
                      numeris, ipse tuemur suum apud mediocrem iactant. Libidinibus
                      amatoriis dicta albuci manum, statue.
                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                      Negant parvos fructu nostram mutans supplicii ac dissentias, maius tibi licebit
                      ruinae philosophia. Salutatus repellere titillaret expetendum
                      ipsi. Cupiditates intellegam exercitumque privatio concederetur,
                      sempiternum, verbis malint dissensio nullas noctesque earumque.
                    </div>
                    <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                      Benivole horrent tantalo fuisset adamare fugiendam tractatos indicaverunt animis
                      chaere, brevi minuendas, ubi angoribus iisque deorsum audita
                      haec dedocendi utilitas. Panaetium erimus platonem varias
                      imperitos animum, iudiciorumque operis multa disputando.
                    </div>
                    <div class="tab-pane" id="exampleTabsFour" role="tabpanel">
                      Metus subtilius texit consilio fugiendam, opinionum levius amici inertissimae pecuniae
                      tribus ordiamur, alienus artes solitudo, minime praesidia
                      proficiscuntur reiciat detracta involuta veterum. Rutilius
                      quis honestatis hominum, quisquis percussit sibi explicari.
                    </div>
                  </div>
                </div>





                @push('scripts')

                      <script src="{{asset('assets/global/js/plugins/responsive-tabs.min.js')}}"></script>
                      <script src="{{asset('assets/global/js/components/tabs.min.js')}}"></script>
                  @endpush