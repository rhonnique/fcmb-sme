<div class="page-aside">
      <!-- Contacts Sidebar -->
      <div class="page-aside-switch">
        <i class="icon wb-chevron-left" aria-hidden="true"></i>
        <i class="icon wb-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner" data-plugin="pageAsideScroll">



        <div data-role="container">
          <div data-role="content">


@if($group->count() > 0)

@else

<div class="page-aside-section">
                        <div class="list-group">
                          <a class="list-group-item text-color danger" href="javascript:void(0)">

                            No contact group added yet! </a>
                        </div>
                      </div>

@endif


<div class="page-aside-section">
                        <div class="list-group">
                          <a id="addLabelToggle" class="list-group-item" href="javascript:void(0)"
                          onclick='defaultModal({ "modalID": "addGroup" })'>
                                            <i class="icon wb-plus" aria-hidden="true"></i> Add New Group
                                          </a>
                        </div>
                      </div>




<!--

            <div class="page-aside-section">
              <div class="list-group">
                <a class="list-group-item" href="javascript:void(0)">
                  <span class="item-right">61</span><i class="icon wb-inbox" aria-hidden="true"></i>
                  All contacts</a>
              </div>
            </div>


            -->

            <div class="page-aside-section">
              <h5 class="page-aside-title">LABEL</h5>
              <div class="list-group has-actions">
                <div class="list-group-item">
                  <div class="list-content">
                    <span class="item-right">10</span>
                    <span class="list-text">Work</span>
                    <div class="item-actions">
                      <span class="btn btn-pure btn-icon" data-toggle="list-editable"><i class="icon wb-edit" aria-hidden="true"></i></span>
                      <span class="btn btn-pure btn-icon" data-tag="list-delete"><i class="icon wb-trash" aria-hidden="true"></i></span>
                    </div>
                  </div>
                  <div class="list-editable">
                    <div class="form-group form-material">
                      <input type="text" class="form-control empty" name="label" data-bind=".list-text"
                      value="Work">
                      <button type="button" class="input-editable-close icon wb-close" data-toggle="list-editable-close"
                      aria-label="Close" aria-expanded="true"></button>
                    </div>
                  </div>
                </div>
                <div class="list-group-item">
                  <div class="list-content">
                    <span class="item-right">5</span>
                    <span class="list-text">Family</span>
                    <div class="item-actions">
                      <span class="btn btn-pure btn-icon" data-toggle="list-editable"><i class="icon wb-edit" aria-hidden="true"></i></span>
                      <span class="btn btn-pure btn-icon" data-tag="list-delete"><i class="icon wb-trash" aria-hidden="true"></i></span>
                    </div>
                  </div>
                  <div class="list-editable">
                    <div class="form-group form-material">
                      <input type="text" class="form-control empty" name="label" data-bind=".list-text"
                      value="Family">
                      <button type="button" class="input-editable-close icon wb-close" data-toggle="list-editable-close"
                      aria-label="Close" aria-expanded="true"></button>
                    </div>
                  </div>
                </div>
                <div class="list-group-item">
                  <div class="list-content">
                    <span class="item-right">16</span>
                    <span class="list-text">Private</span>
                    <div class="item-actions">
                      <span class="btn btn-pure btn-icon" data-toggle="list-editable"><i class="icon wb-edit" aria-hidden="true"></i></span>
                      <span class="btn btn-pure btn-icon" data-tag="list-delete"><i class="icon wb-trash" aria-hidden="true"></i></span>
                    </div>
                  </div>
                  <div class="list-editable">
                    <div class="form-group form-material">
                      <input type="text" class="form-control empty" name="label" data-bind=".list-text"
                      value="Private">
                      <button type="button" class="input-editable-close icon wb-close" data-toggle="list-editable-close"
                      aria-label="Close" aria-expanded="true"></button>
                    </div>
                  </div>
                </div>
                <div class="list-group-item">
                  <div class="list-content">
                    <span class="item-right">30</span>
                    <span class="list-text">Friends</span>
                    <div class="item-actions">
                      <span class="btn btn-pure btn-icon" data-toggle="list-editable"><i class="icon wb-edit" aria-hidden="true"></i></span>
                      <span class="btn btn-pure btn-icon" data-tag="list-delete"><i class="icon wb-trash" aria-hidden="true"></i></span>
                    </div>
                  </div>
                  <div class="list-editable">
                    <div class="form-group form-material">
                      <input type="text" class="form-control empty" name="label" data-bind=".list-text"
                      value="Friends">
                      <button type="button" class="input-editable-close icon wb-close" data-toggle="list-editable-close"
                      aria-label="Close" aria-expanded="true"></button>
                    </div>
                  </div>
                </div>
                <a id="addLabelToggle" class="list-group-item"
                onclick="history.pushState('data', '','contacts/33543344')">
                  <i class="icon wb-plus" aria-hidden="true"></i> Add New Label
                </a>
              </div>
            </div>




          </div>




        </div>





      </div>
    </div>
















    <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
            <i class="icon wb-chevron-left" aria-hidden="true"></i>
            <i class="icon wb-chevron-right" aria-hidden="true"></i>
        </div>



        <div class="page-aside-inner" data-plugin="pageAsideScroll">
            <div data-role="container">
                <div data-role="content">
                    <div class="page-aside-section">
                        <div class="list-group">
                            <a class="list-group-item" [routerLink]="['/contacts']">
                                <span class="item-right">61</span>
                                <i class="icon wb-inbox" aria-hidden="true"></i>All contacts
                            </a>
                        </div>
                    </div>
                    <div class="page-aside-section">
                        <h5 class="page-aside-title">LABEL</h5>
                        <div class="list-group has-actions">

                            <div class="list-group-item" *ngFor="let group of contact_group">
                                <div class="list-content">
                                    <span class="item-right">{{group.total_contacts}}</span>
                                    <span class="list-text">{{group.name}}</span>
                                    <div class="item-actions">
                                        <span class="btn btn-pure btn-icon" data-toggle="list-editable">
                                            <i class="icon wb-edit" aria-hidden="true"></i>
                                        </span>
                                        <span class="btn btn-pure btn-icon" data-tag="list-delete">
                                            <i class="icon wb-trash" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>



                            <a onclick='defaultModal({ "modalID": "contact-group-form" })' class="list-group-item" href="javascript:void(0)">
                                <i class="icon wb-plus" aria-hidden="true"></i> Add New Label
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>