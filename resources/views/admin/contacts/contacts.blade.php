@extends('site/master')
@section('title', 'Category')


@push('csrf_token')
      <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')

@include('site.menu')

<div class="page bg-white animsition">
@include('site.contacts.includes-group')

    <!-- Contacts Content -->
    <div class="page-main">

      <!-- Contacts Content Header -->
      <div class="page-header">
                  <h1 class="page-title">Contacts<span>{{$group_name}}</span></h1>
                  <div class="page-header-actions">

                  <ul>

                  <li>
                  <div class="btn-group">

<button class="btn btn-icon btn-pure btn-default btn-delete-selected" type="button">
<i class="icon wb-trash" aria-hidden="true"></i> Delete</button>

<button class="btn btn-icon btn-pure btn-default" type="button">
<i class="icon wb-download" aria-hidden="true"></i> Export</button>

<button class="btn btn-icon btn-pure btn-default btn-success" type="button"
onclick='defaultModal({ "modalID": "addContact" })'>
<i class="icon wb-plus" aria-hidden="true"></i> Add New</button>



                            </div>
                  </li>

<!--
                  <li>
                  <button type="button" class="btn btn-sm btn-success"
                  data-toggle="tooltip" data-original-title="Add New">
                            <i class="icon wb-plus" aria-hidden="true"></i> Add New
                          </button>
                  </li> -->
                  <li>
                  <div class="dropdown">
                                                  <button type="button" class="btn btn-pure dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                Filter
                                                                <span class="icon wb-chevron-down-mini" aria-hidden="true"></span>
                                                              </button>


                                                  <div class="dropdown-menu dropdown-filter dropdown-menu-right
                                                  animation-scale-up animation-top-right animation-duration-250" role="menu">

                                                      <input class="form-control input-sm" type="text" placeholder="Sender/Message" />

                                                      <div class="input-daterange" data-plugin="datepicker">
                                                          <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">
                                                              <i class="icon wb-calendar" aria-hidden="true"></i>
                                                            </span>
                                                            <input type="text" class="form-control" name="start" id="date-start" />
                                                          </div>
                                                          <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">to</span>
                                                            <input type="text" class="form-control" name="end" id="date-end" />
                                                          </div>
                                                        </div>

                                                      <button class="btn btn-sm btn-success">Submit</button>

                                                  </div>


                                                </div>
                  </li>
                  </ul>







                  </div>
                </div>






      <!-- Contacts Content -->
      <div class="page-content page-content-table">


        <!-- Contacts -->
        <table class="table is-indent tablesaw" data-tablesaw-mode="stack" data-plugin="animateList"
        data-animate="fade" data-child="tr" data-selectable="selectable">
          <thead>
            <tr>
              <th class="cell-30" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                <span class="checkbox-custom checkbox-primary checkbox-lg contacts-select-all">
                  <input type="checkbox" class="contacts-checkbox selectable-all" id="select_all"
                  />
                  <label for="select_all"></label>
                </span>
              </th>
              <th class="cell-300" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone</th>
              <th class="cell-400" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Name</th>
              <th class="cell-400" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Email</th>
              <th class="cell-30">ac</th>

            </tr>
          </thead>
          <tbody>
          @if(count($table) > 0)
          @foreach($table as $row)
            <tr>
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" value="{{$row->id}}" />
                  <label for="contacts_1"></label>
                </span>
              </td>
              <td class="cell-300">
                {{$row->phone}}
              </td>
              <td class="cell-300">{{$row->name}}</td>
              <td>{{$row->email}}</td>
              <td align="right">:</td>
            </tr>
            @endforeach
            @endif
          </tbody>
        </table>

        <ul data-plugin="paginator" data-total="50" data-skin="pagination-gap"></ul>
      </div>
    </div>
  </div>


   <!-- Site Action -->
      <div class="site-action">
      	<button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating"
      	data-toggle="tooltip" data-original-title="Add New Contact">
      		<i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
      		<i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
      	</button>
      	<div class="site-action-buttons">
      		<button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom"
      		data-toggle="tooltip" data-original-title="Delete Selected">
      			<i class="icon wb-trash" aria-hidden="true"></i>
      		</button>
      		<button type="button" data-action="folder" class="btn-raised btn btn-success btn-floating animation-slide-bottom"
      		data-toggle="tooltip" data-original-title="Edit Selected">
      			<i class="icon wb-edit" aria-hidden="true"></i>
      		</button>
      	</div>
      </div>
      <!-- End Site Action -->


@include('site.contacts.pop-add-group')
@include('site.contacts.pop-add-contacts')







    <!-- Add User Form -->
    <div class="modal fade" id="addUserForm" aria-hidden="true" aria-labelledby="addUserForm"
         role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
    				<h4 class="modal-title">Create New Contact</h4>
    			</div>
    			<div class="modal-body">
    				<form>
    					<div class="form-group">
    						<input type="text" class="form-control" name="name" placeholder="Name" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="phone" placeholder="Phone" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="email" placeholder="Email" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="address" placeholder="Address" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="birthday" placeholder="Birthday"
    							/>
    					</div>
    				</form>
    			</div>
    			<div class="modal-footer">
    				<button class="btn btn-primary" data-dismiss="modal" type="submit">Save</button>
    				<a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
    			</div>
    		</div>
    	</div>
    </div>
    <!-- End Add User Form -->

<script>
    var current_group_id = "{{$contact_group_id}}";
</script>


@include('site.footer')

  @endsection



  @push('styles')
      <link href="{{asset('assets/global/vendor/filament-tablesaw/tablesaw.min.css?v2.2.0')}}" />
      <link href="{{asset('assets/assets/examples/css/apps/contacts.min.css?v2.2.0')}}" />
  @endpush

  @push('scripts')

      <script src="{{asset('assets/global/vendor/filament-tablesaw/tablesaw.js')}}"></script>
      <script src="{{asset('assets/global/vendor/bootbox/bootbox.js')}}"></script>
      <script src="{{asset('assets/global/js/plugins/sticky-header.min.js')}}"></script>
      <script src="{{asset('assets/global/js/plugins/action-btn.min.js')}}"></script>
      <script src="{{asset('assets/global/js/plugins/selectable.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/animate-list.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/material.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/selectable.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/bootbox.min.js')}}"></script>
      <script src="{{asset('assets/assets/js/app.min.js?time='.time())}}"></script>
      <script src="{{asset('assets/assets/examples/js/apps/contacts.min.js?time='.time())}}"></script>
  @endpush