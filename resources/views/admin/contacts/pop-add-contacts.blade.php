<!-- Add Contact Form -->
    <div class="modal fade" id="addContact" aria-hidden="true" aria-labelledby="addContact"
         role="dialog" tabindex="-1">
    	<div class="modal-dialog modal-sidebar">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
    				<h4 class="modal-title">Create New Contact</h4>
    			</div>
    			<div class="modal-body">



    			<div class="nav-tabs-horizontal">
                                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                                    <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne"
                                      role="tab">Phone Numbers Only</a></li>
                                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo"
                                      role="tab">Detailed Contacts</a></li>
                                  </ul>
                                  <div class="tab-content padding-top-20">
                                    <div class="tab-pane active clearfix" id="exampleTabsOne" role="tabpanel">

{{ Form::open(['action' => 'Site\Contacts@add_contact', 'name' => 'form_add_contact','enctype'=>'multipart/form-data']) }}
<div class="form-group">
     <textarea class="form-control" rows="10" name="contact_list"
         placeholder="Paste contact here..."></textarea>
</div>

<div class="form-group">
<!--
                  <div class="input-group input-group-file">
                    <input type="text" class="form-control" readonly="">
                    <span class="input-group-btn">
                      <span class="btn btn-success btn-file">
                        <i class="icon wb-upload" aria-hidden="true"></i>
                        <input type="file" name="" multiple="">
                      </span>
                    </span>
                  </div>
                  -->

                  <input type="file" name="contact_upload" id="contact_upload">
</div>

<div class="pull-right">
    <button class="btn btn-primary" name="btnAddContact" type="submit">Save</button>
    <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
</div>
<input type="hidden" value="{{Input::get('id')}}" name="contact_group_id" />
{{ Form::close() }}

                                    </div>
                                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                      Negant parvos fructu nostram mutans supplicii ac dissentias, maius tibi licebit
                                      ruinae philosophia. Salutatus repellere titillaret expetendum
                                      ipsi. Cupiditates intellegam exercitumque privatio concederetur,
                                      sempiternum, verbis malint dissensio nullas noctesque earumque.
                                    </div>
                                  </div>
                                </div>





    			</div>

    		</div>
    	</div>
    </div>
    <!-- End Contact Form -->
