<div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
            <i class="icon wb-chevron-left" aria-hidden="true"></i>
            <i class="icon wb-chevron-right" aria-hidden="true"></i>
        </div>



        <div class="page-aside-inner" data-plugin="pageAsideScroll">
            <div data-role="container">
                <div data-role="content">
                    <div class="page-aside-section">
                        <div class="list-group">
                            <a class="list-group-item" href="{{asset('contacts')}}">
                                <span class="item-right total-all-contacts">61</span>
                                <i class="icon wb-inbox" aria-hidden="true"></i>All contacts
                            </a>
                        </div>
                    </div>
                    <div class="page-aside-section">
                        <h5 class="page-aside-title">LABEL</h5>
                        <div class="list-group has-actions">
                        <div class="contact-group-list"></div>
                        <!--
                            <div class="list-group-item">
                                <div class="list-content">
                                    <span class="item-right">32</span>
                                    <span class="list-text">bffedd</span>
                                    <div class="item-actions">
                                        <span class="btn btn-pure btn-icon">
                                            <i class="icon wb-edit" aria-hidden="true"></i>
                                        </span>
                                        <span class="btn btn-pure btn-icon" >
                                            <i class="icon wb-trash" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            -->

                            <a onclick='defaultModal({ "modalID": "addGroup" })' class="list-group-item" href="javascript:void(0)">
                                <i class="icon wb-plus" aria-hidden="true"></i> Add New Label
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>