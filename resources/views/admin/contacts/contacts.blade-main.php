@extends('site/master')
@section('title', 'Category')



@section('content')

@include('site.menu')





<div class="page bg-white animsition">
@include('site.contacts.includes-group')

    <!-- Contacts Content -->
    <div class="page-main">

      <!-- Contacts Content Header -->
      <div class="page-header">
                  <h1 class="page-title">Contacts: Work</h1>
                  <div class="page-header-actions">

                  <ul>

                  <li>
                  <div class="btn-group">

<button class="btn btn-icon btn-pure btn-default btn-sm" type="button">
<i class="icon wb-trash" aria-hidden="true"></i> Delete Selected</button>

<button class="btn btn-icon btn-pure btn-default btn-sm" type="button">
<i class="icon wb-download" aria-hidden="true"></i> Export</button>

<button class="btn btn-icon btn-pure btn-default btn-sm btn-success" type="button"
onclick='defaultModal({ "modalID": "addContact" })'>
<i class="icon wb-plus" aria-hidden="true"></i> Add New</button>



                            </div>
                  </li>

<!--
                  <li>
                  <button type="button" class="btn btn-sm btn-success"
                  data-toggle="tooltip" data-original-title="Add New">
                            <i class="icon wb-plus" aria-hidden="true"></i> Add New
                          </button>
                  </li> -->
                  <li>
                  <div class="dropdown">
                                                  <button type="button" class="btn btn-pure dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                Filter
                                                                <span class="icon wb-chevron-down-mini" aria-hidden="true"></span>
                                                              </button>


                                                  <div class="dropdown-menu dropdown-filter dropdown-menu-right
                                                  animation-scale-up animation-top-right animation-duration-250" role="menu">

                                                      <input class="form-control input-sm" type="text" placeholder="Sender/Message" />

                                                      <div class="input-daterange" data-plugin="datepicker">
                                                          <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">
                                                              <i class="icon wb-calendar" aria-hidden="true"></i>
                                                            </span>
                                                            <input type="text" class="form-control" name="start" id="date-start" />
                                                          </div>
                                                          <div class="input-group input-group-sm">
                                                            <span class="input-group-addon">to</span>
                                                            <input type="text" class="form-control" name="end" id="date-end" />
                                                          </div>
                                                        </div>

                                                      <button class="btn btn-sm btn-success">Submit</button>

                                                  </div>


                                                </div>
                  </li>
                  </ul>







                  </div>
                </div>






      <!-- Contacts Content -->
      <div class="page-content page-content-table">


        <!-- Contacts -->
        <table class="table is-indent tablesaw" data-tablesaw-mode="stack" data-plugin="animateList"
        data-animate="fade" data-child="tr" data-selectable="selectable">
          <thead>
            <tr>
              <th class="cell-30" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">
                <span class="checkbox-custom checkbox-primary checkbox-lg contacts-select-all">
                  <input type="checkbox" class="contacts-checkbox selectable-all" id="select_all"
                  />
                  <label for="select_all"></label>
                </span>
              </th>
              <th class="cell-300" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Name</th>
              <th class="cell-300" scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">Phone</th>
              <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">Email</th>

            </tr>
          </thead>
          <tbody>
            <tr>
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_1" data-content="id-190000000002"
                  value="3344211-334322"
                  />
                  <label for="contacts_1"></label>
                </span>
              </td>
              <td class="cell-300">
                Herman Beck
              </td>
              <td class="cell-300">(119)-298-8025</td>
              <td>julio.williamson73@gmail.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_2"
                  />
                  <label for="contacts_2"></label>
                </span>
              </td>
              <td class="cell-300">
                Mary Adams
              </td>
              <td class="cell-300">(838)-780-5116</td>
              <td>heidi.morrison77@hotmail.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_3"
                  />
                  <label for="contacts_3"></label>
                </span>
              </td>
              <td class="cell-300">
                Caleb Richards
              </td>
              <td class="cell-300">(861)-579-6099</td>
              <td>raymond.byrd19@aol.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_4"
                  />
                  <label for="contacts_4"></label>
                </span>
              </td>
              <td class="cell-300">
                June Lane
              </td>
              <td class="cell-300">(210)-727-1136</td>
              <td>eileen.gordon24@yahoo.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_5"
                  />
                  <label for="contacts_5"></label>
                </span>
              </td>
              <td class="cell-300">
                Edward Fletcher
              </td>
              <td class="cell-300">(729)-682-8842</td>
              <td>jim.hunter32@gmail.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_6"
                  />
                  <label for="contacts_6"></label>
                </span>
              </td>
              <td class="cell-300">
                Crystal Bates
              </td>
              <td class="cell-300">(916)-801-4120</td>
              <td>zoe.perez69@yahoo.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_7"
                  />
                  <label for="contacts_7"></label>
                </span>
              </td>
              <td class="cell-300">
                Nathan Watts
              </td>
              <td class="cell-300">(320)-725-3333</td>
              <td>esther.kelly84@hotmail.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_8"
                  />
                  <label for="contacts_8"></label>
                </span>
              </td>
              <td class="cell-300">
                Heather Harper
              </td>
              <td class="cell-300">(386)-653-1983</td>
              <td>jacob.morgan95@gmail.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_9"
                  />
                  <label for="contacts_9"></label>
                </span>
              </td>
              <td class="cell-300">
                Willard Wood
              </td>
              <td class="cell-300">(130)-591-7236</td>
              <td>jacob.morgan95@gmail.com</td>
              
            </tr>
            <tr >
              
              <td class="cell-30">
                <span class="checkbox-custom checkbox-primary checkbox-lg">
                  <input type="checkbox" class="contacts-checkbox selectable-item" id="contacts_10"
                  />
                  <label for="contacts_10"></label>
                </span>
              </td>
              <td class="cell-300">
                Ronnie Ellis
              </td>
              <td class="cell-300">(769)-963-2966</td>
              <td>erin.miller29@yahoo.com</td>
              
            </tr>
          </tbody>
        </table>

        <ul data-plugin="paginator" data-total="50" data-skin="pagination-gap"></ul>
      </div>
    </div>
  </div>


   <!-- Site Action -->
    <div class="site-action">
    	<button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating"
    	data-toggle="tooltip" data-original-title="Add New Contact">
    		<i class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
    		<i class="back-icon wb-close animation-scale-up" aria-hidden="true"></i>
    	</button>
    	<div class="site-action-buttons">
    		<button type="button" data-action="trash" class="btn-raised btn btn-success btn-floating animation-slide-bottom"
    		data-toggle="tooltip" data-original-title="Delete Selected">
    			<i class="icon wb-trash" aria-hidden="true"></i>
    		</button>
    		<button type="button" data-action="folder" class="btn-raised btn btn-success btn-floating animation-slide-bottom"
    		data-toggle="tooltip" data-original-title="Edit Selected">
    			<i class="icon wb-edit" aria-hidden="true"></i>
    		</button>
    	</div>
    </div>
    <!-- End Site Action -->





<!-- Add Contact Form -->
    <div class="modal fade" id="addContact" aria-hidden="true" aria-labelledby="addContact"
         role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
    				<h4 class="modal-title">Create New Contact</h4>
    			</div>
    			<div class="modal-body">





    			<div class="nav-tabs-horizontal">
                                  <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                                    <li class="active" role="presentation"><a data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne"
                                      role="tab">Copy & Paste Contact</a></li>
                                    <li role="presentation"><a data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo"
                                      role="tab">Upload Contact</a></li>
                                  </ul>
                                  <div class="tab-content padding-top-20">
                                    <div class="tab-pane active clearfix" id="exampleTabsOne" role="tabpanel">
                                      <form>
                                      <div class="form-group">
                                      <textarea class="form-control" rows="10" name="contact_list"
                                      placeholder="Paste contact here..."></textarea>
                                      </div>
                                      <div class="pull-right">
                                      <button class="btn btn-primary" type="submit">Save</button>
                                      <a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                                      </div>
                                      </form>
                                    </div>
                                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                      Negant parvos fructu nostram mutans supplicii ac dissentias, maius tibi licebit
                                      ruinae philosophia. Salutatus repellere titillaret expetendum
                                      ipsi. Cupiditates intellegam exercitumque privatio concederetur,
                                      sempiternum, verbis malint dissensio nullas noctesque earumque.
                                    </div>
                                  </div>
                                </div>





    			</div>

    		</div>
    	</div>
    </div>
    <!-- End Contact Form -->






<!-- Add Group Form -->
    <div class="modal fade" id="addGroup" aria-hidden="true" aria-labelledby="addGroup"
         role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
    				<h4 class="modal-title">Create New Contact Group</h4>
    			</div>

    			<div class="modal-body clearfix">
{{ Form::open(['action' => 'Site\login@loginSubmit']) }}

    			<div class="form-group">
    			<label class="label-control">Group Name:</label>
    			<input class="form-control" name="group_name" placeholder="Group Name" />
    			</div>


<div class="pull-right">
                    				<button class="btn btn-primary" name="btnSaveGroup" type="submit">Save</button>
                    				<a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                    			</div>

{{ Form::close() }}



    			</div>



    		</div>
    	</div>
    </div>
    <!-- End Group Form -->








    <!-- Add User Form -->
    <div class="modal fade" id="addUserForm" aria-hidden="true" aria-labelledby="addUserForm"
         role="dialog" tabindex="-1">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
    				<h4 class="modal-title">Create New Contact</h4>
    			</div>
    			<div class="modal-body">
    				<form>
    					<div class="form-group">
    						<input type="text" class="form-control" name="name" placeholder="Name" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="phone" placeholder="Phone" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="email" placeholder="Email" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="address" placeholder="Address" />
    					</div>
    					<div class="form-group">
    						<input type="text" class="form-control" name="birthday" placeholder="Birthday"
    							/>
    					</div>
    				</form>
    			</div>
    			<div class="modal-footer">
    				<button class="btn btn-primary" data-dismiss="modal" type="submit">Save</button>
    				<a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
    			</div>
    		</div>
    	</div>
    </div>
    <!-- End Add User Form -->


@include('site.footer')

  @endsection



  @push('styles')
      <link href="{{asset('assets/global/vendor/filament-tablesaw/tablesaw.min.css?v2.2.0')}}" />
      <link href="{{asset('assets/assets/examples/css/apps/contacts.min.css?v2.2.0')}}" />
  @endpush

  @push('scripts')

      <script src="{{asset('assets/global/vendor/filament-tablesaw/tablesaw.js')}}"></script>
      <script src="{{asset('assets/global/vendor/bootbox/bootbox.js')}}"></script>
      <script src="{{asset('assets/global/js/plugins/sticky-header.min.js')}}"></script>
      <script src="{{asset('assets/global/js/plugins/action-btn.min.js')}}"></script>
      <script src="{{asset('assets/global/js/plugins/selectable.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/animate-list.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/material.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/selectable.min.js')}}"></script>
      <script src="{{asset('assets/global/js/components/bootbox.min.js')}}"></script>
      <script src="{{asset('assets/assets/js/app.min.js')}}"></script>
      <script src="{{asset('assets/assets/examples/js/apps/contacts.min.js')}}"></script>
  @endpush