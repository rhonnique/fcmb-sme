
<!-- Add Group Form -->
    <div class="modal fade" id="addGroup" aria-hidden="true" aria-labelledby="addGroup"
         role="dialog" tabindex="-1">
    	<div class="modal-dialog modal-sidebar">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
    				<h4 class="modal-title">Create New Contact Group</h4>
    			</div>

    			<div class="modal-body clearfix">
{{ Form::open(['action' => 'Site\Contacts@create_contact_group']) }}

    			<div class="form-group">
    			<label class="label-control">Group Name:</label>
    			<input class="form-control" name="group_name" placeholder="Group Name" />
    			</div>


<div class="pull-right">
                    				<button class="btn btn-primary" name="btnSaveGroup" type="submit">Save</button>
                    				<a class="btn btn-sm btn-white" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                    			</div>

{{ Form::close() }}



    			</div>



    		</div>
    	</div>
    </div>
    <!-- End Group Form -->