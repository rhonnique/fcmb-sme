@extends('site/master')
@section('title', 'Category')



@section('content')


<div class="page bg-white">
  <div class="page-main">


<!--
                    <div class="input-daterange" data-plugin="datepicker">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control" name="start" />
                      </div>
                      <div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control" name="end" />
                      </div>
                    </div>
                    -->


    <div class="page-header">
      <h1 class="page-title" id="page-title">Sent</h1>
    </div>





    <div class="page-content page-content-table" data-selectable="selectable">

      <!-- Actions -->
      <div class="page-content-actions">
        <div class="pull-right filter">
          <div class="dropdown">
            <button type="button" class="btn btn-pure dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          Filter
                          <span class="icon wb-chevron-down-mini" aria-hidden="true"></span>
                        </button>


            <div class="dropdown-menu dropdown-filter dropdown-menu-right
            animation-scale-up animation-top-right animation-duration-250" role="menu">

                <input class="form-control input-sm" type="text" placeholder="Sender/Message" />

                <div class="input-daterange" data-plugin="datepicker">
                    <div class="input-group input-group-sm">
                      <span class="input-group-addon">
                        <i class="icon wb-calendar" aria-hidden="true"></i>
                      </span>
                      <input type="text" class="form-control" name="start" id="date-start" />
                    </div>
                    <div class="input-group input-group-sm">
                      <span class="input-group-addon">to</span>
                      <input type="text" class="form-control" name="end" id="date-end" />
                    </div>
                  </div>

                <button class="btn btn-sm btn-success">Submit</button>

            </div>


          </div>
        </div>
        <div class="actions-main">
          <span class="checkbox-custom checkbox-primary checkbox-lg inline-block vertical-align-bottom">
                        <input type="checkbox" class="mailbox-checkbox selectable-all" id="select_all"
                        />
                        <label for="select_all"></label>
                      </span>
          <div class="btn-group">

            <div class="dropdown">
              <button class="btn btn-icon btn-pure btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button"><i class="icon wb-trash" aria-hidden="true" data-toggle="tooltip"
                            data-original-title="Folder" data-container="body" title=""></i> Delete</button>
            </div>


            <div class="dropdown">
                <button class="btn btn-icon btn-pure btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button"><i class="icon wb-plus" aria-hidden="true" data-toggle="tooltip"
                              data-original-title="Folder" data-container="body" title=""></i> Add New</button>
              </div>


          </div>
        </div>
      </div>

      <!-- Mailbox -->
      <table id="mailboxTable" class="table" data-plugin="animateList" data-animate="fade" data-child="tr">
        <tbody>


          <!--
            <tr id="' + data.id + '" data-mailbox="slidePanel" ' + ("true" === data.unread ? 'class="unread"' : "") + '><td class="cell-60"><span class="checkbox-custom checkbox-primary checkbox-lg"><input type="checkbox" class="mailbox-checkbox selectable-item" id="mail_' + data.id + '"/><label for="mail_' + data.id + '"></label></span></td><td class="cell-30 responsive-hide"><span class="checkbox-important checkbox-default"><input type="checkbox" class="mailbox-checkbox mailbox-important" ' + ("true" === data.starred ? 'checked="checked"' : "") + ' id="mail_' + data.id + '_important"/><label for="mail_' + data.id + '_important"></label></span></td><td class="cell-60 responsive-hide"><a class="avatar" href="javascript:void(0)"><img class="img-responsive" src="' + data.avatar + '" alt="..."></a></td><td><div class="content"><div class="title">' + data.name + '</div><div class="abstract">' + data.title + '</div></div></td><td class="cell-30 responsive-hide">' + (data.attachments.length > 0 ? '<i class="icon wb-paperclip" aria-hidden="true"></i>' : "") + '</td><td class="cell-130"><div class="time">' + data.time + "</div>" + (data.group.length > 0 ? '<div class="identity"><i class="wb-medium-point ' + data.color + '" aria-hidden="true"></i>' + data.group + "</div>" : "") + "</td></tr>
          -->

          <tr>
            <td class="cell-60"><span class="checkbox-custom checkbox-primary checkbox-lg"><input type="checkbox" class="mailbox-checkbox selectable-item" id="mail_mid_1"><label for="mail_mid_1"></label></span></td>
                <td>
                  <div class="content">
                    <div class="title">OKAVANGOSMS</div>
                    <div class="abstract">Buy 100 - 9,999 units of SMS @ 2.00 10,000 - 99,999 @ 1.50 100,000 - and above @ 1.10 Offer last till 30th of Sept. 2017 And get 10 free SMS units when you register Call 08108449476 / 08023069470 visit www.okavangobulksms.com</div>
                    <div class="inline-action">
                    <span><i class="icon wb-table"></i> Open/Forward</span>
                    <span><i class="icon wb-pie-chart"></i> View Report</span>
                    <span><i class="icon wb-download"></i> Download Report</span>
                    </div>
                  </div>
                </td>
                <td class="cell-30 responsive-hide"></td>
                <td class="cell-130 font-size-12">
                  <div class="time">2 hours ago</div>
                  <div class="identity">&#8358; 2.50</div>
                </td>
          </tr>


          <tr>
              <td class="cell-60"><span class="checkbox-custom checkbox-primary checkbox-lg"><input type="checkbox" class="mailbox-checkbox selectable-item" id="mail_mid_1"><label for="mail_mid_1"></label></span></td>
                  <td>
                    <div class="content">
                      <div class="title">Sclogistics</div>
                      <div class="abstract">3 days to go as Simplified Corporate Logistics unveils her One click import/export solutions for Nigeria Business. Click on http://www.sclogisticsng.com/event_registration.php to register for free or call Onyinye on 08038323999</div>
                    </div>
                  </td>
                  <td class="cell-30 responsive-hide"></td>
                  <td class="cell-130">
                    <div class="time">2 hours ago</div>
                    <div class="identity">200.00 NGN</div>
                  </td>
            </tr>



            <tr>
                <td class="cell-60"><span class="checkbox-custom checkbox-primary checkbox-lg"><input type="checkbox" class="mailbox-checkbox selectable-item" id="mail_mid_1"><label for="mail_mid_1"></label></span></td>
                    <td>
                      <div class="content">
                        <div class="title">Mercylink</div>
                        <div class="abstract">Test Personalise ID: 451436683896173 Name: Adeshola Makinwa Lacation: Allen. Ikeja</div>
                        <div class="inline-action">
                          <span><i class="icon wb-plus"></i> Open/Forward</span>
                          <span><i class="icon wb-plus"></i> View Report</span>
                          <span><i class="icon wb-plus"></i> Download Report</span>
                        </div>
                      </div>
                    </td>
                    <td class="cell-30 responsive-hide"></td>
                    <td class="cell-130">
                      <div class="time">2 hours ago</div>
                      <div class="identity">200.00 NGN</div>
                    </td>
              </tr>


              <tr>
                  <td class="cell-60"><span class="checkbox-custom checkbox-primary checkbox-lg"><input type="checkbox" class="mailbox-checkbox selectable-item" id="mail_mid_1"><label for="mail_mid_1"></label></span></td>
                      <td>
                        <div class="content">
                          <div class="title">Edward Fletcher</div>
                          <div class="abstract">Genus alteram linguam ut isdem, desiderent litteris allicit acuti iuvaret</div>
                        </div>
                      </td>
                      <td class="cell-30 responsive-hide"></td>
                      <td class="cell-130">
                        <div class="time">2 hours ago</div>
                        <div class="identity">200.00 NGN</div>
                      </td>
                </tr>


                <tr>
                    <td class="cell-60"><span class="checkbox-custom checkbox-primary checkbox-lg"><input type="checkbox" class="mailbox-checkbox selectable-item" id="mail_mid_1"><label for="mail_mid_1"></label></span></td>
                        <td>
                          <div class="content">
                            <div class="title">Edward Fletcher</div>
                            <div class="abstract">Genus alteram linguam ut isdem, desiderent litteris allicit acuti iuvaret</div>
                          </div>
                        </td>
                        <td class="cell-30 responsive-hide"></td>
                        <td class="cell-130">
                          <div class="time">2 hours ago</div>
                          <div class="identity">200.00 NGN</div>
                        </td>
                  </tr>


        </tbody>
      </table>
      <!-- pagination -->
      <ul data-plugin="paginator" data-total="50" data-skin="pagination-gap"></ul>
    </div>



  </div>
</div>

@endsection

@push('styles')
    <link href="{{asset('assets/assets/examples/css/apps/mailbox.min.css')}}" />
    <link href="{{asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" />
    <link href="{{asset('assets/assets/examples/css/forms/advanced.min.css?v2.2.0')}}" />
@endpush


@push('scripts')
    <script src="{{asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/global/js/components/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('assets/global/js/plugins/selectable.min.js')}}"></script>
    <script src="{{asset('assets/global/js/components/selectable.min.js')}}"></script>
    <script src="{{asset('assets/global/js/components/table.min.js')}}"></script>

    <script src="{{asset('assets/assets/examples/js/forms/advanced.min.js')}}"></script>
@endpush















