
        <button type="button" class="btn btn-sm btn-icon btn-round btn-success waves-effect waves-classic panel-show-new"
                data-toggle="tooltip" data-original-title="Add New">
                  <i class="icon md-plus" aria-hidden="true"></i>
       </button>

        <button type="button" class="btn btn-sm btn-icon btn-round btn-danger waves-effect waves-classic btn-delete-selected"
        data-toggle="tooltip" data-original-title="Delete Selected">
          <i class="icon md-delete" aria-hidden="true"></i>
        </button>


<span class="btn-search-wrapper">
        <button type="button" class="btn btn-sm btn-icon
        btn-round btn-info waves-effect waves-classic toggle-search"
                data-toggle="tooltip" data-original-title="Search">
                  <i class="icon md-search" aria-hidden="true"></i>
                </button>

                <div class="search-wrapper">

                <div class="form-group">

                <input class="form-control form-control form-control-sm"
                value="{{$select['details']}}"
                type="text" id="s_details" placeholder="Verse/Passage" />
</div>



<div class="form-group clearfix">
                <div class="input-daterange" data-plugin="datepicker">
                                                    <div class="input-group input-group-sm">
                                                      <span class="input-group-addon">
                                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                                      </span>
                                                      <input type="text" class="form-control" name="start"
                                                      value="{{$select['date_from']}}"
                                                       id="s_date_from" placeholder="From" readonly/>
                                                    </div>
                                                    <div class="input-group input-group-sm">
                                                      <span class="input-group-addon">to</span>
                                                      <input type="text" class="form-control" name="end"
                                                      value="{{$select['date_to']}}"
                                                      id="s_date_to" placeholder="To" readonly />
                                                    </div>
                                                  </div>

</div>


<div>
<div class="pull-left">
<button class="btn btn-default btn-sm waves-effect waves-classic btn-search-cancel btn-close-search">Cancel</button>
</div>
<div class="pull-right">
<button class="btn btn-success btn-sm waves-effect waves-classic btn-search-submit">Submit</button>
</div>
</div>




                </div>
</span>

