<html>
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    @stack('csrf_token')

  <title>@yield('title') - City Of David</title>
  <link rel="apple-touch-icon" href="assets/assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="assets/global/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="assets/assets/css/site.min.css">
  <link rel="stylesheet" href="assets/assets/css/app.css?time={{time()}}">
  <!-- Plugins -->
  <link rel="stylesheet" href="assets/global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="assets/global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="assets/global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="assets/global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="assets/global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="assets/global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="assets/global/vendor/waves/waves.css">
  <link rel="stylesheet" href="assets/global/vendor/select2/select2.css">
  <link rel="stylesheet" href="assets/global/vendor/bootstrap-markdown/bootstrap-markdown.css">
  <link rel="stylesheet" href="assets/global/vendor/bootstrap-select/bootstrap-select.css">

  <link rel="stylesheet" href="assets/global/vendor/slick-carousel/slick.css">
  <link rel="stylesheet" href="assets/global/vendor/plyr/plyr.css">
  <link rel="stylesheet" href="assets/global/vendor/magnific-popup/magnific-popup.css">
  <link rel="stylesheet" href="assets/assets/examples/css/advanced/lightbox.css">
  <link rel="stylesheet" href="assets/global/vendor/jquery-wizard/jquery-wizard.css">
  <link rel="stylesheet" href="assets/global/vendor/formvalidation/formValidation.css">

   <link rel="stylesheet" href="assets/global/vendor/alertify/alertify.css">

  <!--
  <link rel="stylesheet" href="assets/assets/examples/css/pages/email.css">
  <link rel="stylesheet" href="assets/assets/examples/css/pages/login-v3.css">

  <link rel="stylesheet" href="assets/assets/examples/css/pages/profile.css">
  <link rel="stylesheet" href="assets/global/vendor/plyr/plyr.css">
  <link rel="stylesheet" href="assets/global/vendor/magnific-popup/magnific-popup.css">
   <link rel="stylesheet" href="assets/assets/examples/css/pages/profile-v2.css"> -->
<!--
  <link rel="stylesheet" href="assets/assets/examples/css/apps/message.css">

  <link rel="stylesheet" href="assets/assets/examples/css/pages/login-v3.css">
  <link rel="stylesheet" href="assets/assets/examples/css/dashboard/v1.css"> -->
  <!-- Fonts -->
  <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="assets/global/fonts/brand-icons/brand-icons.min.css">
  <link rel="stylesheet" href="assets/global/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="assets/global/fonts/ionicons/ionicons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

  @stack('styles')

  <script src="assets/global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>

@yield('content')





  <script src="assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="assets/global/vendor/jquery/jquery.js"></script>
  <script src="assets/global/vendor/tether/tether.js"></script>
  <script src="assets/global/vendor/bootstrap/bootstrap.js"></script>
  <script src="assets/global/vendor/animsition/animsition.js"></script>
  <script src="assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="assets/global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="assets/global/vendor/switchery/switchery.min.js"></script>
  <script src="assets/global/vendor/intro-js/intro.js"></script>
  <script src="assets/global/vendor/screenfull/screenfull.js"></script>
  <script src="assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="assets/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="assets/global/vendor/slick-carousel/slick.js"></script>
  <script src="assets/global/vendor/plyr/plyr.js"></script>
  <script src="assets/global/vendor/magnific-popup/jquery.magnific-popup.js"></script>
  <script src="assets/global/vendor/autosize/autosize.js"></script>
  <script src="assets/global/vendor/aspaginator/jquery.asPaginator.min.js"></script>
  <script src="assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="assets/global/vendor/formvalidation/formValidation.js"></script>
  <script src="assets/global/vendor/formvalidation/framework/bootstrap.js"></script>
  <script src="assets/global/vendor/jquery-wizard/jquery-wizard.js"></script>
  <script src="assets/global/vendor/bootstrap-markdown/bootstrap-markdown.js"></script>
  <script src="assets/global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="assets/global/vendor/marked/marked.js"></script>
  <script src="assets/global/vendor/to-markdown/to-markdown.js"></script>

  <script src="assets/global/vendor/alertify/alertify.js"></script>



<!--


  <script src="assets/global/vendor/chartist/chartist.min.js"></script>
  <script src="assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js"></script>
  <script src="assets/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
  <script src="assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
  <script src="assets/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="assets/global/vendor/peity/jquery.peity.min.js"></script>

-->
  <!-- Scripts -->
  <script src="assets/global/js/State.js"></script>
  <script src="assets/global/js/Component.js"></script>
  <script src="assets/global/js/Plugin.js"></script>
  <script src="assets/global/js/Base.js"></script>
  <script src="assets/global/js/Config.js"></script>
  <script src="assets/assets/js/Section/Menubar.js"></script>
  <script src="assets/assets/js/Section/GridMenu.js"></script>
  <script src="assets/assets/js/Section/Sidebar.js"></script>
  <script src="assets/assets/js/Section/PageAside.js"></script>
  <script src="assets/assets/js/Plugin/menu.js"></script>
  <script src="assets/global/js/config/colors.js"></script>
  <script src="assets/assets/js/config/tour.js"></script>

  <script>
  Config.set('assets', 'assets/assets');
  </script>
  <!-- Page -->
  <script src="assets/assets/js/Site.js"></script>
  <script src="assets/global/js/Plugin/asscrollable.js"></script>
  <script src="assets/global/js/Plugin/slidepanel.js"></script>
  <script src="assets/global/js/Plugin/matchheight.js"></script>
  <script src="assets/global/js/Plugin/jquery-placeholder.js"></script>
  <script src="assets/global/js/Plugin/material.js"></script>
  <script src="assets/assets/examples/js/pages/profile-v2.js"></script>
  <script src="assets/global/js/Plugin/switchery.js"></script>
  <script src="assets/global/js/Plugin/bootstrap-select.js"></script>
  <script src="assets/global/js/Plugin/plyr.js"></script>
  <script src="assets/global/js/Plugin/magnific-popup.js"></script>
  <script src="assets/assets/js/BaseApp.js"></script>
  <script src="assets/assets/js/App/Work.js"></script>

  <script src="https://cdn.jsdelivr.net/hls.js/latest/hls.js"></script>

  @stack('scripts')

      <!--

  <script src="assets/global/js/plyr.min.js"></script>


    <script src="assets/global/js/Plugin/toastr.js"></script>
  <script src="assets/global/js/Plugin/jvectormap.js"></script>
  <script src="assets/global/js/Plugin/peity.js"></script>
  <script src="assets/global/js/Plugin/switchery.js"></script>
  <script src="assets/global/js/Plugin/plyr.js"></script>
  <script src="assets/global/js/Plugin/magnific-popup.js"></script>
  <script src="assets/assets/examples/js/pages/profile-v2.js"></script>
  <script src="assets/assets/examples/js/pages/profile_v3.js"></script>
  <script src="assets/assets/js/App/Message.js"></script>
  <script src="assets/assets/examples/js/apps/message.js"></script>
  <script src="assets/global/js/Plugin/responsive-tabs.js"></script>
  <script src="assets/global/js/Plugin/tabs.js"></script>
  <script src="assets/global/js/Plugin/toastr.js"></script>
  <script src="assets/assets/examples/js/advanced/bootbox-sweetalert.js"></script>
-->

</body>
</html>
