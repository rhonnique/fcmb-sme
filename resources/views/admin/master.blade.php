<html>
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    @stack('csrf_token')

  <title>@yield('title') - City Of David</title>
  <link rel="apple-touch-icon" href="{{asset('assets/assets/images/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{asset('assets/assets/images/favicon.ico')}}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{asset('assets/global/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/css/bootstrap-extend.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/assets/css/site.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/assets/css/app.css?time='.time())}}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{asset('assets/global/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/select2/select2.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-markdown/bootstrap-markdown.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-select/bootstrap-select.css')}}">

  <link rel="stylesheet" href="{{asset('assets/global/vendor/slick-carousel/slick.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/plyr/plyr.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/magnific-popup/magnific-popup.css')}}">
  <link rel="stylesheet" href="{{asset('assets/assets/examples/css/advanced/lightbox.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/jquery-wizard/jquery-wizard.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/formvalidation/formValidation.css')}}">

   <link rel="stylesheet" href="{{asset('assets/global/vendor/alertify/alertify.css')}}">

  <!--
  <link rel="stylesheet" href="{{asset('assets/assets/examples/css/pages/email.css')}}">
  <link rel="stylesheet" href="{{asset('assets/assets/examples/css/pages/login-v3.css')}}">

  <link rel="stylesheet" href="{{asset('assets/assets/examples/css/pages/profile.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/plyr/plyr.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/vendor/magnific-popup/magnific-popup.cs')}}s">
   <link rel="stylesheet" href="{{asset('assets/assets/examples/css/pages/profile-v2.css')}}"> -->
<!--
  <link rel="stylesheet" href="{{asset('assets/assets/examples/css/apps/message.css')}}">

  <link rel="stylesheet" href="{{asset('assets/assets/examples/css/pages/login-v3.css')}}">
  <link rel="stylesheet" href="{{asset('assets/assets/examples/css/dashboard/v1.css')}}"> -->
  <!-- Fonts -->
  <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{asset('assets/global/fonts/material-design/material-design.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/fonts/brand-icons/brand-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/fonts/web-icons/web-icons.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/global/fonts/ionicons/ionicons.min.css')}}">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

  @stack('styles')

  <script src="{{asset('assets/global/vendor/breakpoints/breakpoints.js')}}"></script>
  <script>
  Breakpoints();
  </script>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body class="{{isset($page_body_class)?$page_body_class:''}} animsition">

@if(Session::has('admin_0721983'))
@include('admin.menu')


<div class="page">


<div class="page-header">
      <h1 class="page-title">
      {{isset($page_title)?$page_title:''}}</h1>
      <div class="page-header-actions">
      @if(isset($page_header_action))
          @include($page_header_action)
      @endif
      </div>
    </div>


    <div class="page-content">


    @if(Session::has('msg_ok') || Session::has('msg_error'))

    <div class="alert dark alert-{{ Session::has('msg_ok')?'success':'danger'  }} alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                      {{ Session::has('msg_ok')?Session('msg_ok'):Session('msg_error') }}
                    </div>
    @endif

@yield('content')


</div>

</div>

@include('admin.footer')

@else
@yield('content')
@endif

  <script src="{{asset('assets/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
  <script src="{{asset('assets/global/vendor/jquery/jquery.js')}}"></script>
  <script src="{{asset('assets/global/vendor/tether/tether.js')}}"></script>
  <script src="{{asset('assets/global/vendor/bootstrap/bootstrap.js')}}"></script>
  <script src="{{asset('assets/global/vendor/animsition/animsition.js')}}"></script>
  <script src="{{asset('assets/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
  <script src="{{asset('assets/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
  <script src="{{asset('assets/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
  <script src="{{asset('assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
  <script src="{{asset('assets/global/vendor/waves/waves.js')}}"></script>
  <!-- Plugins -->
  <script src="{{asset('assets/global/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{asset('assets/global/vendor/intro-js/intro.js')}}"></script>
  <script src="{{asset('assets/global/vendor/screenfull/screenfull.js')}}"></script>
  <script src="{{asset('assets/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
  <script src="{{asset('assets/global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
  <script src="{{asset('assets/global/vendor/slick-carousel/slick.js')}}"></script>
  <script src="{{asset('assets/global/vendor/plyr/plyr.js')}}"></script>
  <script src="{{asset('assets/global/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
  <script src="{{asset('assets/global/vendor/autosize/autosize.js')}}"></script>
  <script src="{{asset('assets/global/vendor/aspaginator/jquery.asPaginator.min.js')}}"></script>
  <script src="{{asset('assets/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
  <script src="{{asset('assets/global/vendor/formvalidation/formValidation.js')}}"></script>
  <script src="{{asset('assets/global/vendor/formvalidation/framework/bootstrap.js')}}"></script>
  <script src="{{asset('assets/global/vendor/jquery-wizard/jquery-wizard.js')}}"></script>
  <script src="{{asset('assets/global/vendor/bootstrap-markdown/bootstrap-markdown.js')}}"></script>
  <script src="{{asset('assets/global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
  <script src="{{asset('assets/global/vendor/marked/marked.js')}}"></script>
  <script src="{{asset('assets/global/vendor/to-markdown/to-markdown.js')}}"></script>

  <script src="{{asset('assets/global/vendor/alertify/alertify.js')}}"></script>



<!--


  <script src="{{asset('assets/global/vendor/chartist/chartist.min.js')}}"></script>
  <script src="{{asset('assets/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js')}}"></script>
  <script src="{{asset('assets/global/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
  <script src="{{asset('assets/global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
  <script src="{{asset('assets/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
  <script src="{{asset('assets/global/vendor/peity/jquery.peity.min.js')}}"></script>

-->
  <!-- Scripts -->
  <script src="{{asset('assets/global/js/State.js')}}"></script>
  <script src="{{asset('assets/global/js/Component.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin.js')}}"></script>
  <script src="{{asset('assets/global/js/Base.js')}}"></script>
  <script src="{{asset('assets/global/js/Config.js')}}"></script>
  <script src="{{asset('assets/assets/js/Section/Menubar.js')}}"></script>
  <script src="{{asset('assets/assets/js/Section/GridMenu.js')}}"></script>
  <script src="{{asset('assets/assets/js/Section/Sidebar.js')}}"></script>
  <script src="{{asset('assets/assets/js/Section/PageAside.js')}}"></script>
  <script src="{{asset('assets/assets/js/Plugin/menu.js')}}"></script>
  <script src="{{asset('assets/global/js/config/colors.js')}}')}}"></script>
  <script src="{{asset('assets/assets/js/config/tour.js')}}"></script>

  <script>
  Config.set('assets', 'assets/assets');
  </script>
  <!-- Page -->
  <script src="{{asset('assets/assets/js/Site.js?time='.time())}}"></script>
  <script src="{{asset('assets/global/js/Plugin/asscrollable.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/slidepanel.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/matchheight.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/jquery-placeholder.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/material.js')}}"></script>
  <script src="{{asset('assets/assets/examples/js/pages/profile-v2.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/switchery.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/bootstrap-select.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/plyr.js')}}"></script>
  <script src="{{asset('assets/global/js/Plugin/magnific-popup.js')}}"></script>
  <script src="{{asset('assets/assets/js/BaseApp.js')}}"></script>
  <script src="{{asset('assets/assets/js/App/Work.js')}}"></script>

  <script src="https://cdn.jsdelivr.net/hls.js/latest/hls.js"></script>


  <script>
  var site_url = '{{url('/')}}';
  </script>

  @stack('scripts')



</body>
</html>
