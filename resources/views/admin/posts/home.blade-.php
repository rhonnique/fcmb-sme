@extends('admin.master')
@section('title', $page_title)


@section('content')


<div class="panel">
<div class="panel-body">

  <textarea id="content">Easy (and free!) You should check out our premium features.</textarea>

</div>
</div>

@endsection



@push('scripts')


<script src="{{asset('assets/global/vendor/tinymce/tinymce.min.js')}}"></script>


<script>

            tinymce.init({
            			selector: "textarea",theme: "modern",width: '100%',height: 600,
            			plugins: [
            				"advlist autolink link image lists charmap  preview hr anchor pagebreak",
            				"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            				"table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            			],
            			toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            			toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            			image_advtab: true ,

            			external_filemanager_path:"{{asset("filemanager")}}/",
            			filemanager_title:"File Manager" ,
            			external_plugins: { "filemanager" : "{{asset("filemanager/plugin.min.js")}}"}
            		});
        		
</script>

@endpush
