@extends('admin.master')
@section('title', $page_title)


@section('content')



<div class="panel panel-table-bordered">


@if($table->count() > 0)
{{ Form::open(['action' => 'Admin\Posts@delete', 'id' => 'form_table']) }}
<div class="table-responsive">

    <table class="table table-bordered font-size-14">
        							<thead>
        								<tr>
        									<th width="1%">


        									<span class="checkbox-custom checkbox-primary">
                                                                        <input class="selectable-all" type="checkbox">
                                                                        <label></label>
                                                                      </span>


        									</th>
        									<th width="50%">Name</th>
        									<th width="20%" >Date Created</th>
        									<th width="20%">Last Updated</th>
        									<th width="1%" class="text-center">
        									<i class="icon wb-edit" aria-hidden="true"></i>
        									</th>
        								</tr>
        							</thead>
        							<tbody>
        							@foreach ($table as $row)

        								<tr>
        									<td>
        									<span class="checkbox-custom checkbox-primary">
                                               <input class="selectable-item" type="checkbox" name="table_row_selected[]"
                                               value="{{$row->id}}">
                                                   <label></label>
                                             </span>
        									</td>
        									<td>{{$row->name}}</td>
        									<td >
        									{{$row->created_at}}
        									</td>
        									<td>
        									{{$row->updated_at}}
        									</td>

        									<td class="text-center">

        									<button type="button" class="btn btn-pure btn-dark icon wb-edit no-effect"
        									onclick="window.location='{{url('posts/'.$row->id.'/edit')}}'"></button>

    <!--

        									<div class="btn-group dropdown">
                                                                <button type="button" class="btn btn-info dropdown-toggle" id="exampleBulletDropdown2"
                                                                  data-toggle="dropdown" aria-expanded="false">
                                                                  <i class="icon md-more" aria-hidden="true"></i>
                                                                </button>
                                                                <div class="dropdown-menu bullet dropdown-menu-right" aria-labelledby="exampleBulletDropdown2"
                                                                  role="menu">
                                                                  <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Action</a>
                                                                  <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Another action</a>
                                                                  <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Something else here</a>
                                                                  <div class="dropdown-divider"></div>
                                                                  <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Separated link</a>
                                                                </div>
                                                              </div>
                                                              -->
        									</td>
        								</tr>

        								@endforeach
        							</tbody>
        						</table>

</div>
{{Form::close()}}
@else
<div class="panel-body" style="padding-top: 15px">No result found!</div>
@endif


</div>



@endsection



@push('scripts')

<script>
        		
</script>

@endpush
