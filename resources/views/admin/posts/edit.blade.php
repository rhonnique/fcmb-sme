@extends('admin.master')
@section('title', $page_title)



@section('content')


<div class="panel">
<div class="panel-body">
{{ Form::open(['method' => 'PATCH','action' => ['Admin\Posts@update', $details->id ]]) }}
<div class="form-group">
<input type="text" class="form-control" name="name" placeholder="Title"
 value="{{$details->name}}"/>
</div>

<div class="form-group">
  <textarea id="content" name="content">{{$details->post_content}}</textarea>

  </div>


  <div class="form-group">
  <button class="btn btn-success waves-effect waves-classic" type="submit"
                         id="btnSubmitAdd">Save</button>
  </div>
{{Form::close()}}
</div>
</div>

@endsection




@push('scripts')


<script src="{{asset('assets/global/vendor/tinymce/tinymce.min.js')}}"></script>


<script>

            tinymce.init({
            			selector: "textarea",theme: "modern",width: '100%',height: 600,
            			plugins: [
            				"advlist autolink link image lists charmap  preview hr anchor pagebreak",
            				"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            				"table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
            			],
            			toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            			toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            			image_advtab: true ,

            			external_filemanager_path:"{{asset("filemanager")}}/",
            			filemanager_title:"File Manager" ,
            			external_plugins: { "filemanager" : "{{asset("filemanager/plugin.min.js")}}"}
            		});

</script>

@endpush