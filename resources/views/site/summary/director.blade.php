
@if(isset($dir_bvn_number))
@for($i=0; $i < sizeof($dir_bvn_number); $i++)
@php $id = ($i + 1) @endphp

<div class="clearfix segment-list">
 @if($id != 1)<hr class="divider" />@endif
<div class="col-xs-12">
                <h4 class="segment-title">DIRECTOR {{$id}}</h4>
                </div>
					<div class="col-md-6 col-lg-6">
						<!-- Column 1 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6">BVN </label>
                      <div class="col-sm-6">{{$dir_bvn_number[$i]}}</div>
					</div>

					<div class="form-group row form-icons">
                      <label class="col-sm-6">
					  Title </label>
                      <div class="col-sm-6">{{$dir_title[$i]}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Name </label>
                      <div class="col-sm-6">{{$dir_name[$i]}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Gender </label>
                      <div class="col-sm-6">{{$dir_gender[$i]}}</div>
					</div>

					<div class="form-group row">
                                          <label class="col-sm-6">Nationality </label>
                                          <div class="col-sm-6">{{$dir_nationality[$i]}}</div>
                    					</div>
<!-- Column 1 ends -->
					</div>

					<div class="col-md-6 col-lg-6">
						<!-- Column 2 Starts -->

					<div class="form-group row">
                      <label class="col-sm-6">Country of Residence </label>
                      <div class="col-sm-6">{{country($dir_residence_country[$i])->name}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Address </label>
                      <div class="col-sm-6">{{$dir_address[$i]}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Date Of Birth </label>
                      <div class="col-sm-6">{{$dir_birthdate[$i]}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Phone Number </label>
                      <div class="col-sm-6">{{$dir_phone[$i]}}</div>
					</div>


					<div class="form-group row">
                      <label class="col-sm-6">Occupation </label>
                      <div class="col-sm-6">{{$dir_occupation[$i]}}</div>
					</div>

					<!-- Column 2 ends -->
					</div>

				</div>
				@endfor
@endif