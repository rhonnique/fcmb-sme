
<div class="clearfix segment-list segment-list-{{$id}}">
 @if($id != 2)<hr class="divider" />@endif
<div class="col-xs-12">
                <h4 class="segment-title">SIGNATORY <span></span>
                @if($id != 2)
                <button type="button" class="btn btn-xs btn-danger"
                onclick="remove_signatory({{$id}})">- REMOVE</button>
                @endif
                </h4>
                </div>
					<div class="col-md-6 col-lg-6">
						<!-- Column 1 Starts -->
						@if($id == 2)
						<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Signatory Type<span>*</span> </label>
                      <div class="col-sm-6">
					  {!! Form::select('sign_type', [
												'Single' => 'Single',
												'Multiple' => 'Multiple'
												], null, ['class' => 'form-control segment-signatory-type',
												'onclick' => 'toggle_signatory_type(this.value)']) !!}
                      </div>
					</div>
					@endif

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">BVN<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="sign_bvn_number[]" class="form-control segment-bvn-number"
					  value="96243758678">
					  <section class="text-center">
						  <p class="font-size-12">Forgotten Your BVN? Dial *565*0# to Retrieve it</p>
						  <button type="button" class="btn btn-theme btn-validate-bvn" onclick="validate_bvn({{$id}}, 2)
						  ">VALIDATE BVN</button>
					  </section>
                      </div>
					</div>

					<div class="form-group row form-icons">
                      <label class="col-sm-6 col-form-label">
					  Title<span>*</span> </label>
                      <div class="col-sm-6">
					  {!! Form::select('sign_title[]', ["ALHAJI","ALHAJA","ARCHITECT","BARRISTER","CHIEF","DEACON","DOCTOR/DR.","ELDER","ENGINEER","EVANGELIST","EXCELLENCY","HIGHCHIEF","HONOURABLE/HONBL","HISROYALHIGHNESS","JUSTICE","MR&MRS","M/S","MASTER","MASTER","MAZI","MESSERS","MISS","MISTER","MISTER","MR&MRS","MR","MRS.","MISS/MS","ALHAJI","OBA","OTUNBA","OTUNBA","PASTOR","PASTOR","PROFESSOR","REVEREND","SIR"], null, ['class' => 'form-control segment-title"','placeholder' => '-- Select Title --']) !!}
					  </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Name<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="sign_name[]" readonly class="form-control segment-name">
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Gender<span>*</span> </label>
                      <div class="col-sm-6 form-control-box">
						  <label class="radio-inline" for="sign_gender_male_{{$id}}">
					  <input type="radio" disabled name="sign_gender_[{{$id}}]" class="segment-gender" 
					  value="Male" id="sign_gender_male_{{$id}}">Male</label>

					  <label class="radio-inline" for="sign_gender_female_{{$id}}">
					  <input type="radio" disabled name="sign_gender_[{{$id}}]" class="segment-gender" 
					  value="Female" id="sign_gender_female_{{$id}}">Female</label>
					  <input type="hidden" name="sign_gender[]" class="segment-gender-selected"  />
                      </div>
					</div>


					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Nationality<span>*</span> </label>
                      <div class="col-sm-6">
					  <select name="" class="form-control segment-nationality" name="sign_nationality[]" readonly></select>
                      </div>
					</div>
<!-- Column 1 ends -->
					</div>

					<div class="col-md-6 col-lg-6">
						<!-- Column 2 Starts -->

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Country of Residence<span>*</span> </label>
                      <div class="col-sm-6">
					  {!! Form::select('sign_residence_country[]', array(
                                            'Top Countries' => $countries_top,
  'Other Countries' => $countries,
), null, ['class' => 'form-control segment-residence-country','placeholder' => '-- Select --']) !!}
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Address<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="sign_address[]"
                                            class="form-control segment-address">
                      </div>
					</div>

					<div class="form-group row form-icons">
                      <label class="col-sm-6 col-form-label">Date Of Birth<span>*</span> </label>
                      <div class="col-sm-6">

					  <div class="input-group">
					<i class="form-control-icon form-control-icon-right glyphicon glyphicon-calendar"></i>
					<input placeholder="DD/MM/YYYY" type="text" readonly
					class="form-control segment-birthdate" name="sign_birthdate[]" >
				  </div>
				  
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Phone Number<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="sign_phone[]" class="form-control segment-phone">
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Occupation<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="sign_occupation"
                                              class="form-control segment-occupation">
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Authorized Signatory Identification<span>*</span> </label>
                      <div class="col-sm-6">
						  <!-- Upload block starts -->
					  <div class="uploader-container row">
												
											<div class="col-xs-8">
												<div class="info-container">

												<div id="progressOuter-{{$id}}-1" class="progress progress-striped active" style="display:none;">
            <div id="progressBar-{{$id}}-1" class="progress-bar progress-bar-success"  role="progressbar-{{$id}}-1" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
												</div>

												<div id="msgBox-{{$id}}-1" class="msg-box"></div>
												<input type="hidden" name="sign_file_id[]" id="uploaded-file-{{$id}}-1" >
</div>
											</div>

											<div class="col-xs-4">
												<button id="uploadBtn-{{$id}}-1" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
											</div>

											</div>
						<!-- Upload block ends -->
                      </div>
					</div>


					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Signatory Passport Photo<span>*</span> </label>
                      <div class="col-sm-6">
					  <!-- Upload block starts -->
					  <div class="uploader-container row">
												
											<div class="col-xs-8">
												<div class="info-container">

												<div id="progressOuter-{{$id}}-2" class="progress progress-striped active" style="display:none;">
            <div id="progressBar-{{$id}}-2" class="progress-bar progress-bar-success"  role="progressbar-{{$id}}-2" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
												</div>

												<div id="msgBox-{{$id}}-2" class="msg-box"></div>
												<input type="hidden" name="sign_file_photo[]"
												id="uploaded-file-{{$id}}-2" >
</div>
											</div>

											<div class="col-xs-4">
												<button id="uploadBtn-{{$id}}-2" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
											</div>

											</div>
						<!-- Upload block ends -->
                      </div>
					</div>


					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Signature Of Account Signatory<span>*</span> </label>
                      <div class="col-sm-6">
					  <!-- Upload block starts -->
					  <div class="uploader-container row">
												
											<div class="col-xs-8">
												<div class="info-container">

												<div id="progressOuter-{{$id}}-3" class="progress progress-striped active" style="display:none;">
            <div id="progressBar-{{$id}}-3" class="progress-bar progress-bar-success"  role="progressbar-{{$id}}-3" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
												</div>

												<div id="msgBox-{{$id}}-3" class="msg-box"></div>
												<input type="hidden" name="sign_file_signature[]" id="uploaded-file-{{$id}}-3" >
</div>
											</div>

											<div class="col-xs-4">
												<button id="uploadBtn-{{$id}}-3" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
											</div>

											</div>
						<!-- Upload block ends -->
                      </div>
					</div>




					<!-- Column 2 ends -->
					</div>

				</div>