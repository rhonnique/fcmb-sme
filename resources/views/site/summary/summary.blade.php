@extends('site.master2')
@section('title', $page_title)
@section('content')

{!! Form::open(['action' => 'Site\summary@review', 'id' => 'form_register']) !!}

<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row3 starts here-->


<!-- ***************** Block starts ********************* -->
  <div class="card card-theme card-format-form summary">
              <div class="card-header"><div class="col-xs-12">Company Details</div></div>
              <div class="card-block">

                <div class="row row-lg">
					<div class="col-md-6 col-lg-6">
						<!-- Column 1 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6">Category Of Business </label>
                      <div class="col-sm-6">{{$bus_category}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">
					  Date Of Incorporation/ Registration </label>
                      <div class="col-sm-6">{{$inc_date}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Certificate of Incorporation/ RC Number </label>
                      <div class="col-sm-6">{{$rc_number}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Tax Identification Number (TIN) </label>
                      <div class="col-sm-6">{{$tin_number}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Company/ Business Name </label>
                      <div class="col-sm-6">{{$bus_name}}</div>
					</div>
<!-- Column 1 ends -->
					</div>

					<div class="col-md-6 col-lg-6">
						<!-- Column 2 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6">Operating Address </label>
                      <div class="col-sm-6">{{$bus_address}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">State </label>
                      <div class="col-sm-6">{{$bus_state}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">City/Town </label>
                      <div class="col-sm-6">{{$bus_city}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Phone </label>
                      <div class="col-sm-6">{{$bus_phone}}</div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Email Address </label>
                      <div class="col-sm-6">{{$bus_email}}</div>
					</div>
					<!-- Column 2 ends -->
					</div>

				</div>

              </div>
</div>
<!-- ***************** Block ends ********************* -->





<!-- ***************** Block starts ********************* -->
<div class="card card-theme card-format-form summary">
              <div class="card-header">
              <div class="col-xs-12">Director Details</div></div>
              <div class="card-block director-list-wrapper">
@include('site.summary.director')
              </div>
</div>
<!-- ***************** Block ends ********************* -->





<!-- ***************** Block starts ********************* -->
<div class="card card-theme card-format-form summary">
              <div class="card-header">
              <div class="col-xs-12">Account signatory Details</div></div>
              <div class="card-block signatory-list-wrapper">

				<div class="col-xs-12 text-right margin-top-20 display-none add-signatory-box">
				<button type="button" class="btn btn-theme add-signatory">ADD A SIGNATORY +</button>
				</div>

              </div>
</div>
<!-- ***************** Block ends ********************* -->







<!-- ***************** Block starts ********************* -->
  <div class="card card-theme card-format-form summary">
              <div class="card-header"><div class="col-xs-12">ADDITIONAL Details</div></div>
              <div class="card-block">

                <div class="row row-lg">
					<div class="col-md-6 col-lg-6">
						<!-- Column 1 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6">Type Of Business </label>
                      <div class="col-sm-6">
					  {!! Form::select('bus_type', [
												'Limited Liabilty Company' => 'Limited Liabilty Company'
												], null, ['class' => 'form-control','placeholder' => '-- Select --'])
												 !!}
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Products And Services Description </label>
                      <div class="col-sm-6">
					  <textarea type="text" name="bus_description" rows="3"
					  class="form-control"></textarea>
                      </div>
					</div>
<!-- Column 1 ends -->
					</div>

					<div class="col-md-6 col-lg-6">
						<!-- Column 2 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6">
                      Printable Reference Form </label>
                      <div class="col-sm-6 form-control-box">
                      <a class="download-link"><i class="fa fa-download" aria-hidden="true"></i>Click Here to Download
                      </a>
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6">Certificate of Incorporation </label>
                      <div class="col-sm-6">
                      <!-- Upload block starts -->
                      					  <div class="uploader-container row">

                      											<div class="col-xs-8">
                      												<div class="info-container">

                      												<div id="progressOuter-cert-of-inc" class="progress progress-striped active" style="display:none;">
                      												<div id="progressBar-cert-of-inc" class="progress-bar progress-bar-success"
                      												role="progressbar-cert-of-inc" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                      												</div>

                      												<div id="msgBox-cert-of-inc" class="msg-box"></div>
                      												<input type="hidden" name="file_cert_of_inc[]"
                      												id="uploaded-file-cert-of-inc" >
                      </div>
                      											</div>

                      											<div class="col-xs-4">
                      												<button id="uploadBtn-cert-of-inc" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                      											</div>

                      											</div>
											  <!-- Upload block ends -->
											  
											  <a class="download-link color-theme font-size-12">OTHER <i class="fa fa-plus"
                      aria-hidden="true"></i>
                                            </a>
                      </div>
					</div>



<!-- ************ Upload others block starts *************** -->

<!-- ************ Upload others block starts *************** -->



					<!-- Column 2 ends -->
					</div>

				</div>


<div class="row row-lg margin-top-10">
<div class="col-md-6 col-lg-6">
<div class="form-group row">
<label class="col-sm-6 text-right color-theme-light">Select preffered branch</label>
</div></div>
</div>


<div class="row row-lg">

					<div class="col-md-6 col-lg-6">

					<div class="form-group row">

                                          <label class="col-sm-6">State</label>
                                          <div class="col-sm-6"></div>
                    					</div>
					</div>
					<div class="col-md-6 col-lg-6">
<div class="form-group row">
                      <label class="col-sm-6">Branch</label>
                      <div class="col-sm-6">
					  <select name="pref_branch" id="pref_branch" class="form-control">
						<option disabled selected>-- Select Branch -- </option>
					</select>
                      </div>
					</div>
                    					</div>
</div>

              </div>
</div>
<!-- ***************** Block ends ********************* -->









				</div><!--row6 ends here-->

						<div class="col-md-12 col-sm-12 col-xs-12 term-condition">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<p class="condition">Our Terms and Conditions</p>
							</div>
							<div class="col-md-10 col-sm-12 col-xs-12 tick">
								<p style="color: #6e6e6e; font-size: 18px; text-align: center;"><input type="checkbox" name=""> I have read the important information and accept that by completing the application I will be bound by the terms.</p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12" style="color: #53338e;"><p class="go-back"><span class="glyphicon glyphicon-chevron-left"></span>
							<a onclick="redir()" style="cursor:pointer; color: #53338e; font-weight: bold; "> GO BACK</a></p></div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<p>
								<button type="submit" class="btn btn-theme">SAVE AND CONTINUE LATER</button></p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<p><button class="upload-btn" style="font-weight: bold; padding-left: 5px; padding-right: 5px;">REVIEW AND FINISH</button></p>
							</div>






{!! Form::close() !!}
@endsection
@push('styles')

@endpush
@push('scripts')

@endpush
