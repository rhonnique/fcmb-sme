@extends('site/master')
@section('title', 'Home')
@section('content')


<div class=" col-md-12 col-sm-12 col-xs-12 welcome-content">
			<h2>WELCOME!</h2>
			<p>Open a Business Account from anywhere in the world and get your account number instantly.</p>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 " style="margin-bottom: 50px;">
			<div class="col-md-4 col-sm-6 col-xs-6">
				<a href=""><img src="{{asset('assets/1/images/computer.png')}}" class="img-responsive computer"></a> 
				<button type="button" class="btn btn-warning imge-btm"
                onclick="window.location='{{url('start')}}'">GET STARTED</button>
			</div>
			<div class="col-md-8 col-sm-6 col-xs-6">
				<ul class="open-acct">
					<h3>WHAT DO I NEED TO OPEN FCMB BUSINESS ACCOUNT?</h3>
					<li>CAC/Registration certificates</li>
					<li>BVN</li>
					<li>Identification Card</li>
					<li>Scanned Signature</li>
					<li>Passport Photograph</li>
					<li>Tax identification number</li>
					<li>Board resolution</li>
					<li>2 references (companies)</li>
					<li>scuml certificate (where applicable)</li>
				</ul>
			</div>
		</div>

<section class="body-content col-md-12 col-sm-12 col-xs-12">
		

		



		
			<div class="panel-group" id="accordion">
			  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
				          PRODUCT FEATURES AND BENEFITS
				        </a>
				      </h4>
				    </div>
				    <div id="collapseOne" class="panel-collapse collapse in">
				      <div class="panel-body">
				       <ul class="open-acct">
					       	<li>Free Banking for first 90days.</li>
					       	<li>Zero account maintenance fee.</li>
					       	<li>N5,000 minimum operating balance. </li>
					       	<li>Fixed charge as low as N6,350 on N40million debit turnover.</li>
				       </ul>
				      </div>
				    </div>
			  </div>
			  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
				          REQUIREMENT TO OPEN BUSINESS ACCOUNT
				        </a>
				      </h4>
				    </div>
				    <div id="collapseTwo" class="panel-collapse collapse">
				      <div class="panel-body">
				       <p>Limited Liability Companies</p>
				       <ul class="open-acct">
					       	<li>Scanned copy of certificate of incorporation or registration.</li>
					       	<li>Certified true copy of memorandum and article of association.</li>
					       	<li>Certified true copy of form of C.O.7 or C.A.C2-3 </li>
					       	<li>Board resolution.</li>
				       </ul>
				       <p>Non-Limited Companies - Enterprise, Ventures, Services etc</p>
				       	<ul class="open-acct">
					       	<li>Scanned copy of business registration certificate</li>
					       	<li>Instruction to the bank to open account.</li>
					    </ul>

					    <p>For Associations & Cooperatives (Registered or Non-registered)</p>
				       <ul class="open-acct">
					       	<li>Scanned copy of Certificate of Registration (if registered with state or CAC).</li>
					       	<li>Constitution, Rules and Regulations.</li>
					       	<li>Society / Club/ Association/Cooperative Resolution to open Account or Minute of the meeting where account opening with the bank was discussed. </li>
				       </ul>

				      </div>
				    </div>
			  </div>

			  <div class="panel panel-default">
				    <div class="panel-heading">
				      <h4 class="panel-title">
				        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
				          GENERAL REQUIREMENT
				        </a>
				      </h4>
				    </div>
				    <div id="collapseThree" class="panel-collapse collapse">
				      <div class="panel-body">
				       <ul class="open-acct">
					       	<li>Tax Identification Number (Business name or limited liability profit making company that operates and generates income in Nigeria).</li>
					       	<li>Biometric Verification Number.</li>
					       	<li>2 references (companies)</li>
					       	<li>SCUML (is required by type of business i.e Dealers in Jewelries, Chartered/Professional Accountants, Chartered/Professional Accountants, Audit Firms, Tax Consultant, Clearing And Settlement Companies, Legal Practitioners, Trust and Company Service Providers, Estate Surveyors and Valuers, Hotels and Hospitality industry, Dealers and Miners of  Precious Stones and Metals, Pool Betting, Casino and Lottery, Supermarkets, Non-Governmental Organization, Consultants and Consulting companies, Construction Companies, Estate Agents, Dealers in Real Estate, Importers and Dealers in cars and vehicles, Dealers in Mechanized farming equipment and machinery).</li>
				       </ul>
				      </div>
				    </div>
			  </div>
			  
			</div>   <!--end of accordion-->
	

		
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-6 col-sm-6 col-xs-6">
			<button type="button" class="btn btn-warning">GET STARTED</button>
		</div>

		<div class="col-md-6 col-sm-6 col-xs-6">
			<p class="support" ><a href="#"> Need Support</a> <span class="glyphicon glyphicon-question-sign"></span></p>
		</div>
	</div>
</section>

@endsection