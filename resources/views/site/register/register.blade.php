@extends('site/master2')
@section('title', 'Home')
@section('content')

@push('csrf_token')
      <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush


{!! Form::open(['action' => 'Site\summary@review', 'id' => 'form_register']) !!}

<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row3 starts here-->

<!-- ***************** Block starts ********************* -->
  <div class="card card-theme card-format-form">
              <div class="card-header"><div class="col-xs-12">Company Details</div></div>
              <div class="card-block">

                <div class="row row-lg">
					<div class="col-md-6 col-lg-6">
						<!-- Column 1 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Category Of Business<span>*</span> </label>
                      <div class="col-sm-6">
					  {!! Form::select('bus_category', [
						'Limited Liabilty Company' => 'Limited Liabilty Company',
						'Non-Limited Companies' => 'Non-Limited Companies',
						'Associations And Cooperatives' => 'Associations And Cooperatives'
												], null,
					 ['class' => 'form-control','placeholder' => '- Select -']) !!}
                      </div>
					</div>

					<div class="form-group row form-icons">
                      <label class="col-sm-6 col-form-label">
					  Date Of Incorporation/ Registration<span>*</span> </label>
                      <div class="col-sm-6">
					  <div class="input-group">
					<i class="form-control-icon form-control-icon-right glyphicon glyphicon-calendar"></i>
					<input class="form-control datepicker" placeholder="DD/MM/YYYY" type="text"
					 name="bus_inc_date" value="{{old('bus_inc_date')?:$user_data['bus_inc_date']}}" >
                  </div>
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Certificate of Incorporation/ RC Number<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="bus_rc_number" class="form-control"
					  value="{{old('bus_rc_number')?:$user_data['bus_rc_number']}}">
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Tax Identification Number (TIN)<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="bus_tin_number" class="form-control"
					  value="{{old('bus_tin_number')?:$user_data['bus_tin_number']}}">
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Company/ Business Name<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="bus_name" class="form-control"
					  value="{{old('bus_name')?:$user_data['bus_name']}}" />
                      </div>
					</div>
<!-- Column 1 ends -->
					</div>

					<div class="col-md-6 col-lg-6">
						<!-- Column 2 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Operating Address<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="bus_address" size="20" class="form-control"
					   value="{{old('bus_address')?:$user_data['bus_address']}}" />
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">State<span>*</span> </label>
                      <div class="col-sm-6">
					  {!! Form::select('bus_state', $states, null,
					   ['class' => 'form-control','placeholder' => '-- Select State --', 
					   'onchange' => 'get_cities(this.value)']) !!}
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">City/Town<span>*</span> </label>
                      <div class="col-sm-6">
					  <select name="bus_city" id="bus_city" class="form-control">
						<option disabled selected>-- Select City/Town -- </option>
					</select>
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Phone<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="bus_phone" class="form-control"
					   value="{{old('bus_phone')?:$user_data['bus_phone']}}" />
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Email Address<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="email" name="bus_email" class="form-control"
					   value="{{old('bus_email')?:$user_data['bus_email']}}" />
                      </div>
					</div>
					<!-- Column 2 ends -->
					</div>

				</div>

              </div>
</div>
<!-- ***************** Block ends ********************* -->





<!-- ***************** Block starts ********************* -->
<div class="card card-theme card-format-form">
              <div class="card-header">
              <div class="col-xs-12">Director Details</div></div>
              <div class="card-block director-list-wrapper">

              @if(isset($user_data['dir_bvn_number']))
                 @for($i=0; $i < sizeof($user_data['dir_bvn_number']); $i++)
                    @php
                    $dir_id = ($i+1);
                    $id = $i==0?1:substr(rand().rand(),0,10);

                    $bvn_number = $user_data['dir_bvn_number'][$i];
                    $title = $user_data['dir_title'][$i];
                    $name = $user_data['dir_name'][$i];
                    $gender = $user_data['dir_gender'][$i];
                    $nationality = $user_data['dir_nationality'][$i];
                    $country = $user_data['dir_residence_country'][$i];
                    $address = $user_data['dir_address'][$i];
                    $birthdate = $user_data['dir_birthdate'][$i];
                    $phone = $user_data['dir_phone'][$i];
                    $occupation = $user_data['dir_occupation'][$i];

                    @endphp
                     @include('site.register.director')
                 @endfor
              @endif
              <div class="col-xs-12 text-right margin-top-20">
                               				<button type="button" class="btn btn-theme add-director">ADD A DIRECTOR +</button>
                               </div>
              </div>
</div>
<!-- ***************** Block ends ********************* -->





<!-- ***************** Block starts ********************* -->
<div class="card card-theme card-format-form">
              <div class="card-header">
              <div class="col-xs-12">Account signatory Details</div></div>
              <div class="card-block signatory-list-wrapper">
              @if(isset($user_data['sign_bvn_number']))
                               @for($i=0; $i < sizeof($user_data['sign_bvn_number']); $i++)
                                  @php
                                  $sign_id = ($i+1);
                                  $id = $i==0?2:substr(rand().rand(),0,10);

                                  $sign_bvn_number = $user_data['sign_bvn_number'][$i];
                                  $sign_title = $user_data['sign_title'][$i];
                                  $sign_name = $user_data['sign_name'][$i];
                                  $sign_gender = $user_data['sign_gender'][$i];


                                  $sign_nationality = $user_data['sign_nationality'][$i];
                                  $sign_country = $user_data['sign_residence_country'][$i];
                                  $sign_address = $user_data['sign_address'][$i];
                                  $sign_birthdate = $user_data['sign_birthdate'][$i];
                                  $sign_phone = $user_data['sign_phone'][$i];
                                  $sign_occupation = $user_data['sign_occupation'][$i];



								  /**/

                                  @endphp
                                   @include('site.register.signatory')
                               @endfor
                            @endif
				<div class="col-xs-12 text-right margin-top-20 display-none add-signatory-box">
				<button type="button" class="btn btn-theme add-signatory">ADD A SIGNATORY +</button>
				</div>

              </div>
</div>
<!-- ***************** Block ends ********************* -->







<!-- ***************** Block starts ********************* -->
  <div class="card card-theme card-format-form">
              <div class="card-header"><div class="col-xs-12">ADDITIONAL Details</div></div>
              <div class="card-block">

                <div class="row row-lg">
					<div class="col-md-6 col-lg-6">
						<!-- Column 1 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Type Of Business<span>*</span> </label>
                      <div class="col-sm-6">
					  {!! Form::select('bus_type', [
												'Limited Liabilty Company' => 'Limited Liabilty Company'
												], null, ['class' => 'form-control','placeholder' => '-- Select --'])
												 !!}
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Products And Services Description<span>*</span> </label>
                      <div class="col-sm-6">
					  <textarea type="text" name="bus_description" rows="3"
					  class="form-control"></textarea>
                      </div>
					</div>
<!-- Column 1 ends -->
					</div>

					<div class="col-md-6 col-lg-6">
						<!-- Column 2 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">
                      Printable Reference Form<span>*</span> </label>
                      <div class="col-sm-6 form-control-box">
                      <a class="download-link"><i class="fa fa-download" aria-hidden="true"></i>Click Here to Download
                      </a>
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Certificate of Incorporation<span>*</span> </label>
                      <div class="col-sm-6">
                      <!-- Upload block starts -->
                      					  <div class="uploader-container row">

                      											<div class="col-xs-8">
                      												<div class="info-container">

                      												<div id="progressOuter-cert-of-inc" class="progress progress-striped active" style="display:none;">
                      												<div id="progressBar-cert-of-inc" class="progress-bar progress-bar-success"
                      												role="progressbar-cert-of-inc" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                      												</div>

                      												<div id="msgBox-cert-of-inc" class="msg-box"></div>
                      												<input type="hidden" name="file_cert_of_inc[]"
                      												id="uploaded-file-cert-of-inc" >
                      </div>
                      											</div>

                      											<div class="col-xs-4">
                      												<button id="uploadBtn-cert-of-inc" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                      											</div>

                      											</div>
											  <!-- Upload block ends -->
											  
											  <a class="download-link color-theme font-size-12">OTHER <i class="fa fa-plus"
                      aria-hidden="true"></i>
                                            </a>
                      </div>
					</div>



<!-- ************ Upload others block starts *************** -->
					@include('site.register.others')
<!-- ************ Upload others block starts *************** -->



					<!-- Column 2 ends -->
					</div>

				</div>


<div class="row row-lg margin-top-10">
<div class="col-md-6 col-lg-6">
<div class="form-group row">
<label class="col-sm-6 text-right color-theme-light">Select preffered branch</label>
</div></div>
</div>


<div class="row row-lg">

					<div class="col-md-6 col-lg-6">

					<div class="form-group row">

                                          <label class="col-sm-6 col-form-label">State</label>
                                          <div class="col-sm-6">
                    					  {{ Form::select('pref_state', $states, null, ['class' => 'form-control','placeholder' => '-- Select State --', 'onchange' => 'get_braches(this.value)']) }}
                                          </div>
                    					</div>
					</div>
					<div class="col-md-6 col-lg-6">
<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Branch</label>
                      <div class="col-sm-6">
					  <select name="pref_branch" id="pref_branch" class="form-control">
						<option disabled selected>-- Select Branch -- </option>
					</select>
                      </div>
					</div>
                    					</div>
</div>

              </div>
</div>
<!-- ***************** Block ends ********************* -->









				</div><!--row6 ends here-->

						<div class="col-md-12 col-sm-12 col-xs-12 term-condition">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<p class="condition">Our Terms and Conditions</p>
							</div>
							<div class="col-md-10 col-sm-12 col-xs-12 tick">
								<p style="color: #6e6e6e; font-size: 18px; text-align: center;"><input type="checkbox" name=""> I have read the important information and accept that by completing the application I will be bound by the terms.</p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12" style="color: #53338e;"><p class="go-back"><span class="glyphicon glyphicon-chevron-left"></span>
							<a onclick="redir()" style="cursor:pointer; color: #53338e; font-weight: bold; "> GO BACK</a></p></div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<p>
								<button type="submit" class="btn btn-theme">SAVE AND CONTINUE LATER</button></p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<p><button class="upload-btn" style="font-weight: bold; padding-left: 5px; padding-right: 5px;">REVIEW AND FINISH</button></p>
							</div>


{!! Form::close() !!}
@endsection
@push('styles')
<link href="{{asset('assets/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
<script type="text/javascript" src="{{asset('assets/bootstrap-datetimepicker/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assets/js/app.js?time='.time())}}"></script>
<script src="{{asset('assets/js/AjaxUploader.min.js')}}"></script>

<script type="text/javascript">
var bus_category = "{{old('bus_category')?:$user_data['bus_category']}}";
var bus_state = "{{old('bus_state')?:$user_data['bus_state']}}";
var bus_city = "{{old('bus_city')?:$user_data['bus_city']}}";
var dir_bvn_number = "{{isset($user_data['dir_bvn_number'][0])?$user_data['dir_bvn_number'][0]:''}}";
var sign_bvn_number = "{{isset($user_data['sign_bvn_number'][0])?$user_data['sign_bvn_number'][0]:''}}";
</script>

<script src="{{asset('assets/js/register.js?time='.time())}}"></script>
@endpush
