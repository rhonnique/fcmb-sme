

<div class="col-md-12 col-sm-12 col-xs-12 director-list director-list-{{$id==1?1:$id}}">
									<p class="director-row">DIRECTOR <span></span></p>
								<div class="col-md-6 col-sm-12 col-xs-12 form-row1">

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Category Of Business <span class="star">*</span></label></p>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="dir_bvn_number[]" placeholder="BVN098877IKJ"
                                            value="21753968543"
                                            class="form-control dir-bvn-number"></p>
											<span style="font-size: 12px; color:#838383;">Forgotten Your BVN? Dial *565*0# to Retrieve it</span>

										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12">
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><button class="valid-btn" type="button" onclick="validate_bvn({{$id}})">Validate</button></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Title<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>{!! Form::select('dir_title[]', ["ALHAJI","ALHAJA","ARCHITECT","BARRISTER","CHIEF","DEACON","DOCTOR\/DR.","ELDER","ENGINEER","EVANGELIST","EXCELLENCY","HIGHCHIEF","HONOURABLE\/HONBL","HISROYALHIGHNESS","JUSTICE","MR&MRS","M\/S","MASTER","MASTER","MAZI","MESSERS","MISS","MISTER","MISTER","MR&MRS","MR","MRS.","MISS\/MS","ALHAJI","OBA","OTUNBA","OTUNBA","PASTOR","PASTOR","PROFESSOR","REVEREND","SIR"], null, ['class' => 'form-control dir-title"','placeholder' => '- Select Title -']) !!}
                                            
                                            </p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Name<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="dir_name[]" size="20" readonly class="form-control dir-name"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Gender<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>  <input type="radio" disabled name="dir_gender[]" class="dir-gender" value="Male" > Male
  												<input type="radio" disabled name="dir_gender[]" class="dir-gender" value="Female"> Female</p>
										</div>
									</div>

								</div>

								<div class="col-md-6 col-sm-12 col-xs-12 form-row2">


									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Nationality<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" class="form-control dir-nationality" name="dir_nationality[]" readonly>
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Country of Residence<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>{!! Form::select('dir_residence_country[]', array(
                                            'Top Countries' => $countries_top,
  'Other Countries' => $countries,
), null, ['class' => 'form-control dir-residence-country','placeholder' => '-- Select --']) !!}
</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Address<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="dir_address[]" size="20"
                                            class="form-control dir-address"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Date Of Birth<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12" style="padding-bottom: 10px;">
											<span id="datepicker2" class="input-group date" data-date-format="mm-dd-yyyy">
											    <input class="form-control dir-birthdate" placeholder="dd/mm/yyyy" type="text" readonly
                                                name="dir_birthdate[]" />
											    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</span>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Phone Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="dir_phone[]" size="20" class="form-control dir-phone"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Occupation<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="dir_occupation" size="20"
                                              class="form-control dir-occupation"></p>
										</div>
									</div>

@if($id != 1)
                                    <div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12">
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center; padding-top: 10px;">
											<p><button class="remove-block valid-btn" onclick="remove_director({{$id}})">- Remove</button></p>
										</div>
									</div>
                                    @endif




								</div>
							</div>
