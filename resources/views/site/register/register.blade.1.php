@extends('site/master2')
@section('title', 'Home')
@section('content')




<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row3 starts here-->






							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="detail-row">company details</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="col-md-12 col-sm-12 col-xs-12 form-div">
								<div class="col-md-6 col-sm-12 col-xs-12 form-row1">
									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Category Of Business <span class="star">*</span></label></p>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
											{!! Form::select('bus_category', [
												'Limited Liabilty Company' => 'Limited Liabilty Company'
												], null, ['class' => 'form-control','placeholder' => '- Select -']) !!}
										</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Date Of Incorporation/ Registration<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12" style="padding-bottom: 10px;">
											<span id="datepicker1" class="input-group date" data-date-format="mm-dd-yyyy">
											    <input class="form-control" placeholder="dd/mm/yyyy" type="text" readonly class="form-control" name="bus_inc_date" />
											    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</span>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right no-pad">
											<p><label>Certificate of Incorporation/ RC Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="bus_rc_number" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Tax Identification Number (TIN)<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="bus_tin_number" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Company/ Business Name<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="bus_name" size="20" class="form-control"></p>
										</div>
									</div>									
									
								</div>

								<div class="col-md-6 col-sm-12 col-xs-12 form-row2">
									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Operating Address<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="bus_address" size="20" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>State<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
											{{ Form::select('bus_state', $states, null, ['class' => 'form-control','placeholder' => '- Select State -', 'onchange' => 'get_cities(this.value)']) }}
											</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>City/Town<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
											<select name="bus_city" id="bus_city" class="form-control">
												<option disabled selected>- Select City - </option>
											</select>
											</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Phone Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="bus_phone" size="20" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Email<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="bus_email" size="20" class="form-control"></p>
										</div>
									</div>
									
									
								</div>
							</div>
							</div>
						</div><!--row3 ends here-->



						<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row4 starts here-->
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="detail-row">Director Details</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="col-md-12 col-sm-12 col-xs-12 form-div director-list-wrapper">






							<div class="col-md-12 col-sm-12 col-xs-12 no-pad" style="background-color: #f1f1f1;">										
									
										<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: right; padding-right: 48px !important;">
											<p><button class="valid-btn cursor-pointer add-director">ADD A DIRECTOR + </button></p>
										</div>
										
									</div>


							</div>


						</div>
						</div><!--row4 ends here-->


						<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row5 starts here-->
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="detail-row">Account signatory Details</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="col-md-12 col-sm-12 col-xs-12 form-div">
								<div class="col-md-12 col-sm-12 col-xs-12 signatory-list">
									
								<div class="col-md-6 col-sm-12 col-xs-12 form-row1">

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Signatory Type<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" class="form-control">
												<option value="Limited Liabilty Company">Mr</option>
												<option value="Limited Liabilty Company">Mrs</option>
												<option value="Limited Liabilty Company">Miss</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>BVN<span class="star">*</span></label></p>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" placeholder="BVN098877IKJ" class="form-control"></p>
											<span style="font-size: 12px; color:#838383;">Forgotten Your BVN? Dial *565*0# to Retrieve it</span>

										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12">
										</div>
									
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><button class="valid-btn">Validate</button></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Title<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" class="form-control">
												<option value="Limited Liabilty Company">Mr</option>
												<option value="Limited Liabilty Company">Mrs</option>
												<option value="Limited Liabilty Company">Miss</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Name<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Gender<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>  <input type="radio" name="gender" value="male" > Male
  												<input type="radio" name="gender" value="female" checked > Female</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Nationality<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" class="form-control">
												<option value="Limited Liabilty Company">Nigerian</option>
												<option value="Limited Liabilty Company">Abuja</option>
												<option value="Limited Liabilty Company">Ibadan</option>
										
											</select></p>
										</div>
									</div>									
									
								</div>

								<div class="col-md-6 col-sm-12 col-xs-12 form-row2">				

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Country of Residence<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" class="form-control">
												<option value="Limited Liabilty Company">Nigeria</option>
												<option value="Limited Liabilty Company">Abuja</option>
												<option value="Limited Liabilty Company">Ibadan</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Address<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Date Of Birth<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12" style="padding-bottom: 10px;">
											<span id="datepicker3" class="input-group date" data-date-format="mm-dd-yyyy">
											    <input class="form-control" placeholder="dd/mm/yyyy" type="text" readonly class="form-control" />
											    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</span>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Phone Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Occupation<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right no-pad">
											<p><label>Authorized Signatory Identification<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 btn-pad">
											<p><input type="text" name="" size="20"><button class="upload-btn">UPLOAD</button></p>
											
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Signatory Passport Photo<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 btn-pad">
											<p><input type="text" name="" size="20"><button class="upload-btn">UPLOAD</button></p>
											
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right no-pad">
											<p><label>Signature Of Account Signatory<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 btn-pad">
											<p><input type="text" name="" size="20"><button class="upload-btn">UPLOAD</button></p>
											
										</div>
									</div>				
									
								</div>

								
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12 no-pad" style="background-color: #f1f1f1;">										
									
										<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: right; padding-right: 48px !important;">
											<p><button class="valid-btn cursor-pointer add-size2">ADD A DIRECTOR + </button></p>
										</div>
										
									</div>
							</div>
						</div>
					</div><!--row5 ends here-->

						<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row6 starts here-->
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="detail-row">additional Details</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="col-md-12 col-sm-12 col-xs-12 form-div">
								<div class="col-md-6 col-sm-12 col-xs-12 form-row1">
									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Type of Bussiness<span class="star">*</span></label></p>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20" class="form-control"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Products and Services Description<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12" style="padding-bottom: 10px;">
											<p><textarea name="" class="form-control" rows="10"></textarea></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p style="color: #a94eae;">Select preffered branch</p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											
										</div>
										
									</div>	

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>State<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" class="form-control">
												<option value="Limited Liabilty Company">Lagos</option>
												<option value="Limited Liabilty Company">Abuja</option>
												<option value="Limited Liabilty Company">Ibadan</option>
										
											</select></p>
										</div>
									</div>								
									
								</div>

								<div class="col-md-6 col-sm-12 col-xs-12 form-row2">
									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Printable Reference Form<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><a href="#" style="color: #6e6e6e; font-weight: bold; border-bottom: 1px solid #6e6e6e;"><i class="fa fa-download" aria-hidden="true" style="font-weight: bolder; padding: 0px !important;"></i>Click Here to Download</a></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Certificate of Incorporation<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"><button class="upload-btn">UPLOAD</button></p>
											
										</div>
									</div>

									

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" >
											<p></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
												<div id="myNav" class="myNav-overlay">
							  <a href="registration.html" class="closetag" onclick="closeNav()"><span class="close-times">&times;</span></a>
							  <div class="col-md-12 col-sm-12 col-xs-12 overlay-content">
								<div class="col-md-6 col-sm-6 col-xs-12">
									
								</div>

								<div class="col-md-6 col-sm-6 col-xs-12 no-pad modal-form">
								  	<form class="col-md-12 col-sm-12 col-xs-12 no-pad" action="registration.html">
											
										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>Printable Reference form</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
											<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>Director Identification</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
											<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>Director Signature</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
											<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>Form CAC 2A</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
											<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>Form CAC 7A</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
											<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>Memorandum of Association</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
											<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>SCUML Certificate</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
											<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-5 col-sm-12 col-xs-12 no-pad text-right">
											<p><label>Board Resolution</label></p>
										</div>
										
										<div class="col-md-7 col-sm-12 col-xs-12" >	
										<p><input type="text" name="" ><button class="upload-btn">UPLOAD</button></p>
										</div>
										

										<div class="col-md-6 col-sm-6 col-xs-12" >
											
										</div>
										
										<div class="col-md-6 col-sm-6 col-xs-12 save-btn" style="padding-top:10px;">	
										<p><input type="text" name="" style="background-color:transparent; border:none!important;" ><button class="upload-btn" style="padding:10px 25px;">SAVE</button></p>
										</div>
											
										
								    </form>	
							    </div>

							  </div>
							</div>

											<p><a onclick="openNav()" id="navo" href="#" style="color: #53338e; font-weight: bold; border-bottom: 1px solid #53338e;">OTHERS <span class="glyphicon glyphicon-plus"></span></a>
											</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-12 col-sm-12 col-xs-12" >
											<p></p>
										</div>
										
									</div>

									<div class="row no-pad">
										<div class="col-md-12 col-sm-12 col-xs-12" >
											<p></p>
										</div>
										
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12 text-right">
											<p><label>Branch<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" class="form-control">
												<option value="Limited Liabilty Company">ketu</option>
												<option value="Limited Liabilty Company">ikeja</option>
												<option value="Limited Liabilty Company">ikoyi</option>
										
											</select></p>
										</div>
									</div>

									
									
									
								</div>
							</div>
							</div>
						</div>

				</div><!--row6 ends here-->

						<div class="col-md-12 col-sm-12 col-xs-12 term-condition">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<p class="condition">Our Terms and Conditions</p>
							</div>
							<div class="col-md-10 col-sm-12 col-xs-12 tick">
								<p style="color: #6e6e6e; font-size: 18px; text-align: center;"><input type="checkbox" name=""> I have read the important information and accept that by completing the application I will be bound by the terms.</p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12" style="color: #53338e;"><p class="go-back"><span class="glyphicon glyphicon-chevron-left"></span><a href="#" style="color: #53338e; font-weight: bold; "> GO BACK</a></p></div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<p><button class="upload-btn" style="font-weight: bold; padding-left: 5px; padding-right: 5px;">SAVE AND CONTINUE LATER</button></p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<p><button class="upload-btn" style="font-weight: bold; padding-left: 5px; padding-right: 5px;">REVIEW AND FINISH</button></p>
							</div>




@endsection

@push('scripts')
<script src="{{asset('assets/js/AjaxUploader.min.js')}}"></script>
<script src="{{asset('assets/js/register.js?time='.time())}}"></script>
<script>
	
</script>
@endpush