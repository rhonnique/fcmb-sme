<div class="form-group row">
                                          <label class="col-sm-6 col-form-label">Printable Reference form</label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div
                                          												id="progressOuter-ref-form" class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-ref-form" class="progress-bar progress-bar-success"
                                          												role="progressbar-ref-form" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-ref-form" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_ref_form[]"
                                          												id="uploaded-file-ref-form" />
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-ref-form" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>





<div class="form-group row">
       <label class="col-sm-6 col-form-label">Director Identification </label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div id="progressOuter-dir-id"
                                          												class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-dir-id" class="progress-bar progress-bar-success"
                                          												role="progressbar-dir-id" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-dir-id" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_dir_id[]"
                                          												id="uploaded-file-dir-id" />
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-dir-id" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>




<div class="form-group row">
     <label class="col-sm-6 col-form-label">Director Signature </label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div
                                          												id="progressOuter-dir-signature" class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-dir-signature" class="progress-bar progress-bar-success"
                                          												role="progressbar-dir-signature"
                                          												aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-dir-signature" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_dir_signature[]"
                                          												id="uploaded-file-dir-signature" >
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-dir-signature" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>



<div class="form-group row">
                                          <label class="col-sm-6 col-form-label">Form CAC 2A </label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div
                                          												id="progressOuter-form-cac-2a" class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-form-cac-2a" class="progress-bar progress-bar-success"
                                          												role="progressbar-form-cac-2a" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-form-cac-2a" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_form_cac_2a[]"
                                          												id="uploaded-file-form-cac-2a" >
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-form-cac-2a" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>



<div class="form-group row">
                                          <label class="col-sm-6 col-form-label">Form CAC 7A </label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div
                                          												id="progressOuter-form-cac-7a" class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-form-cac-7a" class="progress-bar progress-bar-success"
                                          												role="progressbar-form-cac-7a" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-form-cac-7a" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_form_cac_7a[]"
                                          												id="uploaded-file-form-cac-7a" >
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-form-cac-7a" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>





<div class="form-group row">
                                          <label class="col-sm-6 col-form-label">Memorandum of Association </label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div
                                          												id="progressOuter-mem-of-ass"
                                          												class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-mem-of-ass" class="progress-bar progress-bar-success"
                                          												role="progressbar-mem-of-ass" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-mem-of-ass" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_mem_of_ass[]"
                                          												id="uploaded-file-mem-of-ass" >
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-mem-of-ass" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>




<div class="form-group row">
                                          <label class="col-sm-6 col-form-label">SCUML Certificate </label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div
                                          												id="progressOuter-scuml-cert"
                                          												class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-scuml-cert" class="progress-bar progress-bar-success"
                                          												role="progressbar-scuml-cert" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-scuml-cert" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_scuml_cert[]"
                                          												id="uploaded-file-scuml-cert" >
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-scuml-cert" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>



<div class="form-group row">
                                          <label class="col-sm-6 col-form-label">Board Resolution </label>
                                          <div class="col-sm-6">
                                          <!-- Upload block starts -->
                                          					  <div class="uploader-container row">

                                          											<div class="col-xs-8">
                                          												<div class="info-container">

                                          												<div
                                          												id="progressOuter-board-res"
                                          												class="progress progress-striped active" style="display:none;">
                                          												<div id="progressBar-board-res" class="progress-bar progress-bar-success"
                                          												role="progressbar-board-res" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                          												</div>

                                          												<div id="msgBox-board-res" class="msg-box"></div>
                                          												<input type="hidden"
                                          												name="file_board_res[]"
                                          												id="uploaded-file-board-res" >
                                          </div>
                                          											</div>

                                          											<div class="col-xs-4">
                                          												<button id="uploadBtn-board-res" class="btn btn-sm btn-theme btn-block" type="button">UPLOAD</button>
                                          											</div>

                                          											</div>
                    											  <!-- Upload block ends -->
                                          </div>
</div>



