
<div class="clearfix segment-list segment-list-{{$id}}">
 @if($id != 1)<hr class="divider" />@endif
<div class="col-xs-12">
                <h4 class="segment-title">DIRECTOR <span>{{isset($dir_id)?$dir_id:''}}</span>
                @if($id != 1)
                <button type="button" class="btn btn-xs btn-danger"
                onclick="remove_director({{$id}})">- REMOVE</button>
                @endif
                </h4>
                </div>
					<div class="col-md-6 col-lg-6">
						<!-- Column 1 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">BVN<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="dir_bvn_number[]" class="form-control segment-bvn-number"
					  value="{{isset($bvn_number)?$bvn_number:''}}">

					  <section class="text-center">
						  <p class="font-size-12 margin-top-10">Forgotten Your BVN? Dial *565*0# to Retrieve it</p>
						  <button type="button" class="btn btn-theme btn-validate-bvn" onclick="validate_bvn({{$id}}, 1)
						  ">VALIDATE BVN</button>
					  </section>
                      </div>
					</div>

					<div class="form-group row form-icons">
                      <label class="col-sm-6 col-form-label">
					  Title<span>*</span> </label>
                      <div class="col-sm-6">
                      @php $title = isset($title)?$title:null @endphp
                      {!! Form::select('dir_title[]', $titles, $title, ['class' => 'form-control segment-title"',
                      'placeholder' => '-- Select Title --']) !!}
					  </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Name<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="dir_name[]" readonly class="form-control segment-name"
					   value="{{isset($name)?$name:''}}" />
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Gender<span>*</span> </label>
                      <div class="col-sm-6 form-control-box">
						  <label class="radio-inline" for="dir_gender_male_{{$id}}">
					  <input type="radio" disabled name="dir_gender_[{{$id}}]" class="segment-gender" 
					  value="Male" id="dir_gender_male_{{$id}}" {{isset($gender)&&$gender=='Male'?'checked':''}}> Male</label>

					  <label class="radio-inline" for="dir_gender_female_{{$id}}">
					  <input type="radio" disabled name="dir_gender_[{{$id}}]" class="segment-gender" 
					  value="Female" id="dir_gender_female_{{$id}}"
					  {{isset($gender)&&$gender=='Female'?'checked':''}}>Female</label>
					  <input type="hidden" name="dir_gender[]" class="segment-gender-selected"
					   value="{{isset($gender)?$gender:''}}" />
                      </div>
					</div>
<!-- Column 1 ends -->
					</div>

					<div class="col-md-6 col-lg-6">
						<!-- Column 2 Starts -->
					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Nationality<span>*</span> </label>
                      <div class="col-sm-6">
					  <select class="form-control segment-nationality" name="dir_nationality[]" readonly>
					  <?php
					  echo isset($nationality)?'<option value="'.$nationality.'">'.$nationality.'</option>':'';
					  ?>
					  </select>
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Country of Residence<span>*</span> </label>
                      <div class="col-sm-6">
                      @php $country = isset($country)?$country:null @endphp
					  {!! Form::select('dir_residence_country[]', array(
                                            'Top Countries' => $countries_top,
  'Other Countries' => $countries,
), $country, ['class' => 'form-control segment-residence-country','placeholder' => '-- Select --']) !!}
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Address<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="dir_address[]" value="{{isset($address)?$address:''}}"
                                            class="form-control segment-address" />
                      </div>
					</div>

					<div class="form-group row form-icons">
                      <label class="col-sm-6 col-form-label">Date Of Birth<span>*</span> </label>
                      <div class="col-sm-6">

					  <div class="input-group">
					<i class="form-control-icon form-control-icon-right glyphicon glyphicon-calendar"></i>
					<input placeholder="DD/MM/YYYY" type="text" readonly
					class="form-control segment-birthdate" name="dir_birthdate[]"
					 value="{{isset($birthdate)?$birthdate:''}}" />
				  </div>
				  
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Phone Number<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="dir_phone[]" class="form-control segment-phone"
					   value="{{isset($phone)?$phone:''}}" />
                      </div>
					</div>

					<div class="form-group row">
                      <label class="col-sm-6 col-form-label">Occupation<span>*</span> </label>
                      <div class="col-sm-6">
					  <input type="text" name="dir_occupation[]" value="{{isset($occupation)?$occupation:''}}"
                                              class="form-control segment-occupation" />
                      </div>
					</div>

					<!-- Column 2 ends -->
					</div>

				</div>

