<!doctype html>
<html>
<head>
@stack('csrf_token')
	<title>@yield('title') - FCMB SME</title>
	
	<link rel="stylesheet" type="text/css" href="{{asset('assets/2/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/2/css/cards.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{asset('assets/2/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/2/css/bootstrap-extend.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/2/fonts/linear/lnr-style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/2/css/owl.theme.default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/2/css/custom.css')}}">

    <link rel="stylesheet" href="{{asset('assets/2/css/style.css?time()='.time())}}">

    <script src="{{asset('assets/2/js/jquery.js')}}"></script>

	@stack('styles')
	
	<style type="text/css">

	/*

.fa {
  padding: 10px;
  font-size: 18px;
  width: 15px;
  height:15px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
  border-radius: 50%;
}

.fa:hover {
    opacity: 0.7;
}

.fa-facebook {
  background: #592b81;
  color: white;
}

.fa-twitter {
  background: #592b81;
  color: white;
}

.fa-linkedin {
  background: #592b81;
  color: white;
}

.fa-youtube {
  background: #592b81;
  color: white;
}

.fa-instagram {
  background: #592b81;
  color: white;
}
.fa-comment {
  background: #592b81;
  color: white;
}

#datepicker{width:190px; margin: 0 0px 10px 0px;}
#datepicker > span:hover{cursor: pointer;}

*/
	
</style>
</head>
<body>
			<div class="wrapper">
				<div class="col-md-12 col-sm-12 col-xs-12 fcmb-top-banner no-pad"><!--row1 starts here-->
					
				</div><!--row1 ends here-->

				<div class="col-md-12 col-sm-12 col-xs-12 top-banner2"><!--row2 starts here-->
					<div class="col-md-6 col-sm-6 col-xs-12">
						<img src="{{asset('assets/2/image/fcmb-logo.png')}}" class="img-responsive">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<p class="fcmb-online">fcmb Online</p>
						<p class="fcmb-acct">Account Opening - business</p>
					</div>
				</div><!--row2 ends here-->
				<div class="col-md-12 col-sm-12 col-xs-12 body-content">

@yield('content')


</div>




				
<footer class="no-pad"><!--footer starts here-->
	<div class="col-md-12 col-sm-12 col-xs-12 fcmb-footer-banner no-pad"><!--footer starts here-->
	<p>My bank and I</p>
	</div><!--footer ends here-->

	<div class="col-md-12 col-sm-12 col-xs-12 fcmb-footer2-banner no-pad">
		<div class="col-md-5 col-sm-12 col-xs-12 social-media">
			<ul>
				<li><a href="#" class="fa fa-facebook"></a></li>
				<li><a href="#" class="fa fa-twitter"></a></li>
				<li><a href="#" class="fa fa-linkedin"></a></li>
				<li><a href="#" class="fa fa-youtube"></a></li>
				<li><a href="#" class="fa fa-instagram"></a></li>
				<li><a href="#" class="fa fa-comment"></a></li>
			</ul>
		</div>

		<div class="col-md-7 col-sm-12 col-xs-12 footer-text">
			<ul>
				<li><a href="#">Press Releases</li></a>
				<li><a href="#">share price</li></a>
				<li><a href="#">whistle blower</li></a>
				<li><a href="#">fraud prevention</li></a>
				<li><a href="#">aml</li></a>
				<li><a href="#">careers</li></a>
			</ul>
		</div>
	</div>

</footer>

</div>

<!-- javascript  -->

<script src="{{asset('assets/2/js/bootstrap.min.js')}}"></script>

@stack('scripts')
<script type="text/javascript">
var Site_Url = '{{url('/')}}';
</script>


</body>
</html>