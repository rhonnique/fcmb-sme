<!DOCTYPE html>
<html>
<head>
@stack('csrf_token')
	<title>SME Landing Page </title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="{{asset('assets/1/css/landingpage.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/1/css/bootstrap.min.css')}}">
	
	<link rel="stylesheet" type="text/css" href="{{asset('assets/1/css/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/1/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/1/css/owl.theme.default.min.css')}}">
    @stack('styles')
</head>
<body>
<div class="wrapper">
<div class="top-header col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<a href=""><img src="{{asset('assets/1/images/logo.png')}}" class="logo "></a>
			</div>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<ul class="pull-right ">
				<li>FCMB ONLINE</li>
				<li><span>ACCOUNT OPENING - BUSINESS</span></li>
				
			</ul>
		</div>
			
		</div>


@yield('content')


<div class="clearfix"></div>
<div class="footer">
	<h6>Copyright © 2018. First City Monument Bank Limited. | <a href="#">www.fcmb.com</a> </h6>
</div>
		
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
@stack('scripts')
<script>
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')

}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);


 $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
</script>
</body>
</html>