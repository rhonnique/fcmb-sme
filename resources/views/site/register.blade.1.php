@extends('site/master2')
@section('title', 'Home')
@section('content')

{{ Form::open(['action' => 'Site\summary@index']) }}



<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row3 starts here-->
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="detail-row">company details</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="col-md-12 col-sm-12 col-xs-12 form-div">
								<div class="col-md-6 col-sm-6 col-xs-12 form-row1">
									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Category Of Business <span class="star">*</span></label></p>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" col="20">
											<option value="Limited Liabilty Company">Limited Liabilty Company</option>
											<option value="Limited Liabilty Company">Limited Liabilty Company</option>
											<option value="Limited Liabilty Company">Limited Liabilty Company</option>
											
										</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Date Of Incorporation/ Registration<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<span id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
											    <input class="form-control" placeholder="dd/mm/yyyy" type="text" readonly />
											    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</span>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Certificate of Incorporation/ RC Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Tax Identification Number (TIN)<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Company/ Business Name<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>									
									
								</div>

								<div class="col-md-6 col-sm-6 col-xs-12 form-row2">
									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Operating Address<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>State<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
											{{ Form::select('bus_state', $states, null, ['class' => 'form-control','placeholder' => '- Select State -', 'onchange' => 'get_cities(this.value)']) }}
											</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>City/Town<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>
											<select name="bus_city" id="bus_city" class="form-control">
												<option disabled selected>- Select City - </option>
											</select>
										</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Phone Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Email<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>
									
									
								</div>
							</div>
							</div>
						</div><!--row3 ends here-->



						<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row4 starts here-->
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="detail-row">Director Details</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="col-md-12 col-sm-12 col-xs-12 form-div">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<p class="director-row">DIRECTOR 1</p>
								<div class="col-md-6 col-sm-6 col-xs-12 form-row1">

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Category Of Business <span class="star">*</span></label></p>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" placeholder="BVN098877IKJ"></p>
											<span style="font-size: 12px; color:#838383;">Forgotten Your BVN? Dial *565*0# to Retrieve it</span>

										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12">
										</div>
									
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center; padding-top: 10px;">
											<p><button class="valid-btn">Validate</button></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Title<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" style="width: 186px;">
												<option value="Limited Liabilty Company">Mr</option>
												<option value="Limited Liabilty Company">Mrs</option>
												<option value="Limited Liabilty Company">Miss</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Name<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Gender<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>  <input type="radio" name="gender" value="male"> Male
  												<input type="radio" name="gender" value="female" checked> Female</p>
										</div>
									</div>									
									
								</div>

								<div class="col-md-6 col-sm-6 col-xs-12 form-row2">
									

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Nationality<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" style="width: 186px;">
												<option value="Limited Liabilty Company">Nigerian</option>
												<option value="Limited Liabilty Company">Abuja</option>
												<option value="Limited Liabilty Company">Ibadan</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Country of Residence<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" style="width: 186px;">
												<option value="">- Select -</option>
												<?php echo $countries ?>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Address<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Date Of Birth<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<span id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
											    <input class="form-control" placeholder="dd/mm/yyyy" type="text" readonly />
											    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</span>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Phone Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Occupation<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12">
										</div>
									
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center; padding-top: 10px;">
											<p><button class="valid-btn">ADD A DIRECTOR + </button></p>
										</div>
									</div>
									
									
								</div>
							</div>
							</div>
						</div>
						</div><!--row4 ends here-->


						<div class="col-md-12 col-sm-12 col-xs-12 detail-container"><!--row5 starts here-->
							<div class="col-md-12 col-sm-12 col-xs-12">
								<p class="detail-row">Account signatory Details</p>
							</div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="col-md-12 col-sm-12 col-xs-12 form-div">
								<div class="col-md-12 col-sm-12 col-xs-12">
									
								<div class="col-md-6 col-sm-6 col-xs-12 form-row1">

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Signatory Type<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" style="width: 186px;">
												<option value="Limited Liabilty Company">Mr</option>
												<option value="Limited Liabilty Company">Mrs</option>
												<option value="Limited Liabilty Company">Miss</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>BVN<span class="star">*</span></label></p>
										</div>

										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" placeholder="BVN098877IKJ"></p>
											<span style="font-size: 12px; color:#838383;">Forgotten Your BVN? Dial *565*0# to Retrieve it</span>

										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12">
										</div>
									
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center; padding-top: 10px;">
											<p><button class="valid-btn">Validate</button></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Title<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" style="width: 186px;">
												<option value="Limited Liabilty Company">Mr</option>
												<option value="Limited Liabilty Company">Mrs</option>
												<option value="Limited Liabilty Company">Miss</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Name<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Gender<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p>  <input type="radio" name="gender" value="male"> Male
  												<input type="radio" name="gender" value="female" checked> Female</p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Nationality<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" style="width: 186px;">
												<option value="Limited Liabilty Company">Nigerian</option>
												<option value="Limited Liabilty Company">Abuja</option>
												<option value="Limited Liabilty Company">Ibadan</option>
										
											</select></p>
										</div>
									</div>									
									
								</div>

								<div class="col-md-6 col-sm-6 col-xs-12 form-row2">				

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Country of Residence<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><select name="" style="width: 186px;">
												<option value="Limited Liabilty Company">Nigeria</option>
												<option value="Limited Liabilty Company">Abuja</option>
												<option value="Limited Liabilty Company">Ibadan</option>
										
											</select></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Address<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Date Of Birth<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<span id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy">
											    <input class="form-control" placeholder="dd/mm/yyyy" type="text" readonly />
											    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
											</span>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Phone Number<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Occupation<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"></p>
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Authorized Signatory Identification<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"><button class="upload-btn" type="file" name="fileupload" value="fileupload" id="fileupload">UPLOAD</button></p>
											
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Signatory Passport Photo<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"><button class="upload-btn">UPLOAD</button></p>
											
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: right;">
											<p><label>Signature Of Account Signatory<span class="star">*</span></label></p>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<p><input type="text" name="" size="20"><button class="upload-btn">UPLOAD</button></p>
											
										</div>
									</div>

									<div class="row no-pad">
										<div class="col-md-6 col-sm-6 col-xs-12">
										</div>
									
										<div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center; padding-top: 10px;">
											<p><button class="valid-btn">ADD A DIRECTOR + </button></p>
										</div>
									</div>
									
									
								</div>
							</div>
							</div>
						</div><!--row5 ends here-->



{{ Form::close() }}

@endsection

@push('scripts')
<script src="{{asset('assets/js/register.js')}}"></script>
@endpush