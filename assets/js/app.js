

$(document).ready(function(){
    /*
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        cache: false,
        statusCode: {
            401: function (xhr) {
                //if(window.console) console.log(xhr.responseText);
                //console.log(xhr, 'ressssppppp')
                logOut();
            },
            500: function (xhr) {
                //if(window.console) console.log(xhr.responseText);
                //console.log(xhr, 'ressssppppp')
                //logOut();
                return 'server eerorrrrr';
            },
            404: function (xhr) {
                //console.log(xhr, 'not found');
            }
        }
    });
    */
});

function set_error(element, text){
    element.closest('.form-group').find('.set-error').remove();
    element.after('<small class="set-error text-danger text-bold">'+text+'</small>');
    setTimeout(function(){
        element.closest('.form-group').find('.set-error').remove();
    }, 10000);
}

function error_code(code){
    var status = '0';
    switch(code){
        case 500:
        status = '005';
        break;
        case 404:
        status = '440';
        break;
        default:
        status = '000';
    }

    return status;
}