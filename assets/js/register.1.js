
window.onload = function () {

    //var director_list_id = Math.floor((Math.random() * 4195675339) + 97);
    load_director();

    $(document).on("click", ".add-director", function (e) {
        var director_list_id = Math.floor((Math.random() * 4195675339) + 97);
        load_director(director_list_id);
    });


    //setUploader(111);
    //setUploader(222);
    //setUploader(333);

    $('#datepicker1').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
};




function get_cities(id) {
    $.ajax({
        type: "GET",
        async: true,
        url: Site_Url + '/get-cities/' + id,
        success: function (data) {
            $("#bus_city").html(data)
        }
    });
}

function load_director(id) {
    var dir_id = id === undefined ? 0 : id;
    var btn_add = $(".add-director");
    $.ajax({
        type: "GET",
        async: true,
        url: Site_Url + '/load-director/' + dir_id,
        beforeSend: function(){
            btn_add.html('please wait...').attr('disabled', true);
        },
        success: function (data) {
            btn_add.html('ADD A DIRECTOR +').attr('disabled', false);
            var director_list = $(".director-list");
            if(director_list.length > 0){
                $(".director-list:last").after(data);
            } else {
                $(".director-list-wrapper").prepend(data);
            }
            $.each($(".director-title span"), function(index) {
                $(this).html(index + 1);
            });
        }
    });
}

function remove_director(id){
    var director = $(".director-list");
    var director_list = $(".director-list-"+id);

    if(director.length > 1){
        var conf = confirm('Are you sure you want to remove selected director?');
        if(!conf){
            return false;
        }

        director_list.remove();
        $.each($(".director-title span"), function(index) {
            $(this).html(index + 1);
        });
    }
}

function validate_bvn(id){
    var director = $(".director-list-"+id);
    var bvn_number = director.find('.dir-bvn-number');
    var name = director.find('.dir-name');
    var gender = director.find('.dir-gender');
    var nationality = director.find('.dir-nationality');
    var residence_country = director.find('.dir-residence-country');
    var address = director.find('.dir-address');
    var birthdate = director.find('.dir-birthdate');
    var phone = director.find('.dir-phone');
    var occupation = director.find('.dir-occupation');

    var data = {
        bvn_number : bvn_number.val()
    }

    $.ajax({
        url: Site_Url + '/get-bvn-details/' + bvn_number.val(),
        type: "GET",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            $("button").attr('disabled', true);
            director.find('.btn-validate-bvn').html('please wait...');
            director.find('input, select').attr('disabled', true);
        },
        success: function (data) {
            console.log(data.details.Gender);
            $("button").attr('disabled', false);
            director.find('.btn-validate-bvn').html('Validate Bvn');
            director.find('input, select').attr('disabled', false);
            if(data.response === false){
                return false;
            }

            var names = data.details.FirstName + ' ' + data.details.MiddleName + ' ' + data.details.LastName
            name.val(names);
            nationality.html('<option value="'+data.details.Nationality+'">'+data.details.Nationality+'</option>');
            birthdate.val(data.details.DateOfBirth);
            director.find('input[type="radio"].dir-gender[value='+data.details.Gender+']').attr('checked', true); 
            //$(".director-list-"+id+" input[name^='dir_gender'][value="+data.details.Gender+"]").attr('checked', true);
            //$("#dir_gender_female_"+id).attr('checked', true);
            //director.find('.dir-nationality');
            address.val(data.details.ResidentialAddress);
            phone.val(data.details.PhoneNumber);
        }
    });
}


function setUploader(id) {
    var btn = document.getElementById('uploadBtn-' + id),
        progressBar = document.getElementById('progressBar-' + id),
        progressOuter = document.getElementById('progressOuter-' + id),
        msgBox = document.getElementById('msgBox-' + id),
        uploadedFile = document.getElementById('sig-file-id-' + id);

    var uploader = new ss.SimpleUpload({
        button: btn,
        url: Site_Url + '/upload-file',
        name: 'uploadfile',
        multipart: true,
        hoverClass: 'hover',
        focusClass: 'focus',
        responseType: 'json',
        startXHR: function () {
            progressOuter.style.display = 'block'; // make progress bar visible
            this.setProgressBar(progressBar);
        },
        onSubmit: function () {
            msgBox.innerHTML = ''; // empty the message box
            msgBox.style.display = 'none';
            msgBox.classList.remove("text-danger");
            msgBox.classList.remove("text-success");

            btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
        },
        onComplete: function (filename, response) {
            btn.innerHTML = 'UPLOAD';
            progressOuter.style.display = 'none'; // hide progress bar when upload is completed

            msgBox.style.display = 'block';

            if (!response) {
                let set_text = 'Unable to upload file, please retry';
                msgBox.innerHTML = set_text;
                msgBox.classList.add("text-danger");
                msgBox.setAttribute("alt", set_text);
                return;
            }

            if (response.success === true) {
                //let set_text = '<strong>' + escapeTags(filename) + '</strong>' + ' successfully uploaded.';
                let set_text = 'File successfully uploaded';
                msgBox.innerHTML = set_text;
                msgBox.classList.add("text-success");
                msgBox.setAttribute("alt", set_text);
                uploadedFile.value = response.filename;
            } else {
                if (response.msg) {
                    let set_text = response.msg;
                    msgBox.innerHTML = escapeTags(set_text);
                    msgBox.classList.add("text-danger");
                    msgBox.setAttribute("alt", set_text);
                } else {
                    let set_text = 'An error occurred and the upload failed, please retry';
                    msgBox.innerHTML = set_text;
                    msgBox.classList.add("text-danger");
                    msgBox.setAttribute("alt", set_text);
                }
            }
        },
        onError: function (filename, errorType, status, statusText, response, uploadBtn, fileSize) {
            console.log(statusText, 'statusText');
            console.log(response, 'response');
            console.log(fileSize, 'fileSize');

            btn.innerHTML = 'UPLOAD';
            progressOuter.style.display = 'none';

            let set_text = 'Unable to upload file, please retry';
            msgBox.style.display = 'block';
            msgBox.innerHTML = set_text;
            msgBox.classList.add("text-danger");
            msgBox.setAttribute("alt", set_text);
        }
    });
}




function escapeTags(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}

