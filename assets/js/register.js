
window.onload = function () {
    if(!dir_bvn_number){
        load_director();
    }

    if(!dir_bvn_number){
        load_signatory();
    }

    $(document).on("click", ".add-director", function (e) {
        var id = Math.floor((Math.random() * 4195675339) + 97);
        load_director(id);
    });

    $(document).on("click", ".add-signatory", function (e) {
        var id = Math.floor((Math.random() * 4195675339) + 97);
        load_signatory(id);
    });

    setUploader('cert-of-inc');
    setUploader('ref-form');
    setUploader('dir-id');
    setUploader('dir-signature');
    setUploader('form-cac-2a');
    setUploader('form-cac-7a');
    setUploader('mem-of-ass');
    setUploader('scuml-cert');
    setUploader('board-res');

    $('.datepicker').datetimepicker({
        viewMode: 'days',
        format: 'DD/MM/YYYY'
    });

    $("[name='bus_category']").val(bus_category);
    $("[name='bus_state']").val(bus_state);
    if(bus_state){
        get_cities(bus_state, bus_city);
    }
};

$(document).ready(function(){
    
});


function get_cities(id, bus_city) {
    $.ajax({
        type: "GET",
        async: true,
        url: Site_Url + '/get-cities/' + id,
        success: function (data) {
            $("#bus_city").html(data);
            if(bus_city != null){
                $("[name='bus_city']").val(bus_city);
            }
        }
    });
}

function load_director(id) {
    var id = id === undefined ? 1 : id;
    var btn = $(".add-director");
    var director = $(".director-list-wrapper");
    if (director.find('.segment-list').length == 10) {
        return false;
    }

    $.ajax({
        type: "GET",
        async: true,
        url: Site_Url + '/load-director/' + id,
        beforeSend: function () {
            btn.html('please wait...').attr('disabled', true);
        },
        success: function (data) {
            btn.html('ADD A DIRECTOR +').attr('disabled', false);

            if (director.find('.segment-list').length > 0) {
                director.find('.segment-list:last').after(data);
            } else {
                $(".director-list-wrapper").prepend(data);
            }

            $.each(director.find('.segment-title span'), function (index) {
                $(this).html(index + 1);
            });
        }
    });
}

function remove_director(id) {
    var director = $(".director-list-wrapper");
    var director_list = $(".segment-list-" + id);

    if (director.find('.segment-list').length > 1) {
        var conf = confirm('Are you sure you want to remove selected director?');
        if (!conf) {
            return false;
        }

        director_list.remove();
        $.each(director.find('.segment-title span'), function (index) {
            $(this).html(index + 1);
        });
    }
}

function load_signatory(id) {
    var id = id === undefined ? 2 : id;
    var btn = $(".add-signatory");
    var signatory = $(".signatory-list-wrapper");
    if (signatory.find('.segment-list').length == 10) {
        return false;
    }

    $.ajax({
        type: "GET",
        async: true,
        url: Site_Url + '/load-signatory/' + id,
        beforeSend: function () {
            btn.html('please wait...').attr('disabled', true);
        },
        success: function (data) {
            btn.html('ADD A SIGNATORY +').attr('disabled', false);
            var signatory = $(".signatory-list-wrapper");

            if (signatory.find('.segment-list').length > 0) {
                signatory.find('.segment-list:last').after(data);
            } else {
                $(".signatory-list-wrapper").prepend(data);
            }
            $.each(signatory.find('.segment-title span'), function (index) {
                $(this).html(index + 1);
            });

            setUploader(id + '-1');
            setUploader(id + '-2');
            setUploader(id + '-3');
        }
    });
}

function remove_signatory(id) {
    var signatory = $(".signatory-list-wrapper");
    var signatory_list = $(".segment-list-" + id);

    if (signatory.find('.segment-list').length > 1) {
        var conf = confirm('Are you sure you want to remove selected signatory?');
        if (!conf) {
            return false;
        }

        signatory_list.remove();
        $.each(signatory.find('.segment-title span'), function (index) {
            $(this).html(index + 1);
        });

        if (signatory.find('.segment-list').length == 1) {
            signatory.find('.segment-signatory-type').val('Single');
        }
    }
}

function toggle_signatory_type(type) {
    var btn = $(".add-signatory-box");
    var segment = $(".signatory-list-wrapper .segment-list");
    if (type === 'Multiple') {
        btn.show();
    } else {
        btn.hide();
        if (segment.length > 1) {
            segment.not(":first").remove();
        }
    }
}

function validate_bvn(id, type) {
    var wrapper = type == 1 ? $(".director-list-wrapper") :
        $(".signatory-list-wrapper");

    var segment = $(".segment-list-" + id);
    var bvn_number = segment.find('.segment-bvn-number');
    var name = segment.find('.segment-name');
    var gender = segment.find('.segment-gender');
    var gender_selected = segment.find('.segment-gender-selected');
    var nationality = segment.find('.segment-nationality');
    var residence_country = segment.find('.segment-residence-country');
    var address = segment.find('.segment-address');
    var birthdate = segment.find('.segment-birthdate');
    var phone = segment.find('.segment-phone');
    var occupation = segment.find('.segment-occupation');

    // build bvn number array
    var bvns = new Array();
    $.each(wrapper.find('.segment-bvn-number'), function (index, value) {
        bvns[index] = $(this).val();
    });

    // remove current bvn number from array
    bvns.splice(jQuery.inArray(bvn_number.val(), bvns), 1);

    // if bvn number already added
    if (jQuery.inArray(bvn_number.val(), bvns) > -1) {
        set_error(bvn_number, 'Bvn number already added!');
        bvn_number.focus();
        return false;
    }

    var data = {
        bvn_number: bvn_number.val()
    }

    $.ajax({
        url: Site_Url + '/get-bvn-details/' + bvn_number.val(),
        type: "GET",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            $("button").attr('disabled', true);
            segment.find('.btn-validate-bvn').html('please wait...');
            segment.find('input, select').attr('disabled', true);

            name.val(''); nationality.val(''); occupation.val('');
            residence_country.val(''); address.val(''); birthdate.val(''); phone.val('');
            //segment.find('input[type="radio"].segment-gender').attr('checked',false);
        },
        success: function (data) {

            $("button").attr('disabled', false);
            segment.find('.btn-validate-bvn').html('Validate Bvn');
            segment.find('input, select').attr('disabled', false);
            if (data.response === false) {
                set_error(bvn_number, data.msg + '!');
                bvn_number.focus();
                return false;
            }

            var names = data.details.FirstName + ' ' + data.details.MiddleName + ' ' + data.details.LastName
            name.val(names);
            nationality.html('<option value="' + data.details.Nationality + '">' + data.details.Nationality + '</option>');
            birthdate.val(data.details.DateOfBirth);
            segment.find('input[type="radio"].segment-gender[value=' + data.details.Gender + ']').attr('checked', true);
            gender.attr('disabled', true);
            gender_selected.val(data.details.Gender);
            address.val(data.details.ResidentialAddress);
            phone.val(data.details.PhoneNumber);
        },
        error(err) {
            console.log(err.status);
            $("button").attr('disabled', false);
            segment.find('.btn-validate-bvn').html('Validate Bvn');
            segment.find('input, select').attr('disabled', false);

            set_error(bvn_number, 'Can not get bvn details at this time, please retry (ERR_' + error_code(err.status) + ')!');
            bvn_number.focus();
            return false;
        }
    })
}


function setUploader(id) {
    var btn = document.getElementById('uploadBtn-' + id),
        progressBar = document.getElementById('progressBar-' + id),
        progressOuter = document.getElementById('progressOuter-' + id),
        msgBox = document.getElementById('msgBox-' + id),
        uploadedFile = document.getElementById('uploaded-file-' + id);

    var uploader = new ss.SimpleUpload({
        button: btn,
        url: Site_Url + '/upload-file',
        name: 'uploadfile',
        multipart: true,
        hoverClass: 'hover',
        focusClass: 'focus',
        responseType: 'json',
        startXHR: function () {
            progressOuter.style.display = 'block'; // make progress bar visible
            this.setProgressBar(progressBar);
        },
        onSubmit: function () {
            msgBox.innerHTML = ''; // empty the message box
            msgBox.style.display = 'none';
            msgBox.classList.remove("text-danger");
            msgBox.classList.remove("text-success");

            btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
        },
        onComplete: function (filename, response) {
            btn.innerHTML = 'UPLOAD';
            progressOuter.style.display = 'none'; // hide progress bar when upload is completed

            msgBox.style.display = 'block';

            if (!response) {
                let set_text = 'Unable to upload file, please retry';
                msgBox.innerHTML = set_text;
                msgBox.classList.add("text-danger");
                msgBox.setAttribute("alt", set_text);
                return;
            }

            if (response.success === true) {
                //let set_text = '<strong>' + escapeTags(filename) + '</strong>' + ' successfully uploaded.';
                let set_text = 'File successfully uploaded';
                msgBox.innerHTML = set_text;
                msgBox.classList.add("text-success");
                msgBox.setAttribute("alt", set_text);
                uploadedFile.value = response.filename;
            } else {
                if (response.msg) {
                    let set_text = response.msg;
                    msgBox.innerHTML = escapeTags(set_text);
                    msgBox.classList.add("text-danger");
                    msgBox.setAttribute("alt", set_text);
                } else {
                    let set_text = 'An error occurred and the upload failed, please retry';
                    msgBox.innerHTML = set_text;
                    msgBox.classList.add("text-danger");
                    msgBox.setAttribute("alt", set_text);
                }
            }
        },
        onError: function (filename, errorType, status, statusText, response, uploadBtn, fileSize) {
            console.log(statusText, 'statusText');
            console.log(response, 'response');
            console.log(fileSize, 'fileSize');

            btn.innerHTML = 'UPLOAD';
            progressOuter.style.display = 'none';

            let set_text = 'Unable to upload file, please retry';
            msgBox.style.display = 'block';
            msgBox.innerHTML = set_text;
            msgBox.classList.add("text-danger");
            msgBox.setAttribute("alt", set_text);
        }
    });
}




function escapeTags(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}


function cache_data() {
    var form = $("#form_register");
    //var action = form.attr("action");

    $.ajax({
        url: Site_Url + '/cache-data',
        type: "POST",
        dataType: "json",
        data: form.serializeArray(),
        async: true,
        beforeSend: function () {
            //blockUI();
        },
        success: function (data) {
            console.log(data);
        }
    });
}


function redir() {
    cache_data();
    setTimeout(function () {
        window.location = Site_Url;
    }, 500);
}

function validate(){

}


