<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
	return view('welcome');
});

*/



Route::get( '/', 'Site\home@index' );
Route::resource( 'start', 'Site\register' );
Route::post( 'upload-file', 'Site\register@upload_file' );
Route::get( 'get-cities/{id}', 'Site\register@get_cities' );
Route::get( 'get-bvn-details/{id}', 'Site\register@get_bvn_details' );
Route::get( 'load-director/{id}', 'Site\register@load_director' );
Route::get( 'load-signatory/{id}', 'Site\register@load_signatory' );
Route::get( 'review', 'Site\summary@index' );
Route::post( 'review', 'Site\summary@review' );
Route::get( 'summary', 'Site\summary@index' );
Route::post( 'cache-data', 'Site\summary@cache' );



