<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Query extends Model {

	public function login( $username, $password ) {
		$query = collect( DB::select( "SELECT a.*, b.login_id, b.status
        FROM register a
        LEFT JOIN account b ON b.unique_id = a.uniques
        WHERE a.email = '" . $username . "' AND a.password = '" . $password . "'" ) )->first();

		if(!$query){
			return false;
		}
		return $query;
	}


	function account_details( $unique_id ) {
		$query = collect( DB::select( "SELECT a.*, b.login_id, b.status
        FROM register a
        LEFT JOIN account b ON b.unique_id = a.uniques
        WHERE a.uniques = '" . $unique_id . "'" ) )->first();

		if(!$query){
			return false;
		}
		return $query;
	}


	function current_event() {
		$event = DB::table( 'event' )->where( [ 'default_event' => 1 ] )->first();
		return $event;
	}


	function assigned_room( $unique_id, $event_id ) {
		$query = DB::table( 'room_assignment' )->where( [
			'user_id' => $unique_id,
			'event_id' => $event_id
		] )->first();
		if(!$query){
			return false;
		}
		return $query;
	}


	function reservation( $unique_id, $event_id ) {
		$query = DB::table( 'reservation' )->where( [
			'unique_id' => $unique_id,
			'event_id' => $event_id
		] )->first();
		if(!$query){
			return false;
		}
		return $query;
	}


	function paystack($ref, $unique_id){
		$query = DB::table( 'paystack' )->where( [
			'ref' => $ref,
			'user' => $unique_id,
			'done' => 1
		] )->first();
		if(!$query){
			return false;
		}
		return $query;
	}

	function update_event_register($unique_id, $event_id){
		$query = DB::table( 'register_event' )
		           ->where( [
			           'unique_id' => $unique_id,
			           'event_id' => $event_id
		           ] )
		           ->update( [
			           'stage' => 0
		           ] );
		if (!$query) {
			return FALSE;
		} else {
			return true;
		}
	}


	function already_booked($room_id, $event_id){
		$query = collect( DB::select( "SELECT a.* FROM room_assignment a
                 LEFT JOIN reservation b ON b.unique_id = a.user_id
                 WHERE SUBSTR(a.room_number, 1, 3) = '".$room_id."' AND a.status = 0
                 AND b.participant_class = 'PARTICIPANT'
                 AND a.event_id = ".$event_id ) );

		if (!$query) {
			return FALSE;
		} else {
			return $query;
		}
	}


}
