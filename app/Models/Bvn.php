<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bvn extends Model
{
    protected $table = 'bvn';
    protected $primaryKey = 'bvn_number';
    protected $guarded = array('id');
}
