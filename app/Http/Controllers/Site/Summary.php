<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\__account;
use DB;
use App\Libraries\__drops;

class Summary extends Controller
{
    public $data = [];
    private $cookie_exp;
	public $__drops;

	public function __construct()
	{
		$this->__drops = new __drops();
		$this->cookie_exp = (60 * 24);
		//$this->data['current_uri'] = Route::getFacadeRoot()->current()->uri();
		$this->data['page_title']         = "Review And Finish";
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('start');
    }



    /**
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
    public function review(Request $request)
    {
        $post = $request->all();

        $session_id = set_session(); 
        ## Company details
        $bus_category = isset($post['bus_category'])?$post['bus_category']:null;
        $inc_date = isset($post['bus_inc_date'])?$post['bus_inc_date']:null;
        $rc_number = isset($post['bus_rc_number'])?$post['bus_rc_number']:null;
        $tin_number = isset($post['bus_tin_number'])?$post['bus_tin_number']:null;
        $bus_name = isset($post['bus_name'])?ucwords($post['bus_name']):null;
        $bus_address = isset($post['bus_address'])?$post['bus_address']:null;
        $bus_state = isset($post['bus_state'])?$post['bus_state']:null;
        $bus_city = isset($post['bus_city'])?$post['bus_city']:null;
        $bus_phone = isset($post['bus_phone'])?$post['bus_phone']:null;
        $bus_email = isset($post['bus_email'])?$post['bus_email']:null;

        ## Directors details
	    $this->data['dir_bvn_number'] = isset($post['dir_bvn_number'])?$post['dir_bvn_number']:null;
	    $this->data['dir_title'] = isset($post['dir_title'])?$post['dir_title']:null;
	    $this->data['dir_name'] = isset($post['dir_name'])?$post['dir_name']:null;
	    $this->data['dir_gender'] = isset($post['dir_gender'])?$post['dir_gender']:null;
	    $this->data['dir_nationality'] = isset($post['dir_nationality'])?$post['dir_nationality']:null;
	    $this->data['dir_residence_country'] = isset($post['dir_residence_country'])?$post['dir_residence_country']:null;
	    $this->data['dir_address'] = isset($post['dir_address'])?$post['dir_address']:null;
	    $this->data['dir_birthdate'] = isset($post['dir_birthdate'])?$post['dir_birthdate']:null;
	    $this->data['dir_phone'] = isset($post['dir_phone'])?$post['dir_phone']:null;
	    $this->data['dir_occupation'] = isset($post['dir_occupation'])?$post['dir_occupation']:null;


        ## Signatory details
	    $this->data['sign_bvn_number'] = isset($post['sign_bvn_number'])?$post['sign_bvn_number']:null;
	    $this->data['sign_title'] = isset($post['sign_title'])?$post['sign_title']:null;
	    $this->data['sign_name'] = isset($post['sign_name'])?$post['sign_name']:null;
	    $this->data['sign_gender'] = isset($post['sign_gender'])?$post['sign_gender']:null;
	    $this->data['sign_nationality'] = isset($post['sign_nationality'])?$post['sign_nationality']:null;
	    $this->data['sign_residence_country'] = isset($post['sign_residence_country'])?$post['sign_residence_country']:null;
	    $this->data['sign_address'] = isset($post['sign_address'])?$post['sign_address']:null;
	    $this->data['sign_birthdate'] = isset($post['sign_birthdate'])?$post['sign_birthdate']:null;
	    $this->data['sign_phone'] = isset($post['sign_phone'])?$post['sign_phone']:null;
	    $this->data['sign_occupation'] = isset($post['sign_occupation'])?$post['sign_occupation']:null;
	    $this->data['sign_file_id'] = isset($post['sign_file_id'])?$post['sign_file_id']:null;
	    $this->data['sign_file_photo'] = isset($post['sign_file_photo'])?$post['sign_file_photo']:null;
	    $this->data['sign_file_signature'] = isset($post['sign_file_signature'])?$post['sign_file_signature']:null;

        ## Company details
        $bus_type = isset($post['bus_type'])?$post['bus_type']:null;
        $description = isset($post['description'])?$post['description']:null;
        $file_cert_of_inc = isset($post['file_cert_of_inc'])?$post['file_cert_of_inc']:null;
        $branch_state = isset($post['branch_state'])?$post['branch_state']:null;
        $branch_city = isset($post['branch_city'])?$post['branch_city']:null;

	    ## Other documents
	    $file_ref_form = isset($post['file_ref_form'])?$post['file_ref_form']:null;
	    $file_dir_id = isset($post['file_dir_id'])?$post['file_dir_id']:null;
	    $file_dir_signature = isset($post['file_dir_signature'])?$post['file_dir_signature']:null;
	    $file_form_cac_2a = isset($post['file_form_cac_2a'])?$post['file_form_cac_2a']:null;
	    $file_form_cac_7a = isset($post['file_form_cac_7a'])?$post['file_form_cac_7a']:null;
	    $file_mem_of_ass = isset($post['file_mem_of_ass'])?$post['file_mem_of_ass']:null;
	    $file_scuml_cert = isset($post['file_scuml_cert'])?$post['file_scuml_cert']:null;
	    $file_board_res = isset($post['file_board_res'])?$post['file_board_res']:null;



        \Cookie::queue( 'session_'.$session_id, $post, ( $this->cookie_exp ) );
	    //\Cookie::queue(\Cookie::forget( 'session_'.$session_id ));

        $this->data['bus_category'] = $bus_category;
        $this->data['inc_date'] = $inc_date;
        $this->data['rc_number'] = $rc_number;
        $this->data['tin_number'] = $tin_number;
        $this->data['bus_name'] = $bus_name;
        $this->data['bus_address'] = $bus_address;
        $this->data['bus_state'] = $this->__drops->state($bus_state)->name;
        $this->data['bus_city'] = $this->__drops->city($bus_city)->name;;
        $this->data['bus_phone'] = $bus_phone;
        $this->data['bus_email'] = $bus_email;

	    //print_r(\Cookie::get( 'session_'.$session_id)); exit;


        //return $session_id;
        return view("site.summary.summary", $this->data);
    }


    /**
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
    public function cache(Request $request)
    {
        $status = DB::table( 'test' )->insert(
		    [
			    'name'   => 'my name'
		    ]
	    );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
