<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Libraries\__account;
use App\Libraries\__drops;
use Illuminate\Http\Request;
use App\Models\Bvn;
use Session;

class Register extends Controller
{

    public $data = [];
    public $__drops;
    public $__account;
    private $states;
    private $countries;
    private $countries_top;
    private $titles;
    private $__session_id;

    public function __construct()
    {
        //$this->data['current_uri'] = Route::getFacadeRoot()->current()->uri();
        //$this->data['page_title']         = "Home";
        $this->__drops = new __drops();
        $this->__account = new __account();

        $this->states = $this->__drops->states();
        $this->countries = $this->__drops->countries();
        $this->countries_top = $this->__drops->countries_top();
        $this->titles = titles();
        $this->session_id = $this->__account->session();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

	    $session_id = set_session();
	    $this->data['states'] = $this->states;
        $this->data['countries'] = $this->countries;
        $this->data['countries_top'] = $this->countries_top;
        $this->data['titles'] = $this->titles;

	    $this->data['user_data'] = \Cookie::get( 'session_'.$session_id);

        return view("site.register.register", $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_cities($id)
    {
        return $this->__drops->cities($id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload_file(Request $request)
    {

        if (!$request->hasFile('uploadfile')) {
            return response()->json([
                'success' => false,
                'msg' => 'No file found, please select a file',
            ], 200);
        }

        $ext = $request->file('uploadfile')->getClientOriginalExtension();
        //$path = $request->file('uploadfile')->store('files');
        $filename = str_random(32) . '.' . $ext;
        $path = $request->file('uploadfile')->storeAs(
            'files', $filename
        );

        if (!$path) {
            return response()->json([
                'success' => false,
                'msg' => 'An error occurred and the upload failed, please retry',
            ], 200);
        }

        return response()->json([
            'success' => true,
            'filename' => $filename,
        ], 200);

    }

    public function get_bvn_details($bvn_number)
    {
        $__account = new \App\Libraries\__account();
        

        //$data = $request->all();
        //$bvn_number = isset($data['bvn_number'])?$data['bvn_number']:null;
        $bvns = json_decode(file_get_contents(storage_path('bvn.json')), true);
        $details = array_search($bvn_number, array_column($bvns, 'BVNNumber'));

        if (!$details) {
            return response()->json([
                'response' => false,
                'msg' => 'No bvn details found',
            ], 200);
        }

        $bvn = $bvns[$details];

        $status = Bvn::updateOrCreate(
            [
                'session_id' => set_session(),
                'first_name' => $bvn['FirstName'],
                'middle_name' => $bvn['MiddleName'],
                'last_name' => $bvn['LastName'],
                'gender' => $bvn['Gender'],
                'nationality' => $bvn['Nationality'],
                'birth_date' => $bvn['DateOfBirth']
            ],
            ['bvn_number' => $bvn_number]
        );

        return response()->json([
            'response' => true,
            'details' => $bvn
        ], 200);

    }


    public function load_director($id = 0)
    {
        $this->data = [];
        $this->data['countries'] = $this->countries;
        $this->data['countries_top'] = $this->countries_top;
        $this->data['titles'] = $this->titles;
        $id = $id?$id:1;
        $this->data['id'] = $id;
        return view("site.register.director", $this->data);
    }


	public function load_signatory($id = 0)
	{
		$this->data = [];
		$this->data['countries'] = $this->countries;
		$this->data['countries_top'] = $this->countries_top;
		$this->data['titles'] = $this->titles;
		$id = $id?$id:2;
		$this->data['id'] = $id;
		return view("site.register.signatory", $this->data);
	}

}
