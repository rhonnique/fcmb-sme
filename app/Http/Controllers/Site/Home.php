<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Faker;
use DB;
use Carbon;
use App\Models\Bvn;
use Session;

class Home extends Controller
{

    public $data = [];

	public function __construct()
	{
		//$this->data['current_uri'] = Route::getFacadeRoot()->current()->uri();
		//$this->data['page_title']         = "Home";
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $__account = new \App\Libraries\__account();
        return $__account->session();
        

        $flight = Bvn::updateOrCreate(
            ['first_name' => 'Oakland', 'last_name' => 'San Diego'],
            ['bvn_number' => 99]
        );

        //print_r(get_bvn_details(95382147543)->details); exit;

        /*
        $db = [];
        $faker = Faker\Factory::create();

        for ($i=0; $i < 20; $i++) {
            $date = $faker->dateTimeBetween('-30 years', '-20 years')->getTimestamp();
            $db[] = date('d-m-Y',$date);
        }

        print_r($db);
        echo '<br/><br/>';

        //echo $db[rand(1,9)];

        //return response()->json($db);

        $query = DB::table('register')->inRandomOrder()->limit(15)->
        select(DB::raw( 'uniques as BVNNumber, firstname as FirstName, othernames as LastName, 
        UPPER(gender) as Gender, residence as Nationality, homeaddress as ResidentialAddress, 
        phone as PhoneNumber, ("'.$db[rand(1,9)].'") as DateOfBirth'))->get();
        */

        
        


        /*
        $BVNSingleRequest = new \stdClass();
        $BVNSingleRequest->BVNNumber = '22146325267';//"22146325267";
        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', env('BVN_ENDPOINT'), [
            'headers' => [
                'AppID' => env('BVN_APP_ID'),
                'AppKey' => env('BVN_APP_KEY'),
            ],
'form_params' => $BVNSingleRequest,
'exceptions' => false,
'http_errors' => false,
]);

print_r($res->getBody()); exit;
*/

        return view( "site.home", $this->data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
