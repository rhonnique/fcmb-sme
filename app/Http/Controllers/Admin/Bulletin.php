<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Input;
use Session;
use Validator;
use Redirect;
use Random;
use Route;

class Bulletin extends Controller
{

	public $data;

	public function __construct() {
		$this->data                       = array();
		$this->data['current_uri']              = Route::getFacadeRoot()->current()->uri();

	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $this->data['page_title']         = "Bulletin";
	    //$this->data['page_header_action'] = "admin.posts.header";
	    $this->data['current_post_id'] = '';

	    $list = "";
	    $select = DB::table( 'posts' )->orderby( 'id', 'DESC' )->get( [ 'post_id', 'name' ] );
	    if ( $select->count() > 0 ) {
		    foreach ( $select as $row ) {
			    $list .= '<option value="' . $row->post_id . '">' . $row->name . '</option>';
		    }
	    }

	    $this->data['select_post'] = $list;

	    $check = DB::table( 'bulletins' )->first();
	    if($check){
		    $this->data['current_post_id'] = $check->post_id;
	    }

	    return view( "admin.bulletin.home", $this->data );
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function get_post($id)
	{
		$post = DB::table('posts')->where('post_id', $id)->first();

		return response()->json($post, 200 );
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $post_id
	 * @return \Illuminate\Http\Response
	 */
	public function save($post_id)
	{

		$check = DB::table( 'bulletins' )->first();
		if ( $check ) {

			$status = DB::table( 'bulletins' )
			            ->where( 'bulletin_id', $check->bulletin_id )
			            ->update( [
				            'post_id'    => $post_id
			            ] );

			if ( $status >= 0 ) {
				return response()->json([
					'Response' => true
				], 200 );
			} else {
				return response()->json([
					'Response' => false
				], 200 );
			}

		} else {
			$bulletin_id = substr( Random::generateInt( 10 ), 0, 10 );
			$status = DB::table( 'bulletins' )->insert(
				[
					'bulletin_id'    => $bulletin_id,
					'post_id'    => $post_id
				]
			);

			if ( $status ) {
				return response()->json([
					'Response' => true
				], 200 );
			} else {
				return response()->json([
					'Response' => false
				], 200 );
			}
		}



	}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }






}
