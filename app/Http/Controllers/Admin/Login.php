<?php

namespace App\Http\Controllers\Admin;

use App\Models\_account;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Model\_Category;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Redirect;
use Random;

class Login extends Controller
{
	public $data;

	public function __construct() {
		$this->data                       = array();

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{

		 //echo(Session::get('admin_0721983')['admin_id']); exit;;
		//print_r(Session::all()); exit;

		if(Session::has('admin_0721983'))
			return redirect('dashboard');

		$this->data['page_title']         = "Login";
		$this->data['page_body_class'] = "page-login-v3 layout-full";
		return view( "admin.login", $this->data );
	}


	/**
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function loginSubmit(Request $request)
	{
		$_account = new _account();
		$email = $request->input( "email" );
		$password = $request->input( "password" );

		$v = Validator::make( $request->all(), [
				'email' => 'required',
				'password' => 'required'
			],
			[
				'email.required' => 'Email address is required!',
				'password.required' => 'Password is required!',
			]
		);

		if ( $v->fails() ) {
			return redirect()->back()->withErrors( $v->errors() );
		}

		$account   = $_account->login( $email, md5($password) );
		if ( !$account ) {
			return redirect()->back()->
			withErrors( array( "email" => array( "Email address or password not found!" ) ) )->
			withInput();
		}

		// Session::put('admin_id', $account->admin_id);
		Session::put('admin_0721983', [
			'admin_id' => $account->admin_id
		]);

		return redirect( 'dashboard' );
	}

}
