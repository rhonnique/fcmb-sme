<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Session;
use Redirect;

class Logout extends Controller
{
	/**
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
    public function index(Request $request)
    {
	    $request->session()->forget('admin_0721983');
	    return redirect( 'login' );
    }

}
