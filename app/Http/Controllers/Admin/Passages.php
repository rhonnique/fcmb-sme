<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Models\_passages;
use Input;
use Session;
use Validator;
use Redirect;
use Random;
use File;
use Excel;
use Route;

class Passages extends Controller {

	public $data = [];

	public function __construct() {
		$this->data['current_uri']        = Route::getFacadeRoot()->current()->uri();
		$this->data['page_title']         = "Daily Passages";
		$this->data['page_header_action'] = "admin.passages-header";
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$_passage                  = new _passages();
		$select['details']         = Input::get( 'details' );
		$select['date_from']       = Input::get( 'date_from' );
		$select['date_to']         = Input::get( 'date_to' );
		$select['paginate']        = 50;
		$selectCheck               = array_filter( $select );
		$this->data['selectCheck'] = empty( $selectCheck ) ? false : true;
		$table                     = $_passage->passages( $select );

		$this->data['select'] = $select;
		$this->data['table']  = $table;

		//$this->data['bodyClass'] = "app-dashboard";
		return view( "admin.passages", $this->data );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$date    = $request->input( "date" );
		$verse   = $request->input( "verse" );
		$passage = $request->input( "passage" );
		$note    = $request->input( "note" );

		$date = strtotime( $date );
		$date = date( 'Y-m-d', $date );

		$check = DB::table( 'daily_passage' )->where( 'date', '=', $date )->first();
		if ( $check ) {
			return redirect()->back()->
			withErrors( array( "date" => array( "Daily Passage already exist for this date!" ) ) )->
			withInput();
		}

		$status = DB::table( 'daily_passage' )->insert(
			[
				'date'    => $date,
				'verse'   => ucwords( $verse ),
				'passage' => $passage,
				'note'    => $note
			]
		);

		if ( $status ) {
			Session::flash( 'msg_ok', 'Passage successfully added!' );
		} else {
			Session::flash( 'msg_error', 'Can not complete your request at the moment!!' );
		}

		return redirect( 'bible-passages' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {

		$date    = $request->input( "date" );
		$verse   = $request->input( "verse" );
		$passage = $request->input( "passage" );
		$note    = $request->input( "note" );

		$date = strtotime( $date );
		$date = date( 'Y-m-d', $date );

		$check = _passages::where( 'date', '=', $date )->whereNotIn( 'id', [ $id ] )->first(); //print_r($check); exit;
		if ( $check ) {
			return redirect()->back()->
			withErrors( array( "date" => array( "Passage for this date already exist!" ) ) )->
			withInput();
		}

		$status = DB::table( 'daily_passage' )
		            ->where( 'id', $id )
		            ->update( [
			            'date'    => $date,
			            'verse'   => ucwords( $verse ),
			            'passage' => $passage,
			            'note'    => $note
		            ] );

		if ( $status >= 0 ) {
			return redirect( resetEdit( "bible-passages" ) )->
			with( 'msg_ok', 'Passage modified successfully!' );
		} else {
			return redirect()->back()->
			with( 'msg_error', 'Can not complete your request at the moment!' );
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}


	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function delete( Request $request ) {

		$delete = DB::table( 'daily_passage' )->whereIn( 'id', $request->table_row_selected )->delete();

		if ( $delete ) {
			Session::flash( 'msg_ok', 'Selected Passages successfully deleted!' );
		} else {
			Session::flash( 'msg_error', 'No passage deleted!' );
		}

		return redirect( 'bible-passages' );

	}


	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function upload( Request $request ) {

		$user_data      = $request->get( 'user_data' );
		$_passage       = new _passages();
		$data           = $request->all();
		$passage_upload = $request->file( 'passage_upload' );
		$count          = 0;

		$path = $passage_upload->getRealPath();
		$ext  = $passage_upload->getClientOriginalExtension();

		if ( $ext != 'csv' && $ext != 'xlsx' ) {
			return response()->json( [
				"Response" => false,
				"Details"  => [
					"Code" => "ERR_FIELD",
					"Msg"  => "Only text files(.txt), comma delimited files(.csv),
						and excel files(.xlsx) are allowed"
				]
			], 200 );
		}

		$data = Excel::selectSheetsByIndex( 0 )->load( $path, function ( $reader ) {
		} )->noHeading()->get();
		/**/
		if ( ! empty( $data ) && $data->count() ) {
			foreach ( $data as $row ) {
				if ( ! empty( $row ) && $row != null ) {
					$date    = $row[0];
					$verse   = $row[1];
					$passage = $row[2];

					$date = strtotime( $date );
					$date = date( 'Y-m-d', $date );

					$check = DB::table( 'daily_passage' )->where( 'date', '=', $date )->first();

					if ( ! $check ) {
						$status = DB::table( 'daily_passage' )->insert( array(
							'date'    => $date,
							'verse'   => $verse,
							'passage' => $passage
						) );

						if ( $status ) {
							$count ++;
						}
					}
				}
			}
		}

		if ( $count > 0 ) {
			Session::flash( 'msg_ok', $count . ' Passages successfully added!' );
		} else {
			Session::flash( 'msg_error', 'No passage added!' );
		}

		return redirect( 'bible-passages' );

	}
}
