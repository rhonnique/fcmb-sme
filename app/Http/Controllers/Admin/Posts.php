<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Models\_posts;
use Input;
use Session;
use Validator;
use Redirect;
use Random;
use Route;

class Posts extends Controller
{

	public $data;

	public function __construct() {
		$this->data                       = array();
		$this->data['current_uri']              = Route::getFacadeRoot()->current()->uri();

	}


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $this->data['page_title']         = "Posts";
	    $this->data['page_header_action'] = "admin.posts.header";

	    $_posts                  = new _posts();
	    $select['details']         = Input::get( 'details' );
	    $select['date_from']       = Input::get( 'date_from' );
	    $select['date_to']         = Input::get( 'date_to' );
	    $select['paginate']        = 50;
	    $selectCheck               = array_filter( $select );
	    $this->data['selectCheck'] = empty( $selectCheck ) ? false : true;
	    $table                     = $_posts->posts( $select );

	    $this->data['select'] = $select;
	    $this->data['table']  = $table;

	    return view( "admin.posts.home", $this->data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    $this->data['page_title_btn']         = '<button type="button" class="btn btn-sm btn-icon btn-round btn-success
        waves-effect waves-classic panel-show-new"
                data-toggle="tooltip" data-original-title="Return"
                onclick="window.location=\'\'">
                  <i class="icon wb-arrow-left" aria-hidden="true"></i>
       </button> ';
	    $this->data['page_title']         = 'Add New Posts';
	    $this->data['page_header_action'] = "admin.posts.create-header";

	    return view( "admin.posts.create", $this->data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $name    = $request->input( "name" );
	    $content   = $request->input( "content" );
	    $post_id = substr( Random::generateInt( 10 ), 0, 10 );

	    $check = DB::table( 'posts' )->where( 'name', '=', $name )->first();
	    if ( $check ) {
		    return redirect()->back()->
		    withErrors( array( "date" => array( "Post already exist!" ) ) )->
		    withInput();
	    }

	    $status = DB::table( 'posts' )->insert(
		    [
			    'post_id'    => $post_id,
			    'name'   => ucwords( $name ),
			    'post_content' => $content
		    ]
	    );

	    if ( $status ) {
		    Session::flash( 'msg_ok', 'Post successfully added!' );
		    return redirect( 'posts' );
	    } else {
		    Session::flash( 'msg_error', 'Can not complete your request at the moment!!' );
		    return redirect()->back()->
		    //withErrors( array( "title" => array( "Category already exist!" ) ) )->
		    withInput();
	    }

	    //return redirect( 'bible-passages' );




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $this->data['page_title_btn']         = '<button type="button" class="btn btn-sm btn-icon btn-round btn-success
        waves-effect waves-classic panel-show-new"
                data-toggle="tooltip" data-original-title="Return"
                onclick="window.location=\'\'">
                  <i class="icon wb-arrow-left" aria-hidden="true"></i>
       </button> ';
	    $this->data['page_title']         = 'Edit Post';
	    $this->data['page_header_action'] = "admin.posts.create-header";

	    $details = DB::table('posts')->where('id', $id)->first();

	    if(!$details){
		    Session::flash( 'msg_error', 'Invalid page request!' );
		    return redirect( 'posts' );
	    }

	    $this->data['details'] = $details;

	    return view( "admin.posts.edit", $this->data );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $name    = $request->input( "name" );
	    $content   = $request->input( "content" );

	    $check = _posts::where( 'name', '=', $name )->whereNotIn( 'id', [ $id ] )->first();
	    if ( $check ) {
		    return redirect()->back()->
		    withErrors( array( "date" => array( "Post already exist!" ) ) )->
		    withInput();
	    }

	    $status = DB::table( 'posts' )
	                ->where( 'id', $id )
	                ->update( [
		                'name'   => ucwords( $name ),
		                'post_content' => $content
	                ] );

	    if ( $status >= 0 ) {
		    return redirect( resetEdit( "posts" ) )->
		    with( 'msg_ok', 'Post modified successfully!' );
	    } else {
		    return redirect()->back()->
		    with( 'msg_error', 'Can not complete your request at the moment!' )->withInput();
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function delete( Request $request ) {

		$delete = DB::table( 'posts' )->whereIn( 'id', $request->table_row_selected )->delete();

		if ( $delete ) {
			Session::flash( 'msg_ok', 'Selected post(s) successfully deleted!' );
		} else {
			Session::flash( 'msg_error', 'No post deleted!' );
		}

		return redirect( 'posts' );

	}
}
