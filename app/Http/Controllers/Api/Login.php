<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use App\Models\Query;

class Login extends Controller
{


	public function index($username, $password) {

		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		$xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<user_login xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '<userName>'.$username.'</userName>';
		$xml .= '<password>'.$password.'</password>';
		$xml .= '</user_login>';
		$xml .= '</soap12:Body></soap12:Envelope>';

		$url = 'http://www.cityofdavidng.org/webservices/service.asmx';
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$result = curl_exec($ch);
		curl_close($ch);

		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml_format = new \SimpleXMLElement($response);
		$xml_format = $xml_format->xpath('//soapBody')[0];
		$xml_format = $xml_format->user_loginResponse->user_loginResult;
		$to_json = json_decode($xml_format, true);

		if(!$to_json){
			return response()->json( false, 200 );
		}

		return response()->json( $to_json, 200 );

	}



    public function index2() {


		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		$xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<user_login xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '<userName>rh2</userName>';
		$xml .= '<password>password</password>';
		$xml .= '</user_login>';
		$xml .= '</soap12:Body></soap12:Envelope>';

		/*
		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		$xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<user_login xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '<userName>'.$username.'</userName>';
		$xml .= '<password>'.$password.'</password>';
		$xml .= '</user_login></soap12:Body></soap12:Envelope>';
		*/

		$url = 'http://www.cityofdavidng.org/webservices/service.asmx';
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$result = curl_exec($ch);
		curl_close($ch);

		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml_format = new \SimpleXMLElement($response);
		$xml_format = $xml_format->xpath('//soapBody')[0];
		$xml_format = $xml_format->user_loginResponse->user_loginResult;
		$to_json = json_decode($xml_format, true);

		if(!$to_json){
			return response()->json( false, 200 );
		}

		return response()->json( $to_json, 200 );

	}




	/**
	 * Get Api request
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 
	public function index(Request $request) {

		$data = $request->all();
		$username = isset($data['username'])?$data['username']:null;
		$password = isset($data['password'])?$data['password']:null;


		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		$xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<user_login xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '<userName>'.$username.'</userName>';
		$xml .= '<password>'.$password.'</password>';
		$xml .= '</user_login>';
		$xml .= '</soap12:Body></soap12:Envelope>';

		$url = 'http://www.cityofdavidng.org/webservices/service.asmx';
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$result = curl_exec($ch);
		curl_close($ch);

		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml_format = new \SimpleXMLElement($response);
		$xml_format = $xml_format->xpath('//soapBody')[0];
		$xml_format = $xml_format->user_loginResponse->user_loginResult;
		$to_json = json_decode($xml_format, true);

		if(!$to_json){
			return response()->json( false, 200 );
		}

		return response()->json( $to_json, 200 );

	}
	*/


}
