<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
//use App\Models\Query;

class Register extends Controller
{


	public function index($email, $username, $password, $first_name, $last_name) {

		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		$xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<create_user xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '<userName>'.$username.'</userName>';
		$xml .= '<password>'.$password.'</password>';
		$xml .= '<firstName>'.$first_name.'</firstName>';
		$xml .= '<lastName>'.$last_name.'</lastName>';
		$xml .= '<email>'.$email.'</email>';
		$xml .= '</create_user></soap12:Body></soap12:Envelope>';

		$url = 'http://www.cityofdavidng.org/webservices/service.asmx';
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$result = curl_exec($ch);
		curl_close($ch);

		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml_format = new \SimpleXMLElement($response);
		$xml_format = $xml_format->xpath('//soapBody')[0];
		$xml_format = $xml_format->create_userResponse->create_userResult;
		$to_json = json_decode($xml_format, true);

		if(!$to_json){
			return response()->json( false, 200 );
		}

		return response()->json( $to_json, 200 );
	}


	/**
	 * Get Api request
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 
	public function index(Request $request) {

		// string apiKey, string userName, string password,string firstName, string lastName,string email
		//$models = new Query();
		$data = $request->all();
		$email = isset($data['email'])?strtolower($data['email']):null;
		$username = isset($data['username'])?$data['username']:null;
		$password = isset($data['password'])?$data['password']:null;
		$first_name = isset($data['first_name'])?ucwords($data['first_name']):null;
		$last_name = isset($data['last_name'])?ucwords($data['last_name']):null;

		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		$xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<create_user xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '<userName>'.$username.'</userName>';
		$xml .= '<password>'.$password.'</password>';
		$xml .= '<firstName>'.$first_name.'</firstName>';
		$xml .= '<lastName>'.$last_name.'</lastName>';
		$xml .= '<email>'.$email.'</email>';
		$xml .= '</create_user></soap12:Body></soap12:Envelope>';

		$url = 'http://www.cityofdavidng.org/webservices/service.asmx';
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$result = curl_exec($ch);
		curl_close($ch);

		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml_format = new \SimpleXMLElement($response);
		$xml_format = $xml_format->xpath('//soapBody')[0];
		$xml_format = $xml_format->create_userResponse->create_userResult;
		$to_json = json_decode($xml_format, true);

		if(!$to_json){
			return response()->json( false, 200 );
		}

		return response()->json( $to_json, 200 );
	}
	*/


}
