<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use SoapClient;

//use App\Models\Query;

class Gallery extends Controller
{


	function index() {

		$xml = '<?xml version="1.0" encoding="utf-8"?>';
        $xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<get_UMG_Albums xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '</get_UMG_Albums></soap12:Body></soap12:Envelope>';

		$url = 'http://www.cityofdavidng.org/webservices/service.asmx';
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$result = curl_exec($ch);
		curl_close($ch);

		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml_format = new \SimpleXMLElement($response);
		$xml_format = $xml_format->xpath('//soapBody')[0];
		$xml_format = $xml_format->get_UMG_AlbumsResponse->get_UMG_AlbumsResult;
		$to_json = json_decode($xml_format, true);

		return response()->json( $to_json, 200 );

	}


	function details($id) {

		$xml = '<?xml version="1.0" encoding="utf-8"?>';
		$xml .= '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
		$xml .= '<soap12:Body>';
		$xml .= '<get_UMG_Photos xmlns="http://www.cityofdavidng.org/">';
		$xml .= '<apiKey>'.$_ENV['COD_API_KEY'].'</apiKey>';
		$xml .= '<albumId>'.$id.'</albumId>';
		$xml .= '</get_UMG_Photos></soap12:Body></soap12:Envelope>';

		$url = 'http://www.cityofdavidng.org/webservices/service.asmx';
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		$result = curl_exec($ch);
		curl_close($ch);

		$response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
		$xml_format = new \SimpleXMLElement($response);
		$xml_format = $xml_format->xpath('//soapBody')[0];
		$xml_format = $xml_format->get_UMG_PhotosResponse->get_UMG_PhotosResult;
		$to_json = json_decode($xml_format, true);

		if(!$to_json){
			return response()->json( false, 200 );
		}

		return response()->json( $to_json, 200 );
	}


	/*
	 function index() {

		 $gallery = collect( DB::select( "SELECT a.id, a.title,
         (SELECT image FROM gallery_photo WHERE gallery_id = a.id LIMIT 1) AS thumb
         FROM gallery a ORDER BY id ASC" ) );

		 return response()->json( [
			 "response" => "Success",
			 "details"  => $gallery
		 ], 200 );
	}


	function details($id) {

		$gallery = DB::table('gallery_photo')->where('gallery_id', $id)
			->get(['image']);

		return response()->json( [
			"response" => "Success",
			"details"  => $gallery
		], 200 );
	}
	*/


}
