<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Input;

class Account extends Controller
{


	/**
	 * Get Api request
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( Request $request ) {

		$data      = $request->all();
		$member_id = isset( $data['member_id'] ) ? $data['member_id'] : null;
		$phone = isset( $data['phone'] ) ? $data['phone'] : null;
		$name = isset( $data['name'] ) ? ucwords($data['name']) : null;

		if ( ! $name || !$phone ) {
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_FIELDS",
					"Msg"  => "Name and phone number are required"
				]
			], 200 );
		}


	    $update = DB::table('members')
		    ->where('member_id', $member_id)
		    ->update([
			    'name' => $name,
			    'phone' => $phone
		    ]);

		if($update >= 0){
			$account = DB::table('members')
			                      ->where('member_id', $member_id)->first();

			return response()->json( [
				"response" => "Success",
				"details"  => array(
					"Member_ID" => $account->member_id,
					"Name" => $account->name,
					"Phone" => $account->phone,
					"Email" => $account->email,
					"Status" => $account->status,
				)
			], 200 );
		}


		return response()->json( [
			"response" => "Error",
			"details"  => [
				"Code" => "ERR_SAVE",
				"Msg"  => "Account not successfully modified"
			]
		], 200 );


    }


}
