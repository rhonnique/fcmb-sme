<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use DB;

class Bulletin extends Controller
{

    public function index()
    {

	    $bulletin = collect( DB::select( "SELECT a.bulletin_id, b.post_id, b.post_content
        FROM bulletins a
        LEFT JOIN posts b ON b.post_id = a.post_id LIMIT 1" ) )->first();

	    if ( ! $bulletin || count($bulletin) == 0 ) {
		    return response()->json( [
			    "response" => "Error",
			    "details"  => [
				    "Code" => "ERR_EMPTY",
				    "Msg"  => "No bulletin available"
			    ]
		    ], 200 );
	    }

	    return response()->json( [
		    "response" => "Success",
		    "details"  => $bulletin
	    ], 200 );
    }


}
