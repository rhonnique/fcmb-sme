<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use MAbiola\Paystack\Paystack;
use App\Models\_pledges;
use Input;
use Random;

class Pledges extends Controller
{

    public function index()
    {
	    $pledges = DB::table('pledges')->orderby('id','DESC')->get(['id','pledge_id','name','amount','status']);
	    return response()->json( [
		    "response" => "Success",
		    "details"  => $pledges
	    ], 200 );
    }



	public function details($pledge_id, $member_id)
	{
		$mb = null;

		$pledges = DB::table('pledges')->where('pledge_id',$pledge_id)->get()->first();

		if($member_id){
			$member_pledge = $this->member_pledge_details($member_id, $pledge_id);
			if($member_pledge->original['response'] == "Success" && $member_pledge->original['details'] != null){
				$mb = $member_pledge->original['details'];
			}
		}

		return response()->json( [
			"response" => "Success",
			"details"  => $pledges,
			"pledge"  => $mb
		], 200 );
	}



	/**
	 * @param  \Illuminate\Http\Request $request
	 *e
	 * @return \Illuminate\Http\Response

	public function details(Request $request)
	{
		$data      = $request->all();
		$member_id = isset( $data['member_id'] ) ? $data['member_id'] : null;
		$pledge_id    = isset( $data['pledge_id'] ) ? $data['pledge_id'] : null;
		$mb = null;

		$pledges = DB::table('pledges')->where('pledge_id',$pledge_id)->get()->first();

		if($member_id){
			$member_pledge = $this->member_pledge_details($member_id, $pledge_id);
			if($member_pledge->original['response'] == "Success" && $member_pledge->original['details'] != null){
				$mb = $member_pledge->original['details'];
			}
		}

		return response()->json( [
			"response" => "Success",
			"details"  => $pledges,
			"pledge"  => $mb
		], 200 );
	}
	*/


	public function member_pledges($member_id)
	{

		$_pledges = new _pledges();
		$select['member_id'] = $member_id;
		$select['get'] = true;
		$pledges = $_pledges->member_pledges($select);

		return response()->json( [
			"response" => "Success",
			"details"  => $pledges
		], 200 );
	}

	public function member_pledge_details($member_id, $pledge_id)
	{

		$_pledges = new _pledges();
		$select['member_id'] = $member_id;
		$select['pledge_id'] = $pledge_id;
		$select['first'] = true;
		$pledges = $_pledges->member_pledges($select);

		return response()->json( [
			"response" => "Success",
			"details"  => $pledges
		], 200 );
	}


	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function make_pledge( Request $request ) {
		$data      = $request->all();
		$member_id = isset( $data['member_id'] ) ? $data['member_id'] : null;
		$amount    = isset( $data['amount'] ) ? $data['amount'] : null;
		$pledge_id    = isset( $data['pledge_id'] ) ? $data['pledge_id'] : null;
		//$pledge_id       = substr( Random::generateInt( 10 ), 0, 10 );

		$add_pledge = DB::table( 'members_pledges' )->insert( array(
			'member_id'         => $member_id,
			'pledge_id'    => $pledge_id,
			'amount'            => $amount
		) );

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"Member_ID"    => $member_id,
				"Pledge_ID"          => $pledge_id,
				"Amount"   => $amount
			)
		], 200 );

	}



	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function summary( Request $request ) {
		$data      = $request->all();
		$member_id = isset( $data['member_id'] ) ? $data['member_id'] : null;
		$amount    = isset( $data['amount'] ) ? $data['amount'] : null;
		$ref       = str_random( 15 );

		$commission = ( 2 / 100 ) * $amount;
		$commission = $commission > 2000 ? 2000 : $commission;
		$total      = $amount + $commission;
		$postAmount = $total * 100;
		$key = $_ENV['PAYSTACK_MODE'] === 'live'?
			$_ENV['PAYSTACK_LIVE_PUBLIC_KEY']:$_ENV['PAYSTACK_TEST_PUBLIC_KEY'];

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"Member_ID"    => $member_id,
				"Paystack_Key" => $key,
				"Sub_Account" => $_ENV['PAYSTACK_SUB_ACCOUNT'],
				"Ref"          => $ref,
				"Commission"   => $commission,
				"Total"        => $total,
				"Post_Amount"  => $postAmount
			)
		], 200 );

	}



	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function done( Request $request ) {
		$data      = $request->all();
		$member_id = isset( $data['member_id'] ) ? $data['member_id'] : null;
		$pledge_id = isset( $data['pledge_id'] ) ? $data['pledge_id'] : null;
		$amount    = isset( $data['amount'] ) ? $data['amount'] : null;
		$commission    = isset( $data['commission'] ) ? $data['commission'] : null;
		$ref       = isset( $data['ref'] ) ? $data['ref'] : null;

		$paystackLibObject = Paystack::make();
		$transaction       = $paystackLibObject->verifyTransaction( $ref );

		if ( ! $transaction ) {
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_RESPONSE",
					"Msg"  => "An error occurred while processing your transaction, please retry"
				]
			], 200 );
		}

		$trx          = $transaction['data'];
		$status       = $trx['status'];
		$msg          = $trx['gateway_response'];
		$trans_amount = $trx['amount'] / 100;
		$auth_code    = $trx['authorization']['authorization_code'];
		$trans_email  = $trx['customer']['email'];
		$trans_date   = $trx['transaction_date'];

		$add_transaction = DB::table( 'transactions' )->insert( array(
			'member_id'         => $member_id,
			'transaction_id'    => $ref,
			'amount'            => $trans_amount,
			'commission' => $commission,
			'status'            => $status,
			'status_msg'        => $msg,
			'auth_code'         => $auth_code,
			'transaction_email' => $trans_email,
			'transaction_date'  => $trans_date
		) );

		$log = DB::table( 'members_pledges_log' )->insert( array(
			'member_id'      => $member_id,
			'pledge_id'      => $pledge_id,
			'transaction_id' => $ref,
			'amount'         => $trans_amount
		) );

		$pledge = DB::table('members_pledges')
			->where(['pledge_id' => $pledge_id, 'member_id' => $member_id])
			->increment('redeemed', $trans_amount);

		$date = date('M d, Y h:i a', strtotime($trans_date));

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"trx"          => $trx,
				"Ref"          => $ref,
				"Status"          => $status,
				"Status_Msg"          => $msg,
				"Date"          => $date,
				"Commission"   => $commission,
				"Amount"   => $amount,
				"Total"        => $commission + $amount,
			)
		], 200 );

	}


}
