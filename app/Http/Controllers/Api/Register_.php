<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
//use App\Models\Query;

class Register_ extends Controller
{


	/**
	 * Get Api request
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {

		//$models = new Query();
		$data = $request->all();

		$phone = isset($data['phone'])?$data['phone']:null;
		$password = isset($data['password'])?md5($data['password']):null;
		$name = isset($data['name'])?ucwords($data['name']):null;
		$email = isset($data['email'])?strtolower($data['email']):null;

		if(!$phone || !$password || !$name || !$email){
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_FIELD",
					"Msg" => "All fields are required"
				]
			], 200 );
		}

		$check_email = DB::table( "members" )->where( [
			'email' => $email
		] )->first();

		if($check_email){
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_002",
					"Msg"  => "Email address already exist"
				]
			], 200 );
		}

		$check_phone = DB::table( "members" )->where( [
			'phone' => $phone
		] )->first();

		if($check_phone){
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_003",
					"Msg"  => "Phone number already exist"
				]
			], 200 );
		}

		$member_id = substr(rand().rand().rand(),0,10);
		$register = DB::table( 'members' )->insert( array(
			'member_id' => $member_id,
			'name'     => $name,
			'phone'         => format_phone(234, $phone),
			'email'         => $email,
			'password'         => $password,
		) );

		if(!$register){
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_004",
					"Msg" => "Can not save data, please retry"
				]
			], 200 );
		}

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"Member_ID" => $member_id,
				"Name" => $name,
				"Phone" => $phone,
				"Email" => $email,
				"Status" => 1
			)
		], 200 );
	}


}
