<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\_sermon;
use Illuminate\Support\Facades\Input;

class Podcast extends Controller
{

    public function index()
    {
	    $pledges = DB::table('podcast')->orderby('id','DESC')->get();
	    return response()->json( [
		    "response" => "Success",
		    "details"  => $pledges
	    ], 200 );
    }


	public function details($pledge_id)
	{
		$pledges = DB::table('podcast')->where('podcast_id',$pledge_id)->get()->first();

		return response()->json( [
			"response" => "Success",
			"details"  => $pledges
		], 200 );
	}


}
