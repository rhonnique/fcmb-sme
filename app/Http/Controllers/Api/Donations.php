<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MAbiola\Paystack\Paystack;
use DB;
use App\Models\Query;

class Donations extends Controller {
	
	
	public function summary($member_id, $amount) {
		$ref       = str_random( 15 );

		$commission = ( 2 / 100 ) * $amount;
		$commission = $commission > 2000 ? 2000 : $commission;
		$total      = $amount + $commission;
		$postAmount = $total * 100;
		$key = $_ENV['PAYSTACK_MODE'] === 'live'?
			$_ENV['PAYSTACK_LIVE_PUBLIC_KEY']:$_ENV['PAYSTACK_TEST_PUBLIC_KEY'];

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"Member_ID"    => $member_id,
				"Paystack_Key" => $key,
				"Sub_Account" => $_ENV['PAYSTACK_SUB_ACCOUNT'],
				"Ref"          => $ref,
				"Commission"   => $commission,
				"Total"        => $total,
				"Post_Amount"  => $postAmount
			)
		], 200 );

	}
	
	public function done($member_id, $ref, $amount, $commission, $purpose) {
		$paystackLibObject = Paystack::make();
		$transaction       = $paystackLibObject->verifyTransaction( $ref );

		if ( ! $transaction ) {
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_RESPONSE",
					"Msg"  => "An error occurred while processing your transaction, please retry"
				]
			], 200 );
		}

		$trx          = $transaction['data'];
		$status       = $trx['status'];
		$msg          = $trx['gateway_response'];
		$trans_amount = $trx['amount'] / 100;
		$auth_code    = $trx['authorization']['authorization_code'];
		$trans_email  = $trx['customer']['email'];
		$trans_date   = $trx['transaction_date'];


		$add_transaction = DB::table( 'transactions' )->insert( array(
			'member_id'         => $member_id,
			'transaction_id'    => $ref,
			'amount'            => $trans_amount,
			'commission' => $commission,
			'status'            => $status,
			'status_msg'        => $msg,
			'auth_code'         => $auth_code,
			'transaction_email' => $trans_email,
			'transaction_date'  => $trans_date
		) );

		$add_donation = DB::table( 'donations' )->insert( array(
			'member_id'      => $member_id,
			'transaction_id' => $ref,
			'amount'         => $trans_amount,
			'purpose'         => $purpose,
		) );

		$date = $trans_date?date( 'M d, Y h:i a', strtotime( $trans_date ) ):
			date( 'M d, Y h:i a');

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"trx"          => $trx,
				"Ref"          => $ref,
				"Status"          => $status,
				"Status_Msg"          => $msg,
				"Date"          => $date,
				"Commission"   => $commission,
				"Amount"   => $amount,
				"Total"        => $commission + $amount,
			)
		], 200 );

	}





	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 
	public function done( Request $request ) {
		$data      = $request->all();
		$member_id = isset( $data['member_id'] ) ? $data['member_id'] : null;
		$amount    = isset( $data['amount'] ) ? $data['amount'] : null;
		$commission    = isset( $data['commission'] ) ? $data['commission'] : null;
		$ref       = isset( $data['ref'] ) ? $data['ref'] : null;
		$purpose       = isset( $data['purpose'] ) ? ucwords($data['purpose']) : null;

		$paystackLibObject = Paystack::make();
		$transaction       = $paystackLibObject->verifyTransaction( $ref );

		if ( ! $transaction ) {
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_RESPONSE",
					"Msg"  => "An error occurred while processing your transaction, please retry"
				]
			], 200 );
		}

		$trx          = $transaction['data'];
		$status       = $trx['status'];
		$msg          = $trx['gateway_response'];
		$trans_amount = $trx['amount'] / 100;
		$auth_code    = $trx['authorization']['authorization_code'];
		$trans_email  = $trx['customer']['email'];
		$trans_date   = $trx['transaction_date'];


		$add_transaction = DB::table( 'transactions' )->insert( array(
			'member_id'         => $member_id,
			'transaction_id'    => $ref,
			'amount'            => $trans_amount,
			'commission' => $commission,
			'status'            => $status,
			'status_msg'        => $msg,
			'auth_code'         => $auth_code,
			'transaction_email' => $trans_email,
			'transaction_date'  => $trans_date
		) );

		$add_donation = DB::table( 'donations' )->insert( array(
			'member_id'      => $member_id,
			'transaction_id' => $ref,
			'amount'         => $trans_amount,
			'purpose'         => $purpose,
		) );

		$date = $trans_date?date( 'M d, Y h:i a', strtotime( $trans_date ) ):
			date( 'M d, Y h:i a');

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"trx"          => $trx,
				"Ref"          => $ref,
				"Status"          => $status,
				"Status_Msg"          => $msg,
				"Date"          => $date,
				"Commission"   => $commission,
				"Amount"   => $amount,
				"Total"        => $commission + $amount,
			)
		], 200 );

	}
	*/




	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 
	public function done( Request $request ) {
		$data      = $request->all();
		$member_id = isset( $data['member_id'] ) ? $data['member_id'] : null;
		$amount    = isset( $data['amount'] ) ? $data['amount'] : null;
		$commission    = isset( $data['commission'] ) ? $data['commission'] : null;
		$ref       = isset( $data['ref'] ) ? $data['ref'] : null;
		$purpose       = isset( $data['purpose'] ) ? ucwords($data['purpose']) : null;

		$paystackLibObject = Paystack::make();
		$transaction       = $paystackLibObject->verifyTransaction( $ref );

		if ( ! $transaction ) {
			return response()->json( [
				"response" => "Error",
				"details"  => [
					"Code" => "ERR_RESPONSE",
					"Msg"  => "An error occurred while processing your transaction, please retry"
				]
			], 200 );
		}

		$trx          = $transaction['data'];
		$status       = $trx['status'];
		$msg          = $trx['gateway_response'];
		$trans_amount = $trx['amount'] / 100;
		$auth_code    = $trx['authorization']['authorization_code'];
		$trans_email  = $trx['customer']['email'];
		$trans_date   = $trx['transaction_date'];


		$add_transaction = DB::table( 'transactions' )->insert( array(
			'member_id'         => $member_id,
			'transaction_id'    => $ref,
			'amount'            => $trans_amount,
			'commission' => $commission,
			'status'            => $status,
			'status_msg'        => $msg,
			'auth_code'         => $auth_code,
			'transaction_email' => $trans_email,
			'transaction_date'  => $trans_date
		) );

		$add_donation = DB::table( 'donations' )->insert( array(
			'member_id'      => $member_id,
			'transaction_id' => $ref,
			'amount'         => $trans_amount,
			'purpose'         => $purpose,
		) );

		$date = $trans_date?date( 'M d, Y h:i a', strtotime( $trans_date ) ):
			date( 'M d, Y h:i a');

		return response()->json( [
			"response" => "Success",
			"details"  => array(
				"trx"          => $trx,
				"Ref"          => $ref,
				"Status"          => $status,
				"Status_Msg"          => $msg,
				"Date"          => $date,
				"Commission"   => $commission,
				"Amount"   => $amount,
				"Total"        => $commission + $amount,
			)
		], 200 );

	}
	*/


}
