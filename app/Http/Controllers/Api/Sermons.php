<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\_sermon;
use Illuminate\Support\Facades\Input;

class Sermons extends Controller
{

    public function index()
    {
	    $sermon = DB::table('sermons')->orderby('id','ASC')
		    ->get(['id','sermon_id','title','banner','author']);
	    return response()->json( [
		    "response" => "Success",
		    "details"  => $sermon
	    ], 200 );
    }


	public function details($sermon_id)
	{
		//$_sermon = new _sermon();
		//$sermon = $_sermon->sermon_details($sermon_id);

		DB::table('sermons')->where( 'sermon_id', $sermon_id )->increment('views', 1);
		//DB::table('sermons')->increment('views', 1, ['name' => 'John']);

		$details = array();
		$sermon = DB::table('sermons')->where('sermon_id',$sermon_id)
			->get(['id','sermon_id','title','description','content_link'])->first();

		$details['id'] = $sermon->id;
		$details['sermon_id'] = $sermon->sermon_id;
		$details['title'] = $sermon->title;
		$details['description'] = $sermon->description;
		$details['content_link'] = $sermon->content_link;

		return response()->json( [
			"response" => "Success",
			"details"  => $details
		], 200 );
	}


}
