<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\_sermon;
use Illuminate\Support\Facades\Input;

class Passage extends Controller
{

    public function index()
    {
	    $passage = DB::table('daily_passage')
		    //->where('date', date('Y-m-d'))
		    ->get()->first();

	    return response()->json( [
		    "response" => "Success",
		    "details"  => $passage
	    ], 200 );
    }


}
