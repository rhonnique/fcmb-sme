<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\_account;
use Request;


class CheckUserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	public function handle($request, Closure $next)
	{
		if (!$request->session()->has('admin_0721983')) {
			if(Request::ajax())
			{
				return response()->json( [
					"Response" => false,
					"Details"  => [
						"Code" => "ERR_AUTH",
						"Msg" => "Authentication failed"
					]
				], 401 );
			}

			return redirect('login');
		}

		//$user_id = $request->session()->get('admin_id');
		$id = $request->session()->get('admin_0721983')['admin_id'];

		$_account = new _account();
		$account   = $_account->details( $id );

		if (!$account) {
			$request->session()->forget('admin_0721983');

			if(Request::ajax())
			{
				return response()->json( [
					"Response" => false,
					"Details"  => [
						"Code" => "ERR_AUTH",
						"Msg" => "Authentication failed"
					]
				], 401 );
			}

			return redirect('login');
		}

		$request->request->add(['user_data' => [
			'admin_id' => $account->admin_id,
			'fullname' => $account->fullname,
			'email' => $account->email,
			//'password' => $account->password,
			'status' => $account->status
		]]);

		return $next($request);
	}
}
