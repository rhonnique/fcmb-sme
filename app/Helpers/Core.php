<?php


function resetEdit( $url ) {
	$page   = Input::get( 'page' );
	$append = $page ? "page=" . $page : "";
	$url    = $url . "?" . $append;

	//return $url = substr($url,-1);

	return $url = substr( $url, - 1 ) == "?" ? substr( $url, 0, - 1 ) : $url;


	//return $url."?".$append;
}


function getEditLink( $url, $id ) {
	$urlString = Request::getQueryString();

	//return url( $url . "?mode=edit&id=" . $id . "&" . $urlString );
	return url( $url . "?mode=edit&id=" . $id . "&" . $urlString );

}

function resetEditLink() {
	return substr( current_url(), 0, - strlen( substr( strrchr( current_url(), "edit" ), 0, 20 ) ) );

	/*
	Request::fullUrl();
	// Returns: http://laravel.dev/test?test=1
	Request::url();
	// Returns: http://laravel.dev/test
	Request::path();
	// Returns: test
	Request::root();
	// Returns: http://laravel.dev
	*/
}


function format_phone( $code, $number ) {
	$number = substr( $number, 0, 1 ) == "0" ? substr( $number, 1 ) : $number;
	$number = substr( $number, 0, strlen( $code ) ) != $code ? $code . $number : $number;

	return $number;
}


function get_bvn_details( $bvn_number ) {


	// 22610937345 9107197364 2208379013 8209781232
	$response = new \stdClass();
	$bvns     = json_decode( file_get_contents( storage_path( 'bvn.json' ) ), true );
	$details  = array_search( $bvn_number, array_column( $bvns, 'BVNNumber' ) );

	if ( ! $details ) {
		$response->ResponseCode = 0;
	}

	$response->details = $bvns[ $details ];

	return $response;
}

function set_session() {
	if ( Session::has( 'session_id' ) ):
		$session_id = Session::get( 'session_id' );
	else:
		$session_id = str_random( 32 );
		Session::put( 'session_id', $session_id );
	endif;

	return $session_id;
}

function titles() {
	$query = array( "ALHAJI"           => "ALHAJI",
	                "ALHAJA"           => "ALHAJA",
	                "ARCHITECT"        => "ARCHITECT",
	                "BARRISTER"        => "BARRISTER",
	                "CHIEF"            => "CHIEF",
	                "DEACON"           => "DEACON",
	                "DOCTOR/DR."       => "DOCTOR/DR.",
	                "ELDER"            => "ELDER",
	                "ENGINEER"         => "ENGINEER",
	                "EVANGELIST"       => "EVANGELIST",
	                "EXCELLENCY"       => "EXCELLENCY",
	                "HIGHCHIEF"        => "HIGHCHIEF",
	                "HONOURABLE/HONBL" => "HONOURABLE/HONBL",
	                "HISROYALHIGHNESS" => "HISROYALHIGHNESS",
	                "JUSTICE"          => "JUSTICE",
	                "M/S"              => "M/S",
	                "MASTER"           => "MASTER",
	                "MAZI"             => "MAZI",
	                "MESSERS"          => "MESSERS",
	                "MISS"             => "MISS",
	                "MISTER"           => "MISTER",
	                "MR&MRS"           => "MR&MRS",
	                "MR"               => "MR",
	                "MRS."             => "MRS.",
	                "MISS/MS"          => "MISS/MS",
	                "OBA"              => "OBA",
	                "OTUNBA"           => "OTUNBA",
	                "PASTOR"           => "PASTOR",
	                "PROFESSOR"        => "PROFESSOR",
	                "REVEREND"         => "REVEREND",
	                "SIR"              => "SIR",
	);
	return $query;
}


function country($code)
{
	$query = \DB::table('countries')->where('code', $code)->first();
	return $query;
}


